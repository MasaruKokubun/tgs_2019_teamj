/*!
@file GameOverStage.cpp
@brief ゲームオーバーステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {
//--------------------------------------------------------------------------------------
//	セレクトマネージャー
//--------------------------------------------------------------------------------------
	SelectManager_GG::SelectManager_GG(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_level(Level::a1),
		m_TotalTime(0.0f),
		m_ScaleTime(0.0f),
		m_x(0),
		m_y(0)
	{}
	SelectManager_GG::~SelectManager_GG() {}

	void SelectManager_GG::OnCreate() {
		// これいる？
		m_StageManager = {
			{ Level::a1},
			{ Level::b1},
		};

		// デバッグ用の文字表示コンポーネントを追加する
		//auto debugStrComp = AddComponent<StringSprite>();
		//debugStrComp->SetTextRect(Rect2D<float>(10, 20, 400, 300)); // テキストの表示エリア
		//debugStrComp->SetBackColor(Col4(0, 0, 0, 0.25f)); // テキストエリアの背景色(RGBA)
	}

	void SelectManager_GG::OnUpdate() {
		shared_ptr<GameOverStage> gameOverStage = nullptr;
		gameOverStage = dynamic_pointer_cast<GameOverStage>(this->GetStage());
		if (gameOverStage) {
			if (gameOverStage->GetCursorFlg()) {

				// 描画時間
				float ElapsedTime = App::GetApp()->GetElapsedTime();
				auto ScenePtr = App::GetApp()->GetScene<Scene>();
				//コントローラの取得
				auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
				// どっちかに傾けている
				if ((CntlVec[0].fThumbLX <= -0.5f || CntlVec[0].fThumbLX >= 0.5f) ||
					(CntlVec[0].fThumbLY <= -0.5f || CntlVec[0].fThumbLY >= 0.5f))
				{
					// デッドゾーンと最初の入力
					if (m_TotalTime >= 0.15f || m_TotalTime == 0.0f) {
						m_TotalTime = 0.0f;
						// 上に傾けた
						if (CntlVec[0].fThumbLY >= 0.5f && (CntlVec[0].fThumbLX < 0.5f&&CntlVec[0].fThumbLX > -0.5f) && !m_lr) {
							auto XAPtr3 = App::GetApp()->GetXAudio2Manager();
							m_y--;
							if (m_y < 0) {
								m_y = (int)m_StageManager.size() - 1;
							}
							m_ScaleTime = 0.0f;
						}
						// 下に傾けた
						if (CntlVec[0].fThumbLY <= -0.5f && (CntlVec[0].fThumbLX < 0.5f&&CntlVec[0].fThumbLX > -0.5f) && !m_lr) {
							auto XAPtr4 = App::GetApp()->GetXAudio2Manager();
							m_y++;
							if (m_y >= (int)m_StageManager.size()) {
								m_y = 0;
							}
							m_ScaleTime = 0.0f;
						}
					}
					m_TotalTime += ElapsedTime;
				}
				else {
					m_TotalTime = 0.0f;
				}
				m_level = m_StageManager[m_y][m_x];

				m_ScaleTime += ElapsedTime * 2.0f;
				if (m_ScaleTime >= XM_2PI) {
					m_ScaleTime = 0;
				}
				auto objects = GetStage()->GetGameObjectVec();
				for (auto object : objects) {
					auto sprite = dynamic_cast<SelectSprite*>(object.get());
					if (sprite) {
						if (!(sprite->GetLevel() == Level::none)) {
							// 選択中のレベルなら
							if (sprite->GetLevel() == m_level) {
								// 明るくする
								sprite->UpdateColor(Col4(1.0f));
								// 大きくしたり小さくしたりする
								//sprite->UpdateScale(Vec2((sinf(m_ScaleTime) + 4.0f) / 3.0f, (sinf(m_ScaleTime) + 4.0f) / 3.0f));
							}
							// それ以外の時
							else {
								// 暗くする
								sprite->UpdateColor(Col4(0.5f));
								// 大きさを元に戻す
								//sprite->UpdateScale(Vec2(1.0f, 1.0f));
							}
						}
					}
				}
				//auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
				//リセットコマンド
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_START&&CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_BACK) {
					App::GetApp()->GetScene<Scene>()->ResetScore();
					//ScorePtr1->SetScore(PlanetCount);
					PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToTitleStage");
				}
			}
		}
	}

//--------------------------------------------------------------------------------------
///	セレクトマネージャー
//--------------------------------------------------------------------------------------
	SelectManager_G::SelectManager_G(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_TotalTime(0.0f),
		m_ScaleTime(0.0f),
		m_x(0),
		m_y(0)
	{}
	SelectManager_G::~SelectManager_G() {}

	void SelectManager_G::OnUpdate()
	{
		shared_ptr<GameOverStage> gameOverStage = nullptr;
		gameOverStage = dynamic_pointer_cast<GameOverStage>(this->GetStage());
		if (gameOverStage) {
			if (gameOverStage->GetCursorFlg()) {
				// 描画時間
				float ElapsedTime = App::GetApp()->GetElapsedTime();
				auto XAPtr = App::GetApp()->GetXAudio2Manager();
				//コントローラの取得
				auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
				// どっちかに傾けている
				if ((CntlVec[0].fThumbLX <= -0.5f || CntlVec[0].fThumbLX >= 0.5f) ||
					(CntlVec[0].fThumbLY <= -0.5f || CntlVec[0].fThumbLY >= 0.5f)
					&& m_Active == true) {
					// デッドゾーンと最初の入力
					if (m_TotalTime >= 0.15f || m_TotalTime == 0.0f) {
						m_TotalTime = 0.0f;
						// 上に傾けた
						if (CntlVec[0].fThumbLY >= 0.5f && (CntlVec[0].fThumbLX < 0.5f&&CntlVec[0].fThumbLX > -0.5f)) {
							m_position--;
							XAPtr->Start(wstringKey::seCursor, 0, 0.5f);
							if (m_position < 1) {
								m_position = 2;
							}
							m_TotalTime = 0.0f;
						}
						// 下に傾けた
						if (CntlVec[0].fThumbLY <= -0.5f && (CntlVec[0].fThumbLX < 0.5f&&CntlVec[0].fThumbLX > -0.5f)) {
							m_position++;
							XAPtr->Start(wstringKey::seCursor, 0, 0.5f);
							if (m_position > 2) {
								m_position = 1;
							}
							m_TotalTime = 0.0f;
						}
					}
					m_TotalTime += ElapsedTime;
				}
				else {
					m_TotalTime = 0.0f;
					if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
						m_Active = false;
					}
				}
			}
		}
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//リセットコマンド
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_START&&CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_BACK) {
			App::GetApp()->GetScene<Scene>()->ResetScore();
			PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToTitleStage");
		}
	}
	//--------------------------------------------------------------------------------------
	//	ゲームオーバーステージクラス実体
	//--------------------------------------------------------------------------------------
	// ビューの作成
	void GameOverStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		// ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(Vec3(0.0f, 5.0f, -5.0f));
		PtrCamera->SetAt(Vec3(0.0f, 0.0f, 0.0f));
		// マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		// デフォルトのライティングを設定
		PtrMultiLight->SetDefaultLighting();
	}

	//スプライトの作成
	void GameOverStage::CreateGameOverSprite() {
		// とりあえずBGで代用
		auto backGroundPtr = AddGameObject<Sprite>(wstringKey::txGameOverBG, true,
			Vec2(1280.0f, 800.0f), Vec2(0.0f, 0.0f));
		backGroundPtr->SetDrawLayer(-1);
		auto MFPtr = AddGameObject<Sprite>(wstringKey::txGameOver_Logo, true,
			Vec2(960.0f, 192.0f), Vec2(0.0f, 300.0f));
		MFPtr->AddTag(L"MF");
		auto RTPtr = AddGameObject<SelectSprite>(wstringKey::txGameOver_Retry, true,
			Vec2(-350.0f, -100.0f),Vec2(384.0f, 192.0f),Level::a1);
		RTPtr->SetDrawActive(false);
		RTPtr->AddTag(L"RT");
		auto TTPtr = AddGameObject<SelectSprite>(wstringKey::txGameOver_Title, true,
			Vec2(-350.0f, -250.0f),Vec2(384.0f, 192.0f), Level::b1);
		TTPtr->SetDrawActive(false);
		TTPtr->AddTag(L"TT");
		auto SMGG = AddGameObject<SelectManager_GG>();
		SetSharedGameObject(L"SMGG", SMGG);
	}
	//カーソルの生成
	void GameOverStage::CreateCursor() {
		auto cursorPtr = AddGameObject<Sprite>(wstringKey::txStageSelect_Cursor, true,
			Vec2(128.0f, 128.0f), Vec2(-570.0f, -100.0f));
		cursorPtr->AddTag(L"CP");
		cursorPtr->SetDrawActive(false);
		auto MRSMPtr = AddGameObject<SelectManager_G>();
		SetSharedGameObject(L"MRSMG", MRSMPtr);
	}

	// 初期化
	void GameOverStage::OnCreate() {
		// ビューの作成
		CreateViewLight();
		// スプライトの作成
		CreateGameOverSprite();
		//カーソルの生成
		CreateCursor();

		// スコアのリセット
		App::GetApp()->GetScene<Scene>()->ResetScore();
	}

	// 更新
	void GameOverStage::OnUpdate() {
		// コントローラチェックして入力があればコマンド呼び出し
		//m_InputHandler.PushHandle(GetThis<GameOverStage>());
		//アニメーションの呼び出し
		OnAnimation();
		//カーソル移動の呼び出し
		OnCursorMove();
	}

	void GameOverStage::OnAnimation() {
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_Time += ElapsedTime * 5.0f;
		for (auto& v : GetGameObjectVec()) {
			if (v->FindTag(L"MF")) {
				if (m_Alpha < 1.0f) {
					m_Alpha += 0.025f;
				}
				v->GetComponent<PCTSpriteDraw>()->SetDiffuse(Col4(1.0f, 1.0f, 1.0f, m_Alpha));
			}
			if (v->FindTag(L"RT") || v->FindTag(L"TT")) {
				if (m_Time > 15.0f) {
					m_CursorFlg = true;
					v->SetDrawActive(true);
				}
			}
		}
	}
	void GameOverStage::OnCursorMove() {
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_Time += ElapsedTime * 5.0f;
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		auto SM = GetSharedGameObject<SelectManager_G>(L"MRSMG");
		int cursorNum = SM->GetCursorPosition();
		wstring now = App::GetApp()->GetScene<Scene>()->GetNowScene();
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		for (auto& v : GetGameObjectVec()) {
			if (m_CursorFlg) {
				if (v->FindTag(L"CP")) {
					auto cursorTransform = v->GetComponent<Transform>();
					auto pos = cursorTransform->GetPosition();
					switch (cursorNum)
					{
					case 1:
						pos.y = -100.0f;
						cursorTransform->SetPosition(pos);
						// Aボタン入力されたらGameStageにシーン遷移
						if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
							AddGameObject<FadeOut>(L"txFade", true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
							XAPtr->Start(wstringKey::seKettei, 0, 0.5f);

						}
						OnSceneTransition(now);
						break;
					case 2:
						pos.y = -250.0f;
						cursorTransform->SetPosition(pos);
						// Aボタン入力されたらTitleStageにシーン遷移
						if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
							AddGameObject<FadeOut>(L"txFade", true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
							XAPtr->Start(wstringKey::seKettei, 0, 0.5f);

						}
						OnSceneTransition(L"ToTitleStage");
						break;
					}
					if (m_Time > 15.0f) {
						v->SetDrawActive(true);
					}
					//点滅処理
					m_Alpha_C += sin(m_Time);
					if (m_Alpha_C > 1.0f) {
						m_Alpha_C = 1.0f;
					}
					if (m_Alpha_C <= 0.3f) {
						m_Alpha_C = 0.3f;
					}
					v->GetComponent<PCTSpriteDraw>()->SetDiffuse(Col4(1.0f, 1.0f, 1.0f, m_Alpha_C));
				}
			}
		}
	}

	bool GameOverStage::GetCursorFlg() const
	{
		return m_CursorFlg;
	}

	// シーン遷移(FadeOutのAddは入力の部分で、入力の外にこれ書いてください)
	void GameOverStage::OnSceneTransition(const wstring& wstringKey)
	{
		shared_ptr<FadeOut> fadeout = nullptr;
		auto objs = GetGameObjectVec();
		for (auto& obj : objs) {
			fadeout = dynamic_pointer_cast<FadeOut>(obj);
			if (fadeout) {
				if (fadeout->GetAlpha() >= 1.0f) {
					PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), wstringKey);
				}
			}
		}

	}
	// Aボタン
	//void GameOverStage::OnPushA() {
	//	PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToGameStage");
	//}
}
//end basecross
