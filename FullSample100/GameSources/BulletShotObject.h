/*!
@file BulletShotObject.h
@brief 弾を撃つオブジェクト
*/

#pragma once
#include "stdafx.h"

namespace basecross{

	//--------------------------------------------------------------------------------------
	///	弾を撃つオブジェクト
	//--------------------------------------------------------------------------------------
	class BulletShotObject : public TestStageObject {
		Vec3 m_Angle;
		float m_Span;
		float m_Time;

		shared_ptr<GameObject> m_GagePtr;
	public :
		//構築と破棄
		//--------------------------------------------------------------------------------------
		/*!
		@brief	コンストラクタ
		@param[in]	StagePtr	ステージ
		*/
		//--------------------------------------------------------------------------------------
		BulletShotObject(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const float& Span = 0.5f
		);
		//--------------------------------------------------------------------------------------
		/*!
		@brief	デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~BulletShotObject();

		// 初期化
		virtual void OnCreate() override;
		// 更新
		virtual void OnUpdate() override;
	};


	//--------------------------------------------------------------------------------------
	///	敵用の弾オブジェクト
	//--------------------------------------------------------------------------------------
	class EnemyBullet :public GameObject
	{
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		Vec3 m_Angle;
		float m_Spead;
		//文字列の表示
		void DrawStrings();
	public:
		//コンストラクタ&デストラクタ
		EnemyBullet(const shared_ptr<Stage>& stagePtr, 
			const Vec3& scale, 
			const Vec3& rotation, 
			const Vec3& position, 
			const Vec3& angle);
		~EnemyBullet() {}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
		virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;
		virtual void OnCollisionExcute(shared_ptr<GameObject>& Other) override;
		virtual void OnCollisionExit(shared_ptr<GameObject>& Other) override;
		// オブジェクトのリセット
		void Reset(const Vec3& pos, const Vec3& angle);
	};

}
//end basecross

