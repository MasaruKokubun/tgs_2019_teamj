#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class EnergyGageBar : public GameObject;
	//	用途: エネルギーゲージバー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	EnergyGageBar::EnergyGageBar(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<Player> playerPtr) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_PlayerPtr(playerPtr)
	{}

	//初期化
	void EnergyGageBar::OnCreate() {

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		//アニメチップ作成
		float tipWidth = 1.0f;
		float tipHeight = 1.0f;
		for (int c = 0; c < 1; c++) {
			for (int r = 0; r < 1; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 0;
		m_AnimTipCol = 0;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		float maxSize = 1.0f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-0.0f, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+maxSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-0.0f, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+maxSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTSpriteDraw>(vertices, indices);
		PtrSpriteDraw->SetTextureResource(wstringKey::txEnergyBar);
		PtrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		//透明処理
		SetAlphaActive(true);
		SetDrawLayer(5);

		GetStage()->AddGameObject<EnergyGageFrame>(m_Scale, m_Rotation, Vec3(m_Scale.x / 2.0f, 0.0f, 0.0f), GetThis<GameObject>());
	}

	//更新
	void EnergyGageBar::OnUpdate() {
		if (m_PlayerPtr != nullptr) {
			auto gage = m_PlayerPtr->m_Energy;
			float elapsedTime = App::GetApp()->GetElapsedTime();
			m_delta += elapsedTime;
			float HelfSize = 0.5f;
			float maxSize = 1.0f*gage;
			Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
			auto animTip = m_AnimTip[m_AnimTipCol][m_AnimTipRow];
			vector<VertexPositionColorTexture> vertices = {
				{ Vec3(-0.0f, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
				{ Vec3(+maxSize, +HelfSize, 0), white, Vec2(animTip.right*gage, animTip.top) },
				{ Vec3(-0.0f, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
				{ Vec3(+maxSize, -HelfSize, 0), white, Vec2(animTip.right*gage, animTip.bottom) }
			};
			auto drawComp = GetComponent<PCTSpriteDraw>();
			drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);
		}
	}

	void EnergyGageBar::OnUpdate2() {
		////文字列の表示
		//DrawStrings();
	}
	void EnergyGageBar::OnDestroy() {
		SetDrawActive(false);
	}

	//--------------------------------------------------------------------------------------
	//	class EnergyGageFrame : public GameObject;
	//	用途: エネルギーゲージフレーム
	//--------------------------------------------------------------------------------------
	//構築と破棄
	EnergyGageFrame::EnergyGageFrame(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<GameObject> parentPtr) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_ParentPtr(parentPtr)
	{}

	//初期化
	void EnergyGageFrame::OnCreate() {
		auto ptrTransform = GetComponent<Transform>();
		//親子関係化
		ptrTransform->SetParent(m_ParentPtr);

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		//アニメチップ作成
		float tipWidth = 1.0f;
		float tipHeight = 1.0f;
		for (int c = 0; c < 1; c++) {
			for (int r = 0; r < 1; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 0;
		m_AnimTipCol = 0;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		float maxSize = 1.0f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTSpriteDraw>(vertices, indices);
		PtrSpriteDraw->SetTextureResource(wstringKey::txEnergyFrame);
		PtrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		//透明処理
		SetAlphaActive(true);
		SetDrawLayer(4);
	}

	//更新
	void EnergyGageFrame::OnUpdate() {

	}

	void EnergyGageFrame::OnUpdate2() {
		////文字列の表示
		//DrawStrings();
	}

	void EnergyGageFrame::OnDestroy() {
		SetDrawActive(false);
	}

}
