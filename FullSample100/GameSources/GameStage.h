/*!
@file GameStage.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage : public Stage {
		shared_ptr<SoundItem> m_BGM;
		bool m_sw = false;
		//惑星の数
		float m_PlanetCount = 0;
		int m_Digit10;
		//ビューの作成
		void CreateViewLight();
		//背景の生成
		void CreateBG();
		//テストボックスの作成
		void CreateTestBox();
		//テストプレイヤーの作成
		void CreateTestPlayer();
		//スピードUIスプライトの作成
		void CreateUISprite();
		//惑星の数のUIを表示
		void CreatePlanetUISprite();
		//数字の作成
		void CreatePlanetCounter();
		// パーティクルの作成
		void CreateParticle();
		// チュートリアルテキスト
		void CreateTutorialText();
		//テキスト更新
		void TextUpdate();
	public:
		//構築と破棄
		GameStage() :Stage() {}
		virtual ~GameStage() {}
		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;
		virtual void OnDestroy()override;


	};

}
//end basecross

