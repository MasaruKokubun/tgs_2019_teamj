#pragma once
#include "stdafx.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	///	衛星付き天体オブジェクト
	//--------------------------------------------------------------------------------------
	class SatelliteWithPlanet : public TestStageObject
	{
		shared_ptr<GameObject> m_GagePtr;
	public:
		// コンストラクタ
		SatelliteWithPlanet(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position);
		~SatelliteWithPlanet() {}

		virtual void OnCreate() override;
		virtual void OnUpdate() override;
	};

	//--------------------------------------------------------------------------------------
	///	衛星付き天体オブジェクトチルドレン
	//--------------------------------------------------------------------------------------
	class Satellite : public GameObject
	{
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;

		const shared_ptr<GameObject>& m_ParentPtr;
		Vec3 m_ParentPosition;

		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[6][6];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;

		float m_Speed;
		float m_Rad;
		float m_Direction;
		float m_Dis;
	public:
		bool m_HitFlg;
		bool m_IntervalFlg;
		float m_IntervalTime;

		Satellite(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, const shared_ptr<GameObject>& parentPtr = nullptr, int Row = 1, int Col = 0);
		~Satellite() {}
		
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

		const float SetAngle(float& angle)
		{
			return angle;
		}		
		const float SetDis(float& dis)
		{
			return dis;
		}
	};
}