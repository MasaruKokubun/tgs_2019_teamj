#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class GoalMarker : public GameObject;
	//	用途: ゴールマーカー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	GoalMarker::GoalMarker(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<GameObject> parentPtr) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_ParentPtr(parentPtr)
	{
	}
	//初期化
	void GoalMarker::OnCreate() {
		auto ptrTransform = GetComponent<Transform>();
		////親子関係化
		//ptrTransform->SetParent(m_ParentPtr);

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		//CollisionSphere衝突判定を付ける
		auto ptrColl = AddComponent<CollisionSphere>();
		ptrColl->SetAfterCollision(AfterCollision::None);

		//Mat4x4 matrix;
		//auto ptrStaticModelDraw = AddComponent<BcPNTStaticModelDraw>();
		////auto ptrBoneModelDraw = AddComponent<BcPNTBoneModelDraw>();
		////マトリクス
		//matrix.affineTransformation(
		//	Vec3(1.0f, 1.0f, 1.0f),	//スケール
		//	Vec3(0.0, 0.0, 0.0),	//原点位置
		//	Vec3(0 * (XM_PI / 180), 0 * (XM_PI / 180), 180.0f * (XM_PI / 180)),					//角度
		//	Vec3(0.0f, 0.0f, 0.0f)	//位置
		//);
		////モデルの描画
		//ptrStaticModelDraw->SetMeshToTransformMatrix(matrix);
		//ptrStaticModelDraw->SetFogEnabled(true);

		//アニメチップ
		float tipWidth = 1.0f;
		float tipHeight = 126.0f/378.0f;
		for (int c = 0; c < 3; c++) {
			for (int r = 0; r < 1; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 0;
		m_AnimTipCol = 0;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);
		PtrSpriteDraw->SetTextureResource(wstringKey::txArrow);
		//PtrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		//透明処理
		SetAlphaActive(true);

		SetDrawLayer(4);
		m_SeFlg = false;

		m_GoalPos = GetStage()->GetSharedGameObject<GoalArea>(wstringKey::obGoal)->GetComponent<Transform>()->GetPosition();
		////文字列をつける
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
	}

	//更新
	void GoalMarker::OnUpdate() {
		auto thisTrans = GetComponent<Transform>();
		auto playerTrans = m_ParentPtr->GetComponent<Transform>();
		auto playerPos = m_ParentPtr->GetComponent<Transform>()->GetPosition();
		float elapsedTime = App::GetApp()->GetElapsedTime();
		//向きを計算
		Vec3 front = m_GoalPos - playerPos;
		//向きからの角度を算出
		float FrontAngle = atan2(front.y, front.x);
		auto deg = FrontAngle * 180.0f / XM_PI;
		//deg += 360.0f;
		deg *= XM_PI / 180.0f;
		thisTrans->SetQuaternion(Quat(0.0f, 0.0f, -1.0f, deg));
		//auto qt = thisTrans->GetQuaternion();
		//qt *= Quat(0.0f, 0.0f, -1.0f, 90.0f*(XM_PI / 180.0f));
		//thisTrans->SetQuaternion(qt);
		//周回
		//m_Rad -= 1.0f*elapsedTime;
		//if (m_Rad <= 0.0f) {
		//	m_Rad = 0.0f;
		//}
		auto playerPtr = dynamic_pointer_cast<Player>(m_ParentPtr);
		auto a = sqrtf(front.x*front.x + front.y*front.y );
		if (sqrtf(front.x*front.x + front.y*front.y) < 4.0f || playerPtr->GetStateMachine()->GetCurrentState() == PlayerAcceleration::Instance()|| playerPtr->GetStateMachine()->GetCurrentState() == PlayerDead::Instance()) {
			SetDrawActive(false);
		}
		else
		{
			SetDrawActive(true);
		}

		auto Rad = 1.0f;
		auto movePos = Vec3(cos(FrontAngle) * Rad + playerPos.x, sin(FrontAngle) * Rad + playerPos.y, 0.0f);
		GetComponent<Transform>()->SetPosition(movePos);
		//Deg *= 180.0f / XM_PI;
		//Deg += 180.0f*elapsedTime;
		//if (Deg > 360.0f) {
		//	Deg = 0.0f;
		//}
		//Deg *= XM_PI / 180.0f;

		m_delta += elapsedTime;
		float HelfSize = 0.5f;
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		auto animTip = m_AnimTip[m_AnimTipCol][m_AnimTipRow];
		vector<VertexPositionColorTexture> vertices = {
			{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
			{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(animTip.right, animTip.top) },
			{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
			{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(animTip.right, animTip.bottom) }
		};

		auto drawComp = GetComponent<PCTStaticDraw>();
		drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);
		if (m_delta >= 0.05f) {
			m_delta = 0.0f;
			if (m_AnimTipCol < 2) {
				m_AnimTipCol++;
			}
			else {
				m_AnimTipCol = 0;
			}
		}


	}
	void GoalMarker::OnUpdate2() {
		////文字列の表示
		//DrawStrings();
	}

	//文字列の表示
	void GoalMarker::DrawStrings() {

		//文字列表示
		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring fpsStr(L"FPS: ");
		fpsStr += Util::UintToWStr(fps);
		fpsStr += L"\nElapsedTime: ";
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		fpsStr += Util::FloatToWStr(ElapsedTime);
		fpsStr += L"\n";

		auto pos = GetComponent<Transform>()->GetPosition();
		wstring positionStr(L"Position:\t");
		positionStr += L"X=" + Util::FloatToWStr(pos.x, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Y=" + Util::FloatToWStr(pos.y, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Z=" + Util::FloatToWStr(pos.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring speadStr(L"Spead:\t");
		//auto gravVelocity = GetComponent<Gravity>()->GetGravityVelocity();
		//speadStr += Util::FloatToWStr(m_Spead*100.0f, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Y=" + Util::FloatToWStr(gravVelocity.y, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Z=" + Util::FloatToWStr(gravVelocity.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring str = fpsStr + positionStr + speadStr;
		//文字列コンポーネントの取得
		auto ptrString = GetComponent<StringSprite>();
		ptrString->SetText(str);
	}
}