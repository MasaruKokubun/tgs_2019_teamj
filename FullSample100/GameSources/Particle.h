/*!
@file Particle.h
@brief パーティクル
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	爆発パーティクルクラス
	//--------------------------------------------------------------------------------------
	class BombParticle : public MultiParticle
	{
	public :
		// 構築と破棄
		BombParticle(const shared_ptr<Stage>& StagePtr);
		virtual ~BombParticle() {}
		// 初期化
		virtual void OnCreate() override;
		void InsertBomb(const Vec3& Pos, const wstring& wstringKey);

	};

	//--------------------------------------------------------------------------------------
	//	回復用パーティクルクラス
	//--------------------------------------------------------------------------------------
	class RecoveryParticle : public MultiParticle
	{
	public:
		// 構築と破棄
		RecoveryParticle(const shared_ptr<Stage>& StagePtr);
		virtual ~RecoveryParticle() {}
		// 初期化
		virtual void OnCreate() override;
		void InsertRecovery(const Vec3& Pos);
	};

}
//end basecross

