/*!
@file Player.h
@brief プレイヤーなど
*/

#pragma once
#include "stdafx.h"

namespace basecross{

	//--------------------------------------------------------------------------------------
	///	プレイヤー
	//--------------------------------------------------------------------------------------
	class Player : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		// コントローラから方向ベクトルを得る
		Vec3 GetMoveVector();
		//文字列の表示
		void DrawStrings();
		//入力ハンドラー
		InputHandler<Player> m_InputHandler;
		//速度
		Vec3 m_Velocity;
		//ステートマシン
		unique_ptr<StateMachine<Player>>  m_StateMachine;
		//ゴールとの当たり判定
		bool m_GoalHitFlg;
		//子オブジェクト
		shared_ptr<GameObject> m_RadarPtr;
		shared_ptr<GameObject> m_SearchAreaPtr;
		shared_ptr<GameObject> m_GoalMarkerPtr;

	public:
		float m_Energy;
		shared_ptr<MyCamera> m_ptrMyCamera;
		float m_MaxSpead;
		float m_Spead;
		Vec3 m_Angle;
		float m_delta;
		bool m_OnHoldA;
		//サウンド
		shared_ptr<XAudio2Manager> m_AudioManager;
		shared_ptr<SoundItem> m_SE = nullptr;
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[4][4];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
		//構築と破棄
		//--------------------------------------------------------------------------------------
		/*!
		@brief	コンストラクタ
		@param[in]	StagePtr	ステージ
		*/
		//--------------------------------------------------------------------------------------
		Player(const shared_ptr<Stage>& StagePtr, const Vec3& scale = Vec3(0.5f, 0.5f, 0.5f), const Vec3& rotation = Vec3(0.0f, 0.0f, 0.0f), const Vec3& position = Vec3(1.0f, 7.5f, 0.0f), Vec3 angle = Vec3(1.0f, 0.0001f, 0.0f));
		//--------------------------------------------------------------------------------------
		/*!
		@brief	デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~Player() {}
		//アクセサ
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
		virtual void OnDestroy()override;
		virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;
		virtual void OnCollisionExcute(shared_ptr<GameObject>& Other) override;
		virtual void OnCollisionExit(shared_ptr<GameObject>& Other) override;
		//Aボタン
		void OnPushA();
		void OnHoldA();
		void OnRelease();
		//Xボタン
		void OnPushX();
		//移動
		void MovePlayer();
		//ダメージ
		void Damage(float damage);
		//回復
		void Recovery(float recovery);
		//ステートマシンの取得
		unique_ptr< StateMachine<Player>>& GetStateMachine() {
			return m_StateMachine;
		}
		//GoalHitFlgのGetSet
		bool GetGoalHitFlg() {
			return m_GoalHitFlg;
		}
		void SetGoalHitFlg(bool flg) {
			m_GoalHitFlg = flg;
		}
		//RadarPtrのGetSet
		shared_ptr<GameObject> GetRadar() {
			return m_RadarPtr;
		}
		//RadarPtrのGetSet
		shared_ptr<GameObject> GetSearchArea() {
			return m_SearchAreaPtr;
		}
		//RadarPtrのGetSet
		shared_ptr<GameObject> GetGoalMarker() {
			return m_GoalMarkerPtr;
		}

	};
	//プレイヤーステート

	//--------------------------------------------------------------------------------------
	/// デフォルト
	//--------------------------------------------------------------------------------------
	class PlayerDefault : public ObjState<Player> {
		PlayerDefault() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(PlayerDefault)
		void Enter(const shared_ptr<Player>& obj) override;
		void Execute(const shared_ptr<Player>& obj) override;
		void Exit(const shared_ptr<Player>& obj) override;
	};
	//--------------------------------------------------------------------------------------
	/// 加速
	//--------------------------------------------------------------------------------------
	class PlayerAcceleration : public ObjState<Player> {
		PlayerAcceleration() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(PlayerAcceleration)
		void Enter(const shared_ptr<Player>& obj) override;
		void Execute(const shared_ptr<Player>& obj) override;
		void Exit(const shared_ptr<Player>& obj) override;
	};
	//--------------------------------------------------------------------------------------
	/// ブースト
	//--------------------------------------------------------------------------------------
	class PlayerBoost : public ObjState<Player> {
		PlayerBoost() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(PlayerBoost)
		void Enter(const shared_ptr<Player>& obj) override;
		void Execute(const shared_ptr<Player>& obj) override;
		void Exit(const shared_ptr<Player>& obj) override;
	};

	//--------------------------------------------------------------------------------------
	/// スタート
	//--------------------------------------------------------------------------------------
	class PlayerStart : public ObjState<Player> {
		PlayerStart() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(PlayerStart)
		void Enter(const shared_ptr<Player>& obj) override;
		void Execute(const shared_ptr<Player>& obj) override;
		void Exit(const shared_ptr<Player>& obj) override;
	};

	//--------------------------------------------------------------------------------------
	/// クリア
	//--------------------------------------------------------------------------------------
	class PlayerClear : public ObjState<Player> {
		PlayerClear() {}
	public:
		shared_ptr<GameObject> m_GoalPtr;
		Vec3 m_GoalPos;

		float m_Deg; //回転角度
		float m_Rad; //回転半径
		float m_RadMax;

		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(PlayerClear)
		void Enter(const shared_ptr<Player>& obj) override;
		void Execute(const shared_ptr<Player>& obj) override;
		void Exit(const shared_ptr<Player>& obj) override;
	};

	//--------------------------------------------------------------------------------------
	/// デッド
	//--------------------------------------------------------------------------------------
		class PlayerDead : public ObjState<Player> {
			PlayerDead() {}
			shared_ptr<GameObject> m_CutInPtr;
		public:
			//ステートのインスタンス取得
			DECLARE_SINGLETON_INSTANCE(PlayerDead)
			void Enter(const shared_ptr<Player>& obj) override;
			void Execute(const shared_ptr<Player>& obj) override;
			void Exit(const shared_ptr<Player>& obj) override;
		};

}
//end basecross

