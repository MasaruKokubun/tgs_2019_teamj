#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	///	テストステージブジェクト
	//--------------------------------------------------------------------------------------
	class SearchArea :public GameObject
	{
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		shared_ptr<GameObject> m_ParentPtr;
		//文字列の表示
		void DrawStrings();
		//// アニメーション用のデータ
		//Rect2D<float> m_AnimTip[4][4];
		//int m_AnimTipCol, m_AnimTipRow;
		//float m_AnimTime;
	public:
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[1][7];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
		float m_delta;
		//サウンド
		shared_ptr<XAudio2Manager> m_AudioManager;
		shared_ptr<SoundItem> m_SE;
		bool m_SeFlg;
		//コンストラクタ&デストラクタ
		SearchArea(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<GameObject> parentPtr=nullptr);
		~SearchArea() {}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
		virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;
		virtual void OnCollisionExcute(shared_ptr<GameObject>& Other) override;
		virtual void OnCollisionExit(shared_ptr<GameObject>& Other) override;
	};
}