/*!
@file Fade.cpp
@brief フェード実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {


	//--------------------------------------------------------------------------------------
	//	フェードアウトクラス
	//--------------------------------------------------------------------------------------
	// 構築と破棄
	FadeOut::FadeOut(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, const bool Trace, const Vec2& Scale, const Vec2& Pos)
		:
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Pos(Pos),
		m_Alpha(0.0f),
		m_FadeFlg(false)
	{}


	// 初期化
	void FadeOut::OnCreate()
	{
		AddTag(L"FadeOut");

		float HalfSize = 0.5f;
		Col4 white(1.0f, 1.0f, 1.0f, m_Alpha);
		// 頂点配列
		vector<VertexPositionColorTexture> vertex = {
			{VertexPositionColorTexture(Vec3(-HalfSize, HalfSize, 0), white, Vec2(0.0f, 0.0f))},
			{VertexPositionColorTexture(Vec3(HalfSize, HalfSize, 0), white, Vec2(1.0f, 0.0f))},
			{VertexPositionColorTexture(Vec3(-HalfSize, -HalfSize, 0), white, Vec2(0.0f, 1.0f))},
			{VertexPositionColorTexture(Vec3(HalfSize, -HalfSize, 0), white, Vec2(1.0f, 1.0f))}
		};
		// インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(true);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_Pos.x, m_Pos.y, 0.0f);
		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(vertex, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(m_TextureKey);

		SetDrawLayer(1001);

		// フェード中の他オブジェクト,ステージの動き
		OtherUpdateActive(false);
		SetFadeFlg(true);
	}

	// 更新
	void FadeOut::OnUpdate()
	{
		if (m_FadeFlg) {
			float elapsedTime = App::GetApp()->GetElapsedTime();

			auto PtrDraw = GetComponent<PCTSpriteDraw>();
			m_Alpha += 0.5f * elapsedTime;
			PtrDraw->SetDiffuse(Col4(1.0f, 1.0f, 1.0f, m_Alpha));
			if (m_Alpha >= 1.0f) {
				m_Alpha = 1.0f;
				m_FadeFlg = false;
				OtherUpdateActive(true);
			}
		}
	}

	// フェード中の他オブジェクト,ステージの動き
	void FadeOut::OtherUpdateActive(bool active) {
		for (auto& obj : this->GetStage()->GetGameObjectVec()) {
			obj->SetUpdateActive(active);
		}
		shared_ptr<TitleStage> title = nullptr;
		title = dynamic_pointer_cast<TitleStage>(this->GetStage());
		if (!title) {
			this->GetStage()->SetUpdateActive(active);
		}
		
	}

	// ゲッターとセッター
	void FadeOut::SetFadeFlg(bool fadeFlg)
	{
		m_FadeFlg = fadeFlg;
	}

	float FadeOut::GetAlpha() const
	{
		return m_Alpha;
	}

	bool FadeOut::GetFadeFlg() const
	{
		return m_FadeFlg;
	}

	//--------------------------------------------------------------------------------------
	//	フェードインクラス
	//--------------------------------------------------------------------------------------
	// 構築と破棄
	FadeIn::FadeIn(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, const bool Trace, const Vec2& Scale, const Vec2& Pos)
		:
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_Scale(Scale),
		m_Pos(Pos),
		m_Alpha(1.0f),
		m_FadeFlg(false)
	{}

	// 初期化
	void FadeIn::OnCreate()
	{
		AddTag(L"FadeIn");

		float HalfSize = 0.5f;
		Col4 white(1.0f, 1.0f, 1.0f, m_Alpha);
		// 頂点配列
		vector<VertexPositionColorTexture> vertex = {
			{VertexPositionColorTexture(Vec3(-HalfSize, HalfSize, 0), white, Vec2(0.0f, 0.0f))},
			{VertexPositionColorTexture(Vec3(HalfSize, HalfSize, 0), white, Vec2(1.0f, 0.0f))},
			{VertexPositionColorTexture(Vec3(-HalfSize, -HalfSize, 0), white, Vec2(0.0f, 1.0f))},
			{VertexPositionColorTexture(Vec3(HalfSize, -HalfSize, 0), white, Vec2(1.0f, 1.0f))}
		};
		// インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(true);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_Scale.x, m_Scale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_Pos.x, m_Pos.y, 0.0f);
		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(vertex, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(m_TextureKey);

		SetDrawLayer(1001);

		// フェード中の他オブジェクト,ステージの動き
		OtherUpdateActive(false);
		SetFadeFlg(true);

		m_StartSprite = GetStage()->AddGameObject<StartSprite>(L"TX_StartA", true, Vec2(512.0f, 128.0f), Vec2(0.0f, 0.0f));
	}

	// 更新
	void FadeIn::OnUpdate()
	{
		if (m_FadeFlg) {
			float elapsedTime = App::GetApp()->GetElapsedTime();

			auto PtrDraw = GetComponent<PCTSpriteDraw>();
			m_Alpha -= 0.5f * elapsedTime;
			if (m_Alpha <= 0.0f) {
				//OtherUpdateActive(true);

				m_Alpha = 0.0f;
				// 現在のステージにplayerはいるか
				shared_ptr<Player> player = nullptr;
				for (auto& obj : this->GetStage()->GetGameObjectVec())
				{
					player = dynamic_pointer_cast<Player>(obj);
					if (player)
					{
						break;
					}
				}

				if (player)
				{
					// playerがいたのであればStartSpriteを表示
					m_StartSprite->SetDrawFlg(true);
					if (m_StartSprite->GetStartFlg())
					{
						m_StartSprite->SetDrawFlg(false);
						OtherUpdateActive(true);
						m_FadeFlg = false;
						GetStage()->RemoveGameObject<GameObject>(GetThis<GameObject>());
					}
				}
				else
				{
					OtherUpdateActive(true);
					m_FadeFlg = false;
					GetStage()->RemoveGameObject<GameObject>(GetThis<GameObject>());
				}


			}
			PtrDraw->SetDiffuse(Col4(1.0f, 1.0f, 1.0f, m_Alpha));
		}
	}

	// フェード中の他オブジェクト,ステージの動き
	void FadeIn::OtherUpdateActive(bool active) {
		for (auto& obj : this->GetStage()->GetGameObjectVec()) {
			//オブジェクト部分は"StringSprite"（文字列表示）の部分を全て消せば止められることが分かりました
			obj->SetUpdateActive(active);	
		}
		this->GetStage()->SetUpdateActive(active);
	}

	// ゲッターとセッター
	void FadeIn::SetFadeFlg(bool fadeFlg)
	{
		m_FadeFlg = fadeFlg;
	}

	float FadeIn::GetAlpha() const
	{
		return m_Alpha;
	}

	bool FadeIn::GetFadeFlg() const
	{
		return m_FadeFlg;
	}


}
//end basecross


// フェードアウトクラスに対して私がやったステージごとの動きの例です OnUpdate()内
//if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
//	AddGameObject<FadeOut>(wstringKey::txSky, true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
//}

//shared_ptr<FadeOut> fadeout = nullptr;
//auto objs = GetGameObjectVec();
//for (auto& obj : objs) {
//	fadeout = dynamic_pointer_cast<FadeOut>(obj);
//	if (fadeout) {
//		if (fadeout->GetAlpha() >= 1.0f) {
//			PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToGameStage");
//		}
//	}
//}

// フェードインの方はほかのゲームオブジェクトの動きを止める以外の処理はしました
// フェードインクラスに対して私がやったステージごとの動きの例です OnCreate()内
// AddGameObject<FadeIn>(wstringKey::txSky, true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));