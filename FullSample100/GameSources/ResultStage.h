/*!
@file GameStage.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
//	セレクトマネージャー
//--------------------------------------------------------------------------------------
	class SelectManager_RR : public GameObject {
		shared_ptr<SoundItem> m_BGM;
		float m_vol = 0.1f;
	private:
		Level m_level;	// 今選択しているレベル
		float m_TotalTime;	// 選択する時に使う
		float m_ScaleTime;	// スケールを変更する用
		int m_x;
		int m_y;
		bool m_lr = false;
		vector<vector<Level>> m_StageManager;	// 選択する時に使う
		bool m_fadeFlag = false;
		bool m_fadeoutFlag = true;

	public:
		// 構築と破壊
		SelectManager_RR(const shared_ptr<Stage>& StagePtr);
		virtual ~SelectManager_RR();
		//初期化
		virtual void OnCreate()override;
		// 更新
		virtual void OnUpdate()override;
		//// 後更新
		//virtual void OnUpdate2()override;
		//// 文字列の表示
		//void DrawStrings();
		//セッターとゲッター
		bool GetFadeFlag() { return m_fadeFlag; };
		void SetFadeFlag(bool flag) { m_fadeFlag = flag; };
		bool GetFadeoutFlag() { return m_fadeoutFlag; };
		void SetFadeoutFlag(bool flag) { m_fadeoutFlag = flag; };
		// レベルの取得
		Level GetLevel() const { return m_level; }
	};
//--------------------------------------------------------------------------------------
//	セレクトマネージャー
//--------------------------------------------------------------------------------------
	class SelectManager_R : public GameObject {
	private:
		float m_TotalTime;	// 選択する時に使う
		float m_ScaleTime;	// スケールを変更する用
		int m_x;
		int m_y;
		//現在のカーソルの位置
		int m_position = 1;
		bool m_Active = true;
	public:
		// 構築と破壊
		SelectManager_R(const shared_ptr<Stage>& StagePtr);
		virtual ~SelectManager_R();
		//初期化
		virtual void OnCreate()override {}
		// 更新
		virtual void OnUpdate()override;
		// 後更新
		virtual void OnUpdate2()override {}
		// 文字列の表示
		void DrawStrings() {}
		//ゲッター
		int GetCursorPosition() { return m_position; };
		//セッター
		void SetCursorPosition(int num) { m_position = num; };
	};

	//--------------------------------------------------------------------------------------
	//	リザルトステージクラス
	//--------------------------------------------------------------------------------------
	class ResultStage : public Stage {
		float m_StartPosX = 1000.0f;
		float m_FinishPosX = -350.0f;
		float m_Time = 0.0f;
		float m_Alpha_C = 0.0f;
		float m_Digit10;
		float rankNum = 0;
		bool seFlg = false;
		bool createFlg = false;
		int rankFlg;
		bool m_AnimFlg;
		bool m_CursorFlg = false;


		// ビューの作成
		void CreateViewLight();
		// スプライトの作成
		void CreateResultSprite();
		//数字スプライトの作成
		void CreateResultNumSprite();
		//カーソルの作成
		void CreateCursor();
		//ボタンの作成
		void CreateButton();
		// 入力ハンドラー
		//InputHandler<ResultStage> m_InputHandler;
		// シーン遷移
		void OnSceneTransition(const wstring& wstringKey);
	public:
		// 構築と破棄
		ResultStage() : Stage(), m_AnimFlg(false) {}
		virtual ~ResultStage() {}
		//初期化
		virtual void OnCreate() override;
		// 更新
		virtual void OnUpdate() override;
		//アニメーション
		void OnAnimation();
		//カーソル移動
		void OnCursorMove();

		// ゲッター
		bool GetCursorFlg() const;

		// Aボタン
		//void OnPushA();
	};

	//--------------------------------------------------------------------------------------
	///	ランク
	//--------------------------------------------------------------------------------------
	class Rank : public GameObject
	{
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;

		bool m_Destroyflg;
		float m_Time = 0.0f;
		bool m_SEFlg = false;
		bool m_AnimFlg;
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[1][4];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
	public:
		//コンストラクタ&デストラクタ
		Rank(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position);
		Rank(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, const bool& AnimFlgPtr, const int& AnimTipColPtr, const int& AinmTipRowPtr);
		~Rank() {}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;

		void SetAnimTip(const int& AnimTipColPtr, const int& AnimTipRowPtr) {
			m_AnimTipCol = AnimTipColPtr;
			m_AnimTipRow = AnimTipRowPtr;
		}
	};
}
//end basecross

