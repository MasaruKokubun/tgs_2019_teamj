#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	//	class SatelliteWithPlanet : public GameObject;
	//	用途: 衛星付き天体オブジェクト
	//--------------------------------------------------------------------------------------
	SatelliteWithPlanet::SatelliteWithPlanet(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position) :
		TestStageObject(stagePtr, scale, rotation, position, 5, 1),
		m_GagePtr(nullptr)
	{}

	// 初期化
	void SatelliteWithPlanet::OnCreate() {
		TestStageObject::OnCreate();
		this->GetStage()->AddGameObject<Satellite>(m_Scale * 0.3f, m_Rotation, m_Position, GetThis<GameObject>(), 5 ,1);	
	}

	void SatelliteWithPlanet::OnUpdate() {
		TestStageObject::OnUpdate();

	}

	Satellite::Satellite(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, const shared_ptr<GameObject>& parentPtr, int Col, int Row) :
		GameObject(stagePtr),
		m_Scale(scale * 0.4f),
		m_Rotation(rotation),
		m_Position(position),
		m_ParentPtr(parentPtr),
		m_AnimTipCol(Col),
		m_AnimTipRow(Row),
		m_Speed(0.4f),
		m_Rad(90.0f * XM_PI / 180.0f),
		m_Direction(1.0f),
		m_Dis(3.0f)
	{}

	void Satellite::OnCreate() {
		m_ParentPosition = m_ParentPtr->GetComponent<Transform>()->GetPosition();
		// 初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		// CollisionSphere衝突判定をつける
		auto ptrColl = AddComponent<CollisionSphere>();
		ptrColl->SetAfterCollision(AfterCollision::None);
		ptrColl->SetSleepActive(true);
		ptrColl->SetSleepTime(0.5f);
		//タグをつける
		AddTag(wstringKey::tagSatellite);

		//アニメチップ作成
		float tipWidth = 128.0f / 768.0f;
		float tipHeight = 128.0f / 768.0f;
		for (int c = 0; c < 6; c++) {
			for (int r = 0; r < 6; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		//m_AnimTipRow = 1;
		//m_AnimTipCol = 0;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		float maxSize = 1.0f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);
		PtrSpriteDraw->SetTextureResource(wstringKey::txPlanets);

		SetAlphaActive(true);

		SetDrawLayer(3);
	}

	void Satellite::OnUpdate() {
		float elapsedTime = App::GetApp()->GetElapsedTime();

		if (m_IntervalFlg) {
			m_IntervalTime += elapsedTime;
			if (m_IntervalTime >= 0.5f) {
				m_IntervalFlg = false;
				m_IntervalTime = 0.0f;
				m_HitFlg = false;
			}
		}

		auto ptr = this->GetComponent<Transform>();

		m_Rad += m_Speed * elapsedTime * m_Direction;
		m_Position.x = m_Dis * sinf(m_Rad) + m_ParentPosition.x;
		m_Position.y = m_Dis * cosf(m_Rad) + m_ParentPosition.y;
		ptr->SetPosition(m_Position.x, m_Position.y, 0.0f);

	}
}