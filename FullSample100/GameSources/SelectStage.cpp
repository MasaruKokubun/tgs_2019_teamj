/*!
@file SelectStage.cpp
@brief ゲームセレクトステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	//	セレクトマネージャー
	//--------------------------------------------------------------------------------------
	SelectManager::SelectManager(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_level(Level::a1),
		m_TotalTime(0.0f),
		m_ScaleTime(0.0f),
		m_x(0),
		m_y(0)
	{}
	SelectManager::~SelectManager() {}

	void SelectManager::OnCreate() {
		// これいる？
		m_StageManager = {
			{ Level::a1,Level::a2 },
			//{ Level::a2},
			{ Level::b1,Level::a2 },
			//{ Level::b2},
			{ Level::c1,Level::a2 },
			{ Level::d1,Level::a2 },
			{ Level::e1,Level::a2 },
			{ Level::f1,Level::a2 },
			//{ Level::g1,Level::g2 }
			//{ Level::c2},
			//{ Level::d1}
		};

		// デバッグ用の文字表示コンポーネントを追加する
		//auto debugStrComp = AddComponent<StringSprite>();
		//debugStrComp->SetTextRect(Rect2D<float>(10, 20, 400, 300)); // テキストの表示エリア
		//debugStrComp->SetBackColor(Col4(0, 0, 0, 0.25f)); // テキストエリアの背景色(RGBA)
	}

	void SelectManager::OnUpdate() {
		// 描画時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto ScenePtr = App::GetApp()->GetScene<Scene>();
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		// どっちかに傾けている
		if ((CntlVec[0].fThumbLX <= -0.5f || CntlVec[0].fThumbLX >= 0.5f) ||
			(CntlVec[0].fThumbLY <= -0.5f || CntlVec[0].fThumbLY >= 0.5f))
		{
			// デッドゾーンと最初の入力
			if (m_TotalTime >= 0.15f || m_TotalTime == 0.0f) {
				m_TotalTime = 0.0f;
				// 上に傾けた
				if (CntlVec[0].fThumbLY >= 0.5f && (CntlVec[0].fThumbLX < 0.5f&&CntlVec[0].fThumbLX > -0.5f) && !m_lr) {
					auto XAPtr3 = App::GetApp()->GetXAudio2Manager();
					m_y--;
					if (m_y < 0) {
						m_y = (int)m_StageManager.size() - 1;
					}
					m_ScaleTime = 0.0f;
				}
				// 下に傾けた
				if (CntlVec[0].fThumbLY <= -0.5f && (CntlVec[0].fThumbLX < 0.5f&&CntlVec[0].fThumbLX > -0.5f) && !m_lr) {
					auto XAPtr4 = App::GetApp()->GetXAudio2Manager();
					m_y++;
					if (m_y >= (int)m_StageManager.size()) {
						m_y = 0;
					}
					m_ScaleTime = 0.0f;
				}
			}
			m_TotalTime += ElapsedTime;
		}
		else {
			m_TotalTime = 0.0f;
		}
		m_level = m_StageManager[m_y][m_x];

		m_ScaleTime += ElapsedTime * 2.0f;
		if (m_ScaleTime >= XM_2PI) {
			m_ScaleTime = 0;
		}
		auto objects = GetStage()->GetGameObjectVec();
		for (auto object : objects) {
			auto sprite = dynamic_cast<SelectSprite*>(object.get());
			if (sprite) {
				if (!(sprite->GetLevel() == Level::none)) {
					// 選択中のレベルなら
					if (sprite->GetLevel() == m_level) {
						// 明るくする
						sprite->UpdateColor(Col4(1.0f));
						// 大きくしたり小さくしたりする
						//sprite->UpdateScale(Vec2((sinf(m_ScaleTime) + 4.0f) / 3.0f, (sinf(m_ScaleTime) + 4.0f) / 3.0f));
					}
					// それ以外の時
					else {
						// 暗くする
						sprite->UpdateColor(Col4(0.5f));
						// 大きさを元に戻す
						//sprite->UpdateScale(Vec2(1.0f, 1.0f));
					}
				}
			}
		}
		//auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//リセットコマンド
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_START&&CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_BACK) {
			App::GetApp()->GetScene<Scene>()->ResetScore();
			//ScorePtr1->SetScore(PlanetCount);
			PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToTitleStage");
		}

	}
//--------------------------------------------------------------------------------------
///	カーソルセレクトマネージャー
//--------------------------------------------------------------------------------------
	SelectManager_C::SelectManager_C(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_TotalTime(0.0f),
		m_ScaleTime(0.0f),
		m_x(0),
		m_y(0)
	{}
	SelectManager_C::~SelectManager_C() {}

	void SelectManager_C::OnUpdate()
	{
		// 描画時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		// どっちかに傾けている
		if ((CntlVec[0].fThumbLX <= -0.5f || CntlVec[0].fThumbLX >= 0.5f) ||
			(CntlVec[0].fThumbLY <= -0.5f || CntlVec[0].fThumbLY >= 0.5f)
			&& m_Active == true) {
			// デッドゾーンと最初の入力
			if (m_TotalTime >= 0.15f || m_TotalTime == 0.0f) {
				m_TotalTime = 0.0f;
				// 上に傾けた
				if (CntlVec[0].fThumbLY >= 0.5f && (CntlVec[0].fThumbLX < 0.5f&&CntlVec[0].fThumbLX > -0.5f)) {
					m_position--;
					XAPtr->Start(wstringKey::seCursor, 0, 0.5f);
					if (m_position < 1) {
						m_position = 6;
					}
					m_TotalTime = 0.0f;
				}
				// 下に傾けた
				if (CntlVec[0].fThumbLY <= -0.5f && (CntlVec[0].fThumbLX < 0.5f&&CntlVec[0].fThumbLX > -0.5f)) {
					m_position++;
					XAPtr->Start(wstringKey::seCursor, 0, 0.5f);
					if (m_position > 6) {
						m_position = 1;
					}
					m_TotalTime = 0.0f;
				}
			}
			m_TotalTime += ElapsedTime;
		}
		else {
			m_TotalTime = 0.0f;
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
				m_Active = false;
			}
		}


	}
	//--------------------------------------------------------------------------------------
	//	セレクトステージクラス実体
	//--------------------------------------------------------------------------------------
	// ビューの作成
	void SelectStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		// ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(Vec3(0.0f, 5.0f, -5.0f));
		PtrCamera->SetAt(Vec3(0.0f, 0.0f, 0.0f));
		// マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		// デフォルトのライティングを設定
		PtrMultiLight->SetDefaultLighting();
	}

	//スプライトの作成
	void SelectStage::CreateSelectSprite() {
		// とりあえずBGで代用
		auto backGroundPtr = AddGameObject<Sprite>(wstringKey::txBackGround, true,
			Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
		backGroundPtr->SetDrawLayer(-1);
		Vec2 spriteScale = Vec2(768.0f, 256.0f) / 3 * 2;
		Vec2 imageScale = Vec2(768.0f, 256.0f);
		Vec2 imageDiviCount = Vec2(2.0f, 4.0f);
		auto ButtonPtr = AddGameObject<SelectSprite>(wstringKey::txStageSelect_Button, true,
			Vec2(-300.0f, 300.0f), spriteScale, Level::a1);
		auto selectPtr = AddGameObject<Sprite>(wstringKey::txStageSelectNo, true,
			Vec2(384.0f, 64.0f), Vec2(-300.0f, 300.0f), 0, 0, imageScale, imageDiviCount);
		auto ButtonPtr1 = AddGameObject<SelectSprite>(wstringKey::txStageSelect_Button,true,
			Vec2(-300.0f, 200.0f), spriteScale, Level::b1);
		auto selectPtr1 = AddGameObject<Sprite>(wstringKey::txStageSelectNo, true,
			Vec2(384.0f, 64.0f), Vec2(-300.0f, 200.0f), 1, 0, imageScale, imageDiviCount);
		auto ButtonPtr2 = AddGameObject<SelectSprite>(wstringKey::txStageSelect_Button, true,
			Vec2(-300.0f, 100.0f), spriteScale, Level::c1);
		auto selectPtr2 = AddGameObject<Sprite>(wstringKey::txStageSelectNo,true,
			Vec2(384.0f, 64.0f), Vec2(-300.0f, 100.0f), 0, 1, imageScale, imageDiviCount);
		auto ButtonPtr3= AddGameObject<SelectSprite>(wstringKey::txStageSelect_Button, true,
			 Vec2(-300.0f, 0.0f), spriteScale, Level::d1);
		auto selectPtr3 = AddGameObject<Sprite>(wstringKey::txStageSelectNo, true,
			Vec2(384.0f, 64.0f), Vec2(-300.0f, 0.0f), 1, 1, imageScale, imageDiviCount);
		auto ButtonPtr4 = AddGameObject<SelectSprite>(wstringKey::txStageSelect_Button, true,
			 Vec2(-300.0f, -100.0f), spriteScale, Level::e1);
		auto selectPtr4 = AddGameObject<Sprite>(wstringKey::txStageSelectNo, true,
			Vec2(384.0f, 64.0f), Vec2(-300.0f, -100.0f), 0, 2, imageScale, imageDiviCount);
		auto ButtonPtr5 = AddGameObject<SelectSprite>(wstringKey::txStageSelect_Button, true,
			Vec2(-300.0f, -200.0f), spriteScale, Level::f1);
		auto selectPtr5 = AddGameObject<Sprite>(wstringKey::txStageSelectNo, true,
			Vec2(384.0f, 64.0f), Vec2(-300.0f, -200.0f), 1, 2, imageScale, imageDiviCount);
		//auto ButtonPtr6 = AddGameObject<SelectSprite>(wstringKey::txStageSelect_Button, true,
		//	Vec2(-300.0f, -300.0f), spriteScale, Level::g1);
		//auto selectPtr6 = AddGameObject<Sprite>(wstringKey::txStageSelectNo, true,
		//	Vec2(384.0f, 64.0f), Vec2(-300.0f, -300.0f), 0, 3, imageScale, imageDiviCount);
		ButtonPtr->AddTag(L"PO");
		selectPtr->SetDrawLayer(1000);
		ButtonPtr1->AddTag(L"PO1");
		selectPtr1->SetDrawLayer(1000);
		ButtonPtr2->AddTag(L"PO2");
		selectPtr2->SetDrawLayer(1000);
		ButtonPtr3->AddTag(L"PO3");
		selectPtr3->SetDrawLayer(1000);
		ButtonPtr4->AddTag(L"PO4");
		selectPtr4->SetDrawLayer(1000);
		ButtonPtr5->AddTag(L"PO5");
		selectPtr5->SetDrawLayer(1000);
		//ButtonPtr6->AddTag(L"PO6");
		//selectPtr6->SetDrawLayer(1000);
		auto SM = AddGameObject<SelectManager>();
		SetSharedGameObject(L"SM", SM);
	}
	//カーソルの生成
	void SelectStage::CreateCursor() {
		auto cursorPtr = AddGameObject<Sprite>(wstringKey::txStageSelect_Cursor, true,
			Vec2(128.0f, 128.0f), Vec2(-590.0f, 300.0f));
		cursorPtr->AddTag(L"CP");		
		auto MRSMPtr = AddGameObject<SelectManager_C>();
		SetSharedGameObject(L"MRSM", MRSMPtr);

	}

	// UIの生成
	void SelectStage::CreatePlanetUI() {
		// ステージセレクトUIの作成
		Vec2 startScale(110, 150);
		Vec2 startPos(300, 0);
		App::GetApp()->GetScene<Scene>()->ResetScore();
		auto scenePtr = App::GetApp()->GetScene<Scene>();
		UINT planetPtr = scenePtr->GetPlanetCount();
		UINT countMax = scenePtr->GetPlanetMax();
		int bestRank = scenePtr->GetRank();
		// ウィンドウ
		auto ptrRank = AddGameObject<Sprite>(wstringKey::txResultRank, true, Vec2(startScale.x * 2.2f, startScale.y * 1.5f), Vec2(startPos.x - 120.0f, startPos.y + 200.0f));
		ptrRank->SetDrawLayer(1);
		auto backSprite = AddGameObject<Sprite>(wstringKey::txResultWindow, true, Vec2(startScale.x * 9.0f, startScale.y * 6.0f), Vec2(startPos.x + 15.0f, startPos.y + 130.0f));
		backSprite->SetDrawLayer(0);
		// スコアカウンター
		auto ptrPlanet = AddGameObject<PlanetUISprite>(wstringKey::txPlanets, true, Vec2(startScale.x * 1.2f, startScale.y * 0.9f), Vec2(startPos.x - 175.0f, startPos.y));
		auto highScore = AddGameObject<Score>(Vec2(startScale.x * 0.8f, startScale.y * 0.8f), Vec2(startPos.x + 20.0f, startPos.y + 5.0f), planetPtr, 2, 99, true);
		highScore->AddTag(L"scoreHigh");
		AddGameObject<AbcSprite>(wstringKey::txABCW, true, Vec2(startScale.x * 1.00f, startScale.y * 1.2f), Vec2(startPos.x + 120.0f, startPos.y + 0.0f));
		auto maxScore = AddGameObject<Score>(Vec2(startScale.x * 0.8f, startScale.y * 0.8f), Vec2(startPos.x + 270.0f, startPos.y + 5.0f), countMax, 2, 99, true);
		maxScore->AddTag(L"scoreMax");
		
		// ランク
		auto rank = AddGameObject<Rank>(Vec3(128.0f, 128.0f, 0.0f), Vec3(0,0,0),Vec3(startPos.x + 100.0f,startPos.y + 200.0f, 0.0f), false, 0, 0);
		rank->AddTag(L"bestRank");
	}

	// 初期化
	void SelectStage::OnCreate() {
		// ビューの作成
		CreateViewLight();
		// スプライトの作成
		CreateSelectSprite();
		//カーソルの生成
		CreateCursor();
		// UIの生成
		CreatePlanetUI();
		// フェードイン
		AddGameObject<FadeIn>(L"txFade", true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
	}

	// 更新
	void SelectStage::OnUpdate() {
		UINT highScore;
		int bestRank;
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		
		m_Time += ElapsedTime * 5.0f;
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		auto SM = GetSharedGameObject<SelectManager_C>(L"MRSM");
		int cursorNum = SM->GetCursorPosition();
		shared_ptr<FadeOut> fadeout = nullptr;
		wstring now = App::GetApp()->GetScene<Scene>()->GetNowScene();
		for (auto& obj : GetGameObjectVec()) {
			fadeout = dynamic_pointer_cast<FadeOut>(obj);
			if (fadeout) {
				break;
			}
		}

		auto scenePtr = App::GetApp()->GetScene<Scene>();
		for (auto& v : GetGameObjectVec()) {
			if (v->FindTag(L"CP")) {
				auto cursorTransform = v->GetComponent<Transform>();
				auto pos = cursorTransform->GetPosition();
				switch (cursorNum)
				{
				case 1:
					highScore = scenePtr->GetHighScore(cursorNum - 1);
					scenePtr->SetPlanetMax(2);
					bestRank = scenePtr->GetBestRank(cursorNum - 1);
					pos.y = 300.0f;
					cursorTransform->SetPosition(pos);
					if (fadeout) {
						if (fadeout->GetAlpha() >= 1.0f) {
							PostEvent(0.0f, GetThis<ObjectInterface>(), scenePtr, L"ToTutorialStage");
							now = L"ToTutorialStage";
						}
					}
					break;
				case 2:
					highScore = scenePtr->GetHighScore(cursorNum - 1);
					scenePtr->SetPlanetMax(5);
					bestRank = scenePtr->GetBestRank(cursorNum - 1);
					pos.y = 200.0f;
					cursorTransform->SetPosition(pos);
					if (fadeout) {
						if (fadeout->GetAlpha() >= 1.0f) {
							PostEvent(0.0f, GetThis<ObjectInterface>(), scenePtr, L"ToGameStage1");
							now = L"ToGameStage1";
						}
					}
					break;
				case 3:
					highScore = scenePtr->GetHighScore(cursorNum - 1);
					scenePtr->SetPlanetMax(7);
					bestRank = scenePtr->GetBestRank(cursorNum - 1);
					pos.y = 100.0f;
					cursorTransform->SetPosition(pos);
					if (fadeout) {
						if (fadeout->GetAlpha() >= 1.0f) {
							PostEvent(0.0f, GetThis<ObjectInterface>(), scenePtr, L"ToGameStage2");
							now = L"ToGameStage2";
						}
					}
					break;
				case 4:
					highScore = scenePtr->GetHighScore(cursorNum - 1);
					scenePtr->SetPlanetMax(7);
					bestRank = scenePtr->GetBestRank(cursorNum - 1);
					pos.y = 0.0f;
					cursorTransform->SetPosition(pos);
					if (fadeout) {
						if (fadeout->GetAlpha() >= 1.0f) {
							PostEvent(0.0f, GetThis<ObjectInterface>(), scenePtr, L"ToGameStage3");
							now = L"ToGameStage3";
						}
					}
					break;
				case 5:
					highScore = scenePtr->GetHighScore(cursorNum - 1);
					scenePtr->SetPlanetMax(10);
					bestRank = scenePtr->GetBestRank(cursorNum - 1);
					pos.y = -100.0f;
					cursorTransform->SetPosition(pos);
					if (fadeout) {
						if (fadeout->GetAlpha() >= 1.0f) {
							PostEvent(0.0f, GetThis<ObjectInterface>(), scenePtr, L"ToGameStage4");
							now = L"ToGameStage4";
						}
					}
					break;
				case 6:
					highScore = App::GetApp()->GetScene<Scene>()->GetHighScore(cursorNum - 1);
					App::GetApp()->GetScene<Scene>()->SetPlanetMax(12);
					bestRank = scenePtr->GetBestRank(cursorNum - 1);
					pos.y = -200.0f;
					cursorTransform->SetPosition(pos);
					if (fadeout) {
						if (fadeout->GetAlpha() >= 1.0f) {
							PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToGameStage5");
							now = L"ToGameStage5";
						}
					}
					break;
				//case 7:
				//	highScore = App::GetApp()->GetScene<Scene>()->GetHighScore(cursorNum - 1);
				//	App::GetApp()->GetScene<Scene>()->SetPlanetMax(99);
				//	bestRank = scenePtr->GetBestRank(cursorNum - 1);
				//	pos.y = -300.0f;
				//	cursorTransform->SetPosition(pos);
				//	if (fadeout) {
				//		if (fadeout->GetAlpha() >= 1.0f) {
				//			PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToGameStage6");
				//			now = L"ToGameStage6";
				//		}
				//	}
				//	break;
				}
					
				scenePtr->SetNowScene(now);
				//点滅処理
				m_Alpha += sin(m_Time * 2.0f);
				if (m_Alpha > 1.0f) {
					m_Alpha = 1.0f;
				}
				if (m_Alpha <= 0.3f) {
					m_Alpha = 0.3f;
				}
				v->GetComponent<PCTSpriteDraw>()->SetDiffuse(Col4(1.0f, 1.0f, 1.0f, m_Alpha));
			}
			
			
		}

		// 惑星カウンターの更新
		auto countMax = scenePtr->GetPlanetMax();

		shared_ptr<Score> score = nullptr;
		shared_ptr<Rank> rank = nullptr;
		auto gameObjects = GetGameObjectVec();
		for (auto obj : gameObjects) {
			score = dynamic_pointer_cast<Score>(obj);
			if (score) {
				if (score->FindTag(L"scoreHigh")) {
					score->SetValue(highScore);
				}

				if (score->FindTag(L"scoreMax")) {
					score->SetValue(countMax);
				}
			}

			rank = dynamic_pointer_cast<Rank>(obj);
			if (rank) {
				if (rank->FindTag(L"bestRank")) {
					rank->SetAnimTip(0, bestRank);
				}
			}
		}

		// コントローラチェックして入力があればコマンド呼び出し
		//m_InputHandler.PushHandle(GetThis<SelectStage>());

		// Aボタン入力されたらGameStageにシーン遷移
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
			AddGameObject<FadeOut>(L"txFade", true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
			XAPtr->Start(wstringKey::seKettei, 0, 0.5f);
		
		}

	}

	// シーン遷移
	void SelectStage::OnSceneTransition(const wstring& wstringKey)
	{
		shared_ptr<FadeOut> fadeout = nullptr;
		auto objs = GetGameObjectVec();
		for (auto& obj : objs) {
			fadeout = dynamic_pointer_cast<FadeOut>(obj);
			if (fadeout) {
				if (fadeout->GetAlpha() >= 1.0f) {
					PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), wstringKey);
				}
			}
		}

	}

	// Aボタン
	//void SelectStage::OnPushA() {
	//	PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToGameStage");
	//}
}
//end basecross
