/*!
@file GameStage7.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage7 : public Stage {
		shared_ptr<SoundItem> m_BGM;
		//惑星の数
		float m_PlanetCount = 0;
		int m_Digit10;
		// ステージの幅、高さ、中央用にそれぞれの半分
		float m_Width;
		float m_Height;
		float m_HalfWidth;
		float m_HalfHeight;
		const float frameOverlap = 1.0f;

		//ビューの作成
		void CreateViewLight();
		//背景の生成
		void CreateBG();
		//テストボックスの作成
		void CreateTestBox();
		//テストプレイヤーの作成
		void CreateTestPlayer();
		//スピードUIスプライトの作成
		void CreateUISprite();
		//惑星の数のUIを表示
		void CreatePlanetUISprite();
		//数字の作成
		void CreatePlanetCounter();
		// パーティクルの作成
		void CreateParticle();
	public:
		//構築と破棄
		GameStage7() :
			Stage(),
			m_Width(50.0f),
			m_HalfWidth(m_Width / 2.0f),
			m_Height(50.0f),
			m_HalfHeight(m_Height / 2.0f)
		{}
		virtual ~GameStage7() {}
		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;
		virtual void OnDestroy()override;


	};

}
//end basecross

