#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class Radar : public GameObject;
	//	用途: レーダー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Radar::Radar(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<GameObject> parentPtr) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_ParentPtr(parentPtr)
	{
	}
	//初期化
	void Radar::OnCreate() {
		auto ptrTransform = GetComponent<Transform>();
		//親子関係化
		ptrTransform->SetParent(m_ParentPtr);

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		////CollisionSphere衝突判定を付ける
		//auto ptrColl = AddComponent<CollisionSphere>();
		//ptrColl->SetAfterCollision(AfterCollision::None);
		////タグをつける
		//AddTag(wstringKey::tagAbsorptionObject);

		//インデックス設定
		float tipWidth = 128.0f / 512.0f;
		float tipHeight = 128.0f / 512.0f;
		for (int c = 0; c < 4; c++) {
			for (int r = 0; r < 4; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 0;
		m_AnimTipCol = 1;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.75f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};
		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);
		PtrSpriteDraw->SetTextureResource(wstringKey::txTestPlayer);
		PtrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		//PtrDraw->SetTextureResource(L"WaraFront_TX");
		//透明処理
		SetAlphaActive(true);
		SetDrawLayer(1);
		//ステートマシンの構築
		m_StateMachine.reset(new StateMachine<Radar>(GetThis<Radar>()));
		//最初のステートを設定
		m_StateMachine->ChangeState(RadarDefault::Instance());

		////文字列をつける
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
	}

	//////衝突判定
	////衝突したら
	//void Radar::OnCollisionEnter(shared_ptr<GameObject>& other) {
	//}
	////衝突し続けてるとき
	//void Radar::OnCollisionExcute(shared_ptr<GameObject>& other) {
	//}
	////衝突し終わったら
	//void Radar::OnCollisionExit(shared_ptr<GameObject>& other) {
	//}

	//更新
	void Radar::OnUpdate() {
		auto playerPtr = dynamic_pointer_cast<Player>(m_ParentPtr);
		if (playerPtr->GetStateMachine()->GetCurrentState() != PlayerDead::Instance()) {
			m_StateMachine->Update();

			float delta = App::GetApp()->GetElapsedTime();
			Quat span(Vec3(0, 0, -1.0f), 2.0f*delta);
			auto qt = GetComponent<Transform>()->GetQuaternion();
			qt *= span;
			GetComponent<Transform>()->SetQuaternion(qt);
		}
	}
	void Radar::OnUpdate2() {
		////文字列の表示
		//DrawStrings();
	}

	//SEセッターゲッター
	void Radar::SetSE(const wstring& wstringKey)
	{
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		XAPtr->Stop(m_SE);
		m_SE = XAPtr->Start(wstringKey, XAUDIO2_LOOP_INFINITE, 0.5f);
	}

	shared_ptr<SoundItem> Radar::GetSE() const
	{
		return m_SE;
	}

	////レーダーステート集
	//デフォルトステート
	IMPLEMENT_SINGLETON_INSTANCE(RadarDefault)

	void RadarDefault::Enter(const shared_ptr<Radar>& obj) {
		obj->m_Delta = 0.0f;
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		XAPtr->Stop(obj->GetSE());
	}
	void RadarDefault::Execute(const shared_ptr<Radar>& obj) {
		if (obj->m_AnimTipRow > 0) {
			obj->m_Delta += App::GetApp()->GetElapsedTime();
			if (obj->m_Delta >= 0.05f) {
				obj->m_Delta = 0.0f;
				obj->m_AnimTipRow--;
				float HelfSize = 0.75f;
				Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
				auto animTip = obj->m_AnimTip[obj->m_AnimTipCol][obj->m_AnimTipRow];
				vector<VertexPositionColorTexture> vertices = {
					{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
					{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(animTip.right, animTip.top) },
					{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
					{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(animTip.right, animTip.bottom) }
				};
				auto drawComp = obj->GetComponent<PCTStaticDraw>();
				drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);
			}
		}
	}
	void RadarDefault::Exit(const shared_ptr<Radar>& obj) {

	}
	//展開ステート
	IMPLEMENT_SINGLETON_INSTANCE(RadarExpansion)

	void RadarExpansion::Enter(const shared_ptr<Radar>& obj) {
		obj->m_Delta = 0.0f;
		//auto XAPtr = App::GetApp()->GetXAudio2Manager();
		//obj->SetSE(L"Survey_SE");
	}
	void RadarExpansion::Execute(const shared_ptr<Radar>& obj) {
		if (obj->m_AnimTipRow < 2) {
			obj->m_Delta += App::GetApp()->GetElapsedTime();
			if (obj->m_Delta >= 0.05f) {
				obj->m_Delta = 0.0f;
				obj->m_AnimTipRow++;
				float HelfSize = 0.75f;
				Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
				auto animTip = obj->m_AnimTip[obj->m_AnimTipCol][obj->m_AnimTipRow];
				vector<VertexPositionColorTexture> vertices = {
					{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
					{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(animTip.right, animTip.top) },
					{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
					{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(animTip.right, animTip.bottom) }
				};
				auto drawComp = obj->GetComponent<PCTStaticDraw>();
				drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);
			}
		}
	}
	void RadarExpansion::Exit(const shared_ptr<Radar>& obj) {

	}

	//文字列の表示
	void Radar::DrawStrings() {

		//文字列表示
		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring fpsStr(L"FPS: ");
		fpsStr += Util::UintToWStr(fps);
		fpsStr += L"\nElapsedTime: ";
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		fpsStr += Util::FloatToWStr(ElapsedTime);
		fpsStr += L"\n";

		auto pos = GetComponent<Transform>()->GetPosition();
		wstring positionStr(L"Position:\t");
		positionStr += L"X=" + Util::FloatToWStr(pos.x, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Y=" + Util::FloatToWStr(pos.y, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Z=" + Util::FloatToWStr(pos.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring speadStr(L"Spead:\t");
		//auto gravVelocity = GetComponent<Gravity>()->GetGravityVelocity();
		//speadStr += Util::FloatToWStr(m_Spead*100.0f, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Y=" + Util::FloatToWStr(gravVelocity.y, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Z=" + Util::FloatToWStr(gravVelocity.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring str = fpsStr + positionStr + speadStr;
		//文字列コンポーネントの取得
		auto ptrString = GetComponent<StringSprite>();
		ptrString->SetText(str);
	}
}