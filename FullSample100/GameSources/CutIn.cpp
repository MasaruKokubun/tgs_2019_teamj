#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	///	カットイン
	//--------------------------------------------------------------------------------------
	CutIn::CutIn(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,const Vec2& StartScale, const Vec3& StartPos) :
		Sprite(StagePtr, TextureKey, Trace, StartScale, Vec2(StartPos.x,StartPos.y)),
		m_Pos(StartPos)
	{}

	CutIn::~CutIn() {}
	void CutIn::OnCreate() {
		Sprite::OnCreate();
		m_CutInSpeed = 500.0f;
	}

	void CutIn::OnUpdate() {
		auto elapsedTime = App::GetApp()->GetElapsedTime();
		auto transPtr = GetComponent<Transform>();
		auto cutInPos = transPtr->GetPosition();
		if (cutInPos.x >= -1000.0f) {
			cutInPos.x += -m_CutInSpeed * elapsedTime;
			transPtr->SetPosition(cutInPos);
		}
		else {
			m_CutInFlg = true;
		}
	}

}