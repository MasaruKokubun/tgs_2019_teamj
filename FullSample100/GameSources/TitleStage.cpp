/*!
@file TitleStage.cpp
@brief タイトルステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {


	//--------------------------------------------------------------------------------------
	//	タイトルステージクラス実体
	//--------------------------------------------------------------------------------------
	// ビューの作成
	void TitleStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		// ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(Vec3(0.0f, 5.0f, -5.0f));
		PtrCamera->SetAt(Vec3(0.0f, 0.0f, 0.0f));
		// マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		// デフォルトのライティングを設定
		PtrMultiLight->SetDefaultLighting();
	}

	//スプライトの作成
	void TitleStage::CreateTitleSprite() {
		// Aボタンを押して
		auto backGroundPtr = AddGameObject<Sprite>(wstringKey::txResultBG, true,
			Vec2(1536.0f, 864.0f), Vec2(0.0f, 0.0f));
		backGroundPtr->SetDrawLayer(-1);
		auto titleLogoPtr = AddGameObject<Sprite>(wstringKey::txTitle_Logo, true,
			Vec2(960.0f, 600.0f), Vec2(0.0f, 150.0f));
		auto MessagePtr = AddGameObject<Sprite>(wstringKey::txMessage, true,
			Vec2(640.0f, 256.0f), Vec2(0, -200.0f));
		MessagePtr->AddTag(L"MS");
	}
	//プレイヤーの生成
	void TitleStage::CreateAnimPlayer() {
		auto ptrPlayer = AddGameObject<TitlePlayer>(Vec3(128.0f, 128.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(m_XPos, -100.0f, 0.0f));
		ptrPlayer->AddTag(L"Ship");
	}

	// 初期化
	void TitleStage::OnCreate() {
		// ビューの作成
		CreateViewLight();
		// スプライトの作成
		CreateTitleSprite();
		CreateTitleSprite(); 
		//プレイヤーの生成
		CreateAnimPlayer();
		auto XAPtr = App::GetApp()->GetXAudio2Manager();

		// フェードイン
		AddGameObject<FadeIn>(L"txFade", true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
	}

	// 更新
	void TitleStage::OnUpdate() {
		OnAnimation();
		// コントローラチェックして入力があればコマンド呼び出し
		//m_InputHandler.PushHandle(GetThis<ResultStage>());

		// Aボタン入力されたらSelectStageにシーン遷移
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && !m_FadeFlg) {
			auto XAPtr = App::GetApp()->GetXAudio2Manager();
			XAPtr->Start(wstringKey::seKettei, 0, 0.5f);
			m_FadeFlg = true;
		}
		if (m_FadeFlg) {
			bool fadeoutFlg = false;
			auto objs = GetGameObjectVec();
			for (auto& obj : objs) {
				auto fadeout = dynamic_pointer_cast<FadeOut>(obj);
				if (fadeout)
				{
					fadeoutFlg = true;
					break;
				}
			}
			if (!fadeoutFlg) {
				AddGameObject<FadeOut>(L"txFade", true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
			}
		}
		shared_ptr<FadeOut> fadeout = nullptr;
		auto objs = GetGameObjectVec();
		for (auto& obj : objs) {
			fadeout = dynamic_pointer_cast<FadeOut>(obj);
			if (fadeout) {
				if (fadeout->GetAlpha() >= 1.0f) {
					PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToSelectStage");
				}
			}
		}
	}
	void TitleStage::OnAnimation() {
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_Time += ElapsedTime * 5.0f;
		for (auto& v : GetGameObjectVec()) {
			if (v->FindTag(L"MS")) {
				m_Alpha += sin(m_Time) * 0.1f;
				if (m_Alpha > 1.0f) {
					m_Alpha = 1.0f;
				}
				if (m_Alpha <= 0.0f) {
					m_Alpha = 0.0f;
				}
				v->GetComponent<PCTSpriteDraw>()->SetDiffuse(Col4(1.0f, 1.0f, 1.0f, m_Alpha));
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && !m_FadeFlg) {
					v->SetDrawActive(false);
				}
			}
			if (v->FindTag(L"Ship")) {
				auto ptrPos = v->GetComponent<Transform>()->GetPosition();
				ptrPos.x += 10.0f;
				if (ptrPos.x > 1500.0f) {
					ptrPos.x = -1500.0f;
				}
				v->GetComponent<Transform>()->SetPosition(ptrPos);
			}
		}
	}

	// Aボタン
	//void TitleStage::OnPushA() {
	//	PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToGameStage");
	//}

	//--------------------------------------------------------------------------------------
	//	class TitlePlayer : public GameObject;
	//	用途: ランク
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TitlePlayer::TitlePlayer(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position)
	{}

	//初期化
	void TitlePlayer::OnCreate() {

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);
		
		Quat span(Vec3(0, 0, -1.0f), 90.0f*XM_PI/180.0f);
		auto qt = GetComponent<Transform>()->GetQuaternion();
		qt *= span;
		GetComponent<Transform>()->SetQuaternion(qt);

		//アニメチップ作成
		float tipWidth = 128.0f / 512.0f;
		float tipHeight = 128.0f / 512.0f;
		for (int c = 0; c < 4; c++) {
			for (int r = 0; r < 4; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 0;
		m_AnimTipCol = 0;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.75f;
		auto animTip = m_AnimTip[m_AnimTipCol][m_AnimTipRow];
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(animTip.left, animTip.top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(animTip.right, animTip.top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(animTip.right, animTip.bottom)}
		};
		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTSpriteDraw>(vertices, indices);
		PtrSpriteDraw->SetTextureResource(wstringKey::txTestPlayer);
		PtrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		//透明処理
		SetAlphaActive(true);
		SetDrawLayer(1);

		auto radarPtr = GetStage()->AddGameObject<TitleRadar>(GetComponent<Transform>()->GetScale(), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f),GetThis<GameObject>());
	}

	void TitlePlayer::OnUpdate() {
		float elapsedTime = App::GetApp()->GetElapsedTime();
		m_delta += elapsedTime;
		float HelfSize = 0.5f;
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		auto animTip = m_AnimTip[m_AnimTipCol][m_AnimTipRow];
		vector<VertexPositionColorTexture> vertices = {
			{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
			{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(animTip.right, animTip.top) },
			{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
			{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(animTip.right, animTip.bottom) }
		};
		auto drawComp = GetComponent<PCTSpriteDraw>();
		drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);
		if (m_delta >= 0.05f) {
			m_delta = 0.0f;
			if (m_AnimTipRow < 3) {
				m_AnimTipRow++;
			}
			else {
				m_AnimTipRow = 2;
			}
		}
	}

	//--------------------------------------------------------------------------------------
	//	class TitleRadar : public GameObject;
	//	用途: レーダー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TitleRadar::TitleRadar(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<GameObject> parentPtr) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_ParentPtr(parentPtr)
	{
	}
	//初期化
	void TitleRadar::OnCreate() {
		auto ptrTransform = GetComponent<Transform>();
		//親子関係化
		ptrTransform->SetParent(m_ParentPtr);

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		//インデックス設定
		float tipWidth = 128.0f / 512.0f;
		float tipHeight = 128.0f / 512.0f;
		for (int c = 0; c < 4; c++) {
			for (int r = 0; r < 4; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 0;
		m_AnimTipCol = 1;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.75f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};
		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTSpriteDraw>(vertices, indices);
		PtrSpriteDraw->SetTextureResource(wstringKey::txTestPlayer);
		PtrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		//透明処理
		SetAlphaActive(true);
		SetDrawLayer(2);

	}

	//更新
	void TitleRadar::OnUpdate() {
		float delta = App::GetApp()->GetElapsedTime();
		Quat span(Vec3(0, 0, -1.0f), 2.0f*delta);
		auto qt = GetComponent<Transform>()->GetQuaternion();
		qt *= span;
		GetComponent<Transform>()->SetQuaternion(qt);
	}
}
//end basecross
