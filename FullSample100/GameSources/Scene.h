/*!
@file Scene.h
@brief シーン
*/
#pragma once

#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------
	const int STAGENUMMAX = 6;
	class Scene : public SceneBase {
		void CreateResourses();
		shared_ptr<SoundItem> m_BGM;
		UINT m_Planet;
		UINT m_PlanetMax;
		int m_Rank;
		UINT m_HighScore[STAGENUMMAX];
		int m_BestRank[STAGENUMMAX];
		//現在のシーンを保存
		wstring nowScene;
	public:
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		*/
		//--------------------------------------------------------------------------------------
		Scene() :SceneBase(), m_Planet(0), m_PlanetMax(0), m_Rank(0)
		{
			ResetHighScore();
		}
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~Scene();
		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//--------------------------------------------------------------------------------------
		/*!
		@brief イベント取得
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnEvent(const shared_ptr<Event>& event) override;
		//惑星の数
		const UINT GetPlanetCount() const { return m_Planet; }
		void SetPlanetCount(UINT num) { m_Planet = num; }
		const UINT GetPlanetMax() const { return m_PlanetMax; }
		void SetPlanetMax(UINT numM) { m_PlanetMax = numM; }
		const int GetRank() const { 
			return m_Rank; 
		}
		void SetRank(int num) {
			m_Rank = num;
		}
		UINT GetHighScore(int stageNum) const {
			return m_HighScore[stageNum];
		}
		void SetHighScore(int stageNum, UINT num) {
			m_HighScore[stageNum] = num;
		}
		int GetBestRank(int stageNum) const {
			return m_BestRank[stageNum];
		}
		void SetBestRank(int stageNum, int rank) {
			m_BestRank[stageNum] = rank;
		}
		void ResetScore() {
			m_Planet = 0;
			m_PlanetMax = 0;
			m_Rank = 0;
		}
		void ResetHighScore() {
			for (int i = 0; i < STAGENUMMAX; i++) {
				m_HighScore[i] = 0;
				m_BestRank[i] = 0;
			}
		}
		void RankCalc();
		void SetAllHighScore(const wstring& sceneKey, const int& stageNum);
		const wstring GetNowScene() const { return nowScene; }
		void SetNowScene(wstring scene) { nowScene = scene; }
	};
}
//end basecross