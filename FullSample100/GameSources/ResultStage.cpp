/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
//	セレクトマネージャー
//--------------------------------------------------------------------------------------
	SelectManager_RR::SelectManager_RR(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_level(Level::a1),
		m_TotalTime(0.0f),
		m_ScaleTime(0.0f),
		m_x(0),
		m_y(0)
	{}
	SelectManager_RR::~SelectManager_RR() {}

	void SelectManager_RR::OnCreate() {
		// これいる？
		m_StageManager = {
			{ Level::a1},
			{ Level::b1},
			{ Level::c1},
		};

		// デバッグ用の文字表示コンポーネントを追加する
		//auto debugStrComp = AddComponent<StringSprite>();
		//debugStrComp->SetTextRect(Rect2D<float>(10, 20, 400, 300)); // テキストの表示エリア
		//debugStrComp->SetBackColor(Col4(0, 0, 0, 0.25f)); // テキストエリアの背景色(RGBA)
	}

	void SelectManager_RR::OnUpdate() {
		shared_ptr<ResultStage> resultStage = nullptr;
		resultStage = dynamic_pointer_cast<ResultStage>(this->GetStage());
		if (resultStage) {
			if (resultStage->GetCursorFlg()) {

				// 描画時間
				float ElapsedTime = App::GetApp()->GetElapsedTime();
				auto ScenePtr = App::GetApp()->GetScene<Scene>();
				//コントローラの取得
				auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
				// どっちかに傾けている
				if ((CntlVec[0].fThumbLX <= -0.5f || CntlVec[0].fThumbLX >= 0.5f) ||
					(CntlVec[0].fThumbLY <= -0.5f || CntlVec[0].fThumbLY >= 0.5f))
				{
					// デッドゾーンと最初の入力
					if (m_TotalTime >= 0.15f || m_TotalTime == 0.0f) {
						m_TotalTime = 0.0f;
						// 上に傾けた
						if (CntlVec[0].fThumbLY >= 0.5f && (CntlVec[0].fThumbLX < 0.5f&&CntlVec[0].fThumbLX > -0.5f) && !m_lr) {
							auto XAPtr3 = App::GetApp()->GetXAudio2Manager();
							m_y--;
							if (m_y < 0) {
								m_y = (int)m_StageManager.size() - 1;
							}
							m_ScaleTime = 0.0f;
						}
						// 下に傾けた
						if (CntlVec[0].fThumbLY <= -0.5f && (CntlVec[0].fThumbLX < 0.5f&&CntlVec[0].fThumbLX > -0.5f) && !m_lr) {
							auto XAPtr4 = App::GetApp()->GetXAudio2Manager();
							m_y++;
							if (m_y >= (int)m_StageManager.size()) {
								m_y = 0;
							}
							m_ScaleTime = 0.0f;
						}
					}
					m_TotalTime += ElapsedTime;
				}
				else {
					m_TotalTime = 0.0f;
				}
				m_level = m_StageManager[m_y][m_x];

				m_ScaleTime += ElapsedTime * 2.0f;
				if (m_ScaleTime >= XM_2PI) {
					m_ScaleTime = 0;
				}
				auto objects = GetStage()->GetGameObjectVec();
				for (auto object : objects) {
					auto sprite = dynamic_cast<SelectSprite*>(object.get());
					if (sprite) {
						if (!(sprite->GetLevel() == Level::none)) {
							// 選択中のレベルなら
							if (sprite->GetLevel() == m_level) {
								// 明るくする
								sprite->UpdateColor(Col4(1.0f));
								// 大きくしたり小さくしたりする
								//sprite->UpdateScale(Vec2((sinf(m_ScaleTime) + 4.0f) / 3.0f, (sinf(m_ScaleTime) + 4.0f) / 3.0f));
							}
							// それ以外の時
							else {
								// 暗くする
								sprite->UpdateColor(Col4(0.5f));
								// 大きさを元に戻す
								//sprite->UpdateScale(Vec2(1.0f, 1.0f));
							}
						}
					}
				}
				//auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
				//リセットコマンド
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_START&&CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_BACK) {
					App::GetApp()->GetScene<Scene>()->ResetScore();
					//ScorePtr1->SetScore(PlanetCount);
					PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToTitleStage");
				}
			}
		}

	}
	//--------------------------------------------------------------------------------------
	///	セレクトマネージャー
	//--------------------------------------------------------------------------------------
	SelectManager_R::SelectManager_R(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_TotalTime(0.0f),
		m_ScaleTime(0.0f),
		m_x(0),
		m_y(0)
	{}
	SelectManager_R::~SelectManager_R() {}

	void SelectManager_R::OnUpdate()
	{
		shared_ptr<ResultStage> resultStage = nullptr;
		resultStage = dynamic_pointer_cast<ResultStage>(this->GetStage());
		if (resultStage) {
			if (resultStage->GetCursorFlg()) {
				// 描画時間
				float ElapsedTime = App::GetApp()->GetElapsedTime();
				auto XAPtr = App::GetApp()->GetXAudio2Manager();
				//コントローラの取得
				auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
				// どっちかに傾けている
				if ((CntlVec[0].fThumbLX <= -0.5f || CntlVec[0].fThumbLX >= 0.5f) ||
					(CntlVec[0].fThumbLY <= -0.5f || CntlVec[0].fThumbLY >= 0.5f)
					&& m_Active == true) {
					// デッドゾーンと最初の入力
					if (m_TotalTime >= 0.15f || m_TotalTime == 0.0f) {
						m_TotalTime = 0.0f;
						// 上に傾けた
						if (CntlVec[0].fThumbLY >= 0.5f && (CntlVec[0].fThumbLX < 0.5f&&CntlVec[0].fThumbLX > -0.5f)) {
							m_position--;
							XAPtr->Start(wstringKey::seCursor, 0, 0.5f);
							if (m_position < 1) {
								m_position = 3;
							}
							m_TotalTime = 0.0f;
						}
						// 下に傾けた
						if (CntlVec[0].fThumbLY <= -0.5f && (CntlVec[0].fThumbLX < 0.5f&&CntlVec[0].fThumbLX > -0.5f)) {
							m_position++;
							XAPtr->Start(wstringKey::seCursor, 0, 0.5f);
							if (m_position > 3) {
								m_position = 1;
							}
							m_TotalTime = 0.0f;
						}
					}
					m_TotalTime += ElapsedTime;
				}
				else {
					m_TotalTime = 0.0f;
					if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
						m_Active = false;
					}
				}
			}
		}

	}
	//--------------------------------------------------------------------------------------
	//	リザルトステージクラス実体
	//--------------------------------------------------------------------------------------
	// ビューの作成
	void ResultStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		// ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(Vec3(0.0f, 5.0f, -5.0f));
		PtrCamera->SetAt(Vec3(0.0f, 0.0f, 0.0f));
		// マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		// デフォルトのライティングを設定
		PtrMultiLight->SetDefaultLighting();
	}

	//スプライトの作成
	void ResultStage::CreateResultSprite() {
		auto backGroundPtr = AddGameObject<Sprite>(wstringKey::txResultBG, true,
			Vec2(1536.0f, 864.0f), Vec2(0.0f, 0.0f));
		backGroundPtr->SetDrawLayer(-1);
		//auto RWPtr = AddGameObject<Sprite>(wstringKey::txResultWindow, true,
		//	Vec2(1280.0f, 800.0f), Vec2(0.0f, 0.0f));
		//RWPtr->AddTag(L"RW");
		//RWPtr->SetDrawActive(false);
		auto MSPtr = AddGameObject<Sprite>(wstringKey::txMissonComplete, true,
			Vec2(768.0f, 192.0f), Vec2(m_StartPosX, 300.0f));
		MSPtr->AddTag(L"MS");
		// UI用のウィンドウ
		auto backSprite = AddGameObject<Sprite>(wstringKey::txResultWindow, true, Vec2(1150, 1000), Vec2(-40, -30));
		backSprite->GetComponent<PCTSpriteDraw>()->SetDiffuse(Col4(1.0f, 1.0f, 1.0f, 1.0f));
		backSprite->SetDrawLayer(0);
		backSprite->AddTag(L"RP");
		backSprite->SetDrawActive(false);

		auto ptrPlanet = AddGameObject<Sprite>(wstringKey::txPlanet_R, true,
			Vec2(130.0f, 130.0f), Vec2(-300.0f, 100.0f));
		ptrPlanet->AddTag(L"RP");
		ptrPlanet->SetDrawActive(false);
		auto ptrRank = AddGameObject<Sprite>(wstringKey::txResultRank, true,
			Vec2(256.0f, 128.0f), Vec2(-300.0f, -130.0f));
		ptrRank->AddTag(L"RR");
		ptrRank->SetDrawActive(false);
		//auto ptrRankEng = AddGameObject<Sprite>(wstringKey::txRank, true,
		//	Vec2(512.0f, 128.0f), Vec2(50.0f, -150.0f));
		//ptrRankEng->AddTag(L"RA");
		//ptrRankEng->SetDrawActive(false);
		auto ptrRankEngA = AddGameObject<Sprite>(wstringKey::txRank_A, true,
			Vec2(128.0f, 128.0f), Vec2(50.0f, -150.0f));
		auto ptrRankEngB = AddGameObject<Sprite>(wstringKey::txRank_B, true,
			Vec2(128.0f, 128.0f), Vec2(50.0f, -150.0f));
		auto ptrRankEngC = AddGameObject<Sprite>(wstringKey::txRank_C, true,
			Vec2(128.0f, 128.0f), Vec2(50.0f, -150.0f));
		auto ptrRankEngS = AddGameObject<Sprite>(wstringKey::txRank_S, true,
			Vec2(128.0f, 128.0f), Vec2(50.0f, -150.0f));
		ptrRankEngA->AddTag(L"RA");
		ptrRankEngB->AddTag(L"RB");
		ptrRankEngC->AddTag(L"RC");
		ptrRankEngS->AddTag(L"RS");
		ptrRankEngA->SetDrawActive(false);
		ptrRankEngB->SetDrawActive(false);
		ptrRankEngC->SetDrawActive(false);
		ptrRankEngS->SetDrawActive(false);

	}
	void ResultStage::CreateResultNumSprite() {
		//auto ptrNumPl = AddGameObject<ScoreSprite>(1, wstringKey::txNum, true,
		//	Vec2(100.0f, 130.0f), Vec3(80.0f, 100.0f, 0.0f));
		//ptrNumPl->AddTag(L"NP");
		//ptrNumPl->SetDrawActive(false);
		//auto ptrNum10 = AddGameObject<ScoreSprite10>(1, wstringKey::txNum, true,
		//	Vec2(100.0f, 130.0f), Vec3(20.0f, 100.0f, 0.0f));
		//ptrNum10->AddTag(L"10");
		//ptrNum10->SetDrawActive(false);

		// スコアカウンター
		UINT planetPtr = App::GetApp()->GetScene<Scene>()->GetPlanetCount();
		UINT CountMax = App::GetApp()->GetScene<Scene>()->GetPlanetMax();
		auto SCountPtr = AddGameObject<Score>(Vec2(80.0f, 100.0f), Vec2(40.0f, 100.0f), planetPtr, 2, CountMax, true);
		SCountPtr->AddTag(L"NP");
		SCountPtr->SetDrawActive(false);
		auto slashPtr = AddGameObject<AbcSprite>(wstringKey::txABCW, true, Vec2(105.0f, 120.0f), Vec2(120.0f, 95.0f));
		slashPtr->AddTag(L"10");
		slashPtr->SetDrawActive(false);
		// スコアの最大値
		auto SCountMAXPtr = AddGameObject<Score>(Vec2(80.0f, 100.0f), Vec2(260.0f, 100.0f), CountMax, 2, CountMax, false);
		SCountMAXPtr->AddTag(L"10");
		SCountMAXPtr->SetDrawActive(false);
		rankNum = (float)planetPtr / CountMax;

		// スコアの保存
		auto scenePtr = App::GetApp()->GetScene<Scene>();
		scenePtr->SetAllHighScore(L"ToTutorialStage", 0);
		scenePtr->SetAllHighScore(L"ToGameStage1"	, 1);
		scenePtr->SetAllHighScore(L"ToGameStage2"	, 2);
		scenePtr->SetAllHighScore(L"ToGameStage3"	, 3);
		scenePtr->SetAllHighScore(L"ToGameStage4"	, 4);
		scenePtr->SetAllHighScore(L"ToGameStage5"	, 5);
		scenePtr->SetAllHighScore(L"ToGameStage6"	, 6);

	}
	//ボタンの生成
	void ResultStage::CreateButton() {
		auto RTPtr = AddGameObject<SelectSprite>(wstringKey::txGameOver_Retry, true,
			Vec2(0.0f, 100.0f), Vec2(384.0f, 192.0f), Level::a1);
		RTPtr->SetDrawActive(false);
		RTPtr->AddTag(L"RT");
		auto TTPtr = AddGameObject<SelectSprite>(wstringKey::txGameOver_Title, true,
			Vec2(0.0f, -200.0f), Vec2(384.0f, 192.0f), Level::c1);
		TTPtr->SetDrawActive(false);
		TTPtr->AddTag(L"TT");
		auto SEPtr = AddGameObject<SelectSprite>(wstringKey::txGameOver_Select, true,
			 Vec2(0.0f, -50.0f),Vec2(384.0f, 192.0f),Level::b1);
		SEPtr->SetDrawActive(false);
		SEPtr->AddTag(L"SE");
		auto SMRR = AddGameObject<SelectManager_RR>();
		SetSharedGameObject(L"SMRR", SMRR);
	}
	void ResultStage::CreateCursor() {
		auto cursorPtr = AddGameObject<Sprite>(wstringKey::txStageSelect_Cursor, true,
			Vec2(128.0f, 128.0f), Vec2(-240.0f, -100.0f));
		cursorPtr->AddTag(L"CP");
		cursorPtr->SetDrawActive(false);
		auto MRSMPtr = AddGameObject<SelectManager_R>();
		SetSharedGameObject(L"MRSMR", MRSMPtr);
	}
	// 初期化
	void ResultStage::OnCreate() {
		// ビューの作成
		CreateViewLight();
		// スプライトの作成
		CreateResultSprite();
		//数字スプライトの作成
		CreateResultNumSprite();
		//ボタンの作成
		CreateButton();
		//カーソルの作成
		CreateCursor();
	}

	// 更新
	void ResultStage::OnUpdate() {
		OnAnimation();
		// コントローラチェックして入力があればコマンド呼び出し
		//m_InputHandler.PushHandle(GetThis<ResultStage>());
		//カーソル移動の呼び出し
		OnCursorMove();

		// Aボタン入力されたらGameStageにシーン遷移
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A && m_AnimFlg) {
			// スコアのリセット
			App::GetApp()->GetScene<Scene>()->ResetScore();

		}

	}
	void ResultStage::OnAnimation() {
		// 描画時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		m_Time += ElapsedTime * 5.0f;
		//if (rankNum >= 0.75f) {
		//	rankFlg = 1;
		//}
		//else if (rankNum >= 0.50f && rankNum < 0.75f) {
		//	rankFlg = 2;
		//}
		//else if (rankNum >= 0.25f && rankNum < 0.50f) {
		//	rankFlg = 3;
		//}
		//else if (rankNum < 0.25f) {
		//	rankFlg = 4;
		//}
		UINT PlanetCount = App::GetApp()->GetScene<Scene>()->GetPlanetCount();
		int c = 0;
		for (auto& v : GetGameObjectVec()) {
			if (v->FindTag(L"MS")) {
				auto ptrPos = v->GetComponent<Transform>()->GetPosition();
				if (ptrPos.x > m_FinishPosX) {
					ptrPos.x -= 50.0f;
				}
				v->GetComponent<Transform>()->SetPosition(ptrPos);
			}
			// 惑星sprite
			if (v->FindTag(L"RW")) {
				if (m_Time > 7.0f) {
					v->SetDrawActive(true);
				}
			}
			if (v->FindTag(L"RP")) {
				if (m_Time > 10.0f) {
					v->SetDrawActive(true);
				}
			}
			// スコア最大値
			if (v->FindTag(L"10")) {
				if (m_Time > 10.0f) {
					v->SetDrawActive(true);
				}
			}
			// 取得スコア
			if (v->FindTag(L"NP")) {
				if (m_Time > 15.0f) {
					v->SetDrawActive(true);
				}
			}
			// ランクテキスト
			if (v->FindTag(L"RR")) {
				if (m_Time > 20.0f) {
					v->SetDrawActive(true);
				}
			}
			// 取得ランク
			if (v->FindTag(L"RR")) {
				if (m_Time > 20.0f && !createFlg) {
					createFlg = true;
					//// アニメーションが終了したら
					//m_AnimFlg = true;
					//switch (rankFlg) {
					//case 1:
					//	XAPtr->Start(wstringKey::seRank_S, 0, 0.5f);
					//	break;
					//case 2:
					//	XAPtr->Start(wstringKey::seRank_A, 0, 0.5f);
					//	break;
					//case 3:
					//	XAPtr->Start(wstringKey::seRank_B, 0, 0.5f);
					//	break;
					//case 4:
					//	XAPtr->Start(wstringKey::seRank_C, 0, 0.5f);
					//	break;
					//}
					AddGameObject<Rank>(Vec3(128.0f, 128.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(50.0f, -130.0f, 0.0f));
				}
			}
			if (v->FindTag(L"RW") || v->FindTag(L"RP") || v->FindTag(L"10") || v->FindTag(L"NP") || v->FindTag(L"RR")) {
				if (m_Time > 35.0f) {
					v->SetDrawActive(false);
				}
			}
			if (v->FindTag(L"RT") || v->FindTag(L"TT") || v->FindTag(L"SE") || v->FindTag(L"CP")) {
				if (m_Time > 35.0f) {
						m_CursorFlg = true;
					v->SetDrawActive(true);
				}
			}
		}
	}
	void ResultStage::OnCursorMove() {
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		auto SM = GetSharedGameObject<SelectManager_R>(L"MRSMR");
		int cursorNum = SM->GetCursorPosition();
		wstring now = App::GetApp()->GetScene<Scene>()->GetNowScene();
		shared_ptr<FadeOut> fadeout = nullptr;
		for (auto& obj : GetGameObjectVec()) {
			fadeout = dynamic_pointer_cast<FadeOut>(obj);
			if (fadeout) {
				break;
			}
		}

		for (auto& v : GetGameObjectVec()) {
			if (m_CursorFlg) {
				auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
				auto SM = GetSharedGameObject<SelectManager_R>(L"MRSMR");
				int cursorNum = SM->GetCursorPosition();
				auto XAPtr = App::GetApp()->GetXAudio2Manager();
				wstring now = App::GetApp()->GetScene<Scene>()->GetNowScene();
				if (v->FindTag(L"CP")) {
					auto cursorTransform = v->GetComponent<Transform>();
					auto pos = cursorTransform->GetPosition();
					switch (cursorNum)
					{
					case 1:
						pos.y = 100.0f;
						cursorTransform->SetPosition(pos);
						// Aボタン入力されたらGameStageにシーン遷移
						if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
							AddGameObject<FadeOut>(L"txFade", true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
							XAPtr->Start(wstringKey::seKettei, 0, 0.5f);

							//PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToGameStage");
						}
						//OnSceneTransition(now);
						if (fadeout) {
							if (fadeout->GetAlpha() >= 1.0f) {
								PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), now);
							}
						}

						break;
					case 2:
						pos.y = -50.0f;
						cursorTransform->SetPosition(pos);
						// Aボタン入力されたらTitleStageにシーン遷移
						if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
							AddGameObject<FadeOut>(L"txFade", true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
							XAPtr->Start(wstringKey::seKettei, 0, 0.5f);

							//PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToTitleStage");
						}
						//OnSceneTransition(L"ToSelectStage");
						if (fadeout) {
							if (fadeout->GetAlpha() >= 1.0f) {
								PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToSelectStage");
							}
						}

						break;
					case 3:
						pos.y = -200.0f;
						cursorTransform->SetPosition(pos);
						// Aボタン入力されたらTitleStageにシーン遷移
						if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
							AddGameObject<FadeOut>(L"txFade", true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
							XAPtr->Start(wstringKey::seKettei, 0, 0.5f);
						}
						//OnSceneTransition(L"ToTitleStage");
						//PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToGameStage");
						if (fadeout) {
							if (fadeout->GetAlpha() >= 1.0f) {
								PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToTitleStage");
							}
						}
						//OnSceneTransition(now);
						break;
					}
					//点滅処理
					m_Alpha_C += sin(m_Time * 2.0f);
					if (m_Alpha_C > 1.0f) {
						m_Alpha_C = 1.0f;
					}
					if (m_Alpha_C <= 0.3f) {
						m_Alpha_C = 0.3f;
					}
					v->GetComponent<PCTSpriteDraw>()->SetDiffuse(Col4(1.0f, 1.0f, 1.0f, m_Alpha_C));
				}
			}
		}
	}

	// ゲッター
	bool ResultStage::GetCursorFlg() const
	{
		return m_CursorFlg;
	}

	// シーン遷移(FadeOutのAddは入力の部分で、入力の外にこれ書いてください)
	void ResultStage::OnSceneTransition(const wstring& wstringKey)
	{
		shared_ptr<FadeOut> fadeout = nullptr;
		auto objs = GetGameObjectVec();
		for (auto& obj : objs) {
			fadeout = dynamic_pointer_cast<FadeOut>(obj);
			if (fadeout) {
				if (fadeout->GetAlpha() >= 1.0f) {
					PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), wstringKey);
				}
			}
		}

	}
	// Aボタン
	//void ResultStage::OnPushA() {
	//	PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToGameStage");
	//}

	//--------------------------------------------------------------------------------------
	//	class Rank : public GameObject;
	//	用途: ランク
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Rank::Rank(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_AnimFlg(true),
		m_AnimTipCol(0),
		m_AnimTipRow(0)
	{}

	Rank::Rank(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, const bool& AnimFlgPtr, const int& AnimTipColPtr, const int& AnimTipRowPtr) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_AnimFlg(AnimFlgPtr),
		m_AnimTipCol(AnimTipColPtr),
		m_AnimTipRow(AnimTipRowPtr)
	{}

	//初期化
	void Rank::OnCreate() {

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		//アニメチップ作成
		float tipWidth = 128.0f/512.0f;
		float tipHeight = 1.0f;
		for (int c = 0; c < 1; c++) {
			for (int r = 0; r < 4; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}
		auto scenePtr=App::GetApp()->GetScene<Scene>();
		float planetCount = float(scenePtr->GetPlanetCount());
		float planetCountMax = float(scenePtr->GetPlanetMax());
		float recoveryRate = planetCount / planetCountMax;
		if (recoveryRate >= 0.5f) {
			m_AnimTipRow = int(4 * recoveryRate) - 1;
		}
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		float maxSize = 1.0f;
	    vector<VertexPositionColorTexture> vertices = {
			{Vec3(-0.0f, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+maxSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-0.0f, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+maxSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTSpriteDraw>(vertices, indices);
		PtrSpriteDraw->SetTextureResource(wstringKey::txRank);
		PtrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		PtrSpriteDraw->SetDrawActive(true);
		//透明処理
		SetAlphaActive(true);
		SetDrawLayer(1);
	}
	void Rank::OnUpdate() {
		if (m_AnimFlg) {
			// 描画時間
			float ElapsedTime = App::GetApp()->GetElapsedTime();
			auto XAPtr = App::GetApp()->GetXAudio2Manager();
			m_Time += ElapsedTime * 5.0f;
			auto PtrSpriteDraw = this->AddComponent<PCTSpriteDraw>();
			if (m_Time > 15.0f) {
				PtrSpriteDraw->SetDrawActive(false);
			}
			switch (m_AnimTipRow) {
			case 0:
				if (!m_SEFlg) {
					m_SEFlg = true;
					XAPtr->Start(wstringKey::seRank_C, 0, 0.5f);
				}
				break;
			case 1:
				if (!m_SEFlg) {
					m_SEFlg = true;
					XAPtr->Start(wstringKey::seRank_B, 0, 0.5f);
				}
				break;
			case 2:
				if (!m_SEFlg) {
					m_SEFlg = true;
					XAPtr->Start(wstringKey::seRank_A, 0, 0.5f);
				}
				break;
			case 3:
				if (!m_SEFlg) {
					m_SEFlg = true;
					XAPtr->Start(wstringKey::seRank_S, 0, 0.5f);
				}
				break;
			}
		}

		// セレクトステージ用
		if (!m_AnimFlg) {
			Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
			float HelfSize = 0.5f;
			float maxSize = 1.0f;
			vector<VertexPositionColorTexture> vertices = {
				{Vec3(-0.0f, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
				{Vec3(+maxSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
				{Vec3(-0.0f, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
				{Vec3(+maxSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
			};

			auto spriteDrawPtr = this->GetComponent<PCTSpriteDraw>();
			spriteDrawPtr->UpdateVertices(vertices);
		}
	}
}
//end basecross
