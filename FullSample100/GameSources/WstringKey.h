#pragma once
#include "stdafx.h"

// プロジェクト内で使用するWstring型を登録しておく
namespace wstringKey {
	// テクスチャキー
	const wstring txTrace = L"TRACE_TX";
	const wstring txSky = L"SKY_TX";
	const wstring txWall = L"WALL_TX";
	const wstring txNum = L"Number_TX";
	const wstring txAbc = L"Abc_TX";
	const wstring txABC = L"ABC_TX";
	const wstring txAbcW = L"AbcW_TX";
	const wstring txABCW = L"ABCW_TX";
	const wstring txShip = L"Ship_TX";
	const wstring txMessage = L"Message_TX";
	const wstring txBackGround = L"BG_TX";
	const wstring txBackGround2 = L"BG2_TX";

	const wstring txResultBG = L"ResultBG_TX";
	const wstring txResultWindow = L"ResultWindow_TX";
	const wstring txGameOverBG = L"GameOverBG_TX";
	const wstring txRank = L"Rank_TX";
	const wstring txMissonComplete = L"MissonComplete_TX";
	const wstring txResultRank = L"ResultRank_TX";
	const wstring txTestPlayer = L"TestPlayer_TX";
	const wstring txSearchWave = L"SearchWave_TX";
	const wstring txTestEnemy = L"TestEnemy_TX";
	const wstring txTestWeapon = L"TestWeapon_TX";
	const wstring txTestFloor = L"TestFloor_TX";
	const wstring txTestSpiral = L"TestSpiral_TX";
	const wstring txTestSpiral2 = L"TestSpiral2_TX";
	const wstring txTestStar = L"TestStar_TX";
	const wstring txFloor = L"Floor_TX";
	const wstring txHpFrame = L"HpFreame_TX";
	const wstring txHpBar = L"HpBar_TX";
	const wstring txEnergyFrame = L"EnergyFreame_TX";
	const wstring txEnergyBar = L"EnergyBar_TX";
	const wstring txRadarGage = L"RadarGage_TX";
	const wstring txSpeedFrame = L"SpeedFreame_TX";
	const wstring txSpeedBar = L"Speed_TX";
	const wstring txWeponGageFrame1 = L"WeponGageFrame1_TX";
	const wstring txWeponGageFrame2 = L"WeponGageFrame2_TX";
	const wstring txWeponGageFrame3 = L"WeponGageFrame3_TX";
	const wstring txExplosion = L"Explosion_TX";
	const wstring txPlayerBullet = L"PlayerBullet_TX";
	const wstring txDamage = L"Damage_TX";
	const wstring txDamage2 = L"Damage2_TX";
	const wstring txWave1 = L"Wave1_TX";
	const wstring txPlanet_R = L"Planet_R_TX";
	const wstring txPlanet_Bl = L"Planet_BL_TX";
	const wstring txPlanet_G = L"Planet_G_TX";
	const wstring txPlanet_Y = L"Planet_Y_TX";
	const wstring txPlanet_P = L"Planet_P_TX";
	const wstring txPlanet_Br = L"Planet_Br_TX";
	const wstring txPlanets = L"Planets_TX";
	const wstring txDebris = L"Debris_TX";
	const wstring txItem = L"Item_TX";
	const wstring txTitle_Logo = L"Title_TX";
	const wstring txStageSelect_Button = L"SS_Button_TX";
	const wstring txStageSelect_Cursor = L"SS_Cursor_TX";
	const wstring txGameOver_Logo = L"GO_Logo_TX";
	const wstring txCopyLight = L"CopyLight_TX";
	const wstring txRank_A = L"Rank_A_TX";
	const wstring txRank_B = L"Rank_B_TX";
	const wstring txRank_C = L"Rank_C_TX";
	const wstring txRank_S = L"Rank_S_TX";
	const wstring txStageSelectNo = L"StageSelectStNo";
	const wstring txGameOver_Retry = L"GO_Retry_TX";
	const wstring txGameOver_Select = L"GO_Select_TX";
	const wstring txGameOver_Title = L"GO_Title_TX";
	const wstring txGoal = L"Goal_TX";
	const wstring txGoalText = L"GoalText_TX";
	const wstring txGameOverText = L"GameOverText_TX";
	const wstring txArrow = L"Arrow_TX";

	//チュートリアルテキスト
	const wstring txT_Text1 = L"T_Text1_TX";
	const wstring txT_Text2 = L"T_Text2_TX";
	const wstring txT_Text3 = L"T_Text3_TX";
	const wstring txT_Text4 = L"T_Text4_TX";
	const wstring txT_Text5 = L"T_Text5_TX";
	const wstring txT_Text6 = L"T_Text6_TX";
	const wstring txT_Text7 = L"T_Text7_TX";
	const wstring txT_Text8 = L"T_Text8_TX";

	const wstring txT_Window1 = L"T_Window1_TX";
	const wstring txT_Check = L"T_Check_TX";
	const wstring txT_Anim = L"T_Anim_TX";

	// メッシュキー
	const wstring meshSquare = L"DEFAULT_SQUARE";
	const wstring meshSphere = L"DEFAULT_SPHERE";
	const wstring meshBox = L"DEFAULT_CUBE";
	const wstring meshPlayer = L"Player_MESH";
	const wstring meshHammer1 = L"Hammer1_MESH";
	const wstring meshHammer2 = L"Hammer2_MESH";
	const wstring meshHammer3 = L"Hammer3_MESH";
	const wstring meshSword1 = L"Sword1_MESH";
	const wstring meshSword2 = L"Sword2_MESH";
	const wstring meshSword3 = L"Sword3_MESH";
	const wstring meshEnemy1 = L"Enemy1_MESH";
	const wstring meshEnemy2 = L"Enemy2_MESH";
	const wstring meshCube = L"Cube_MESH";
	const wstring meshStageObject1 = L"StageObject1_MESH";
	const wstring meshStageObject2 = L"StageObject2_MESH";
	const wstring meshStageObject3 = L"StageObject3_MESH";

	// アニメキー
	const wstring animeHammer3Default = L"Hammer3Default";
	const wstring animeEnemyDefalt = L"EnemyDefalt";
	const wstring animeEnemy2Defalt = L"Enemy2Defalt";
	const wstring animeEnemyAttackStart = L"EnemyAttackStart";
	const wstring animeEnemyAttackMiddle = L"EnemyAttackMiddle";
	const wstring animeEnemyAttackEnd = L"EnemyAttackEnd";
	const wstring animeEnemy2Attack = L"Enemy2Attack";
	const wstring animeCube = L"Cube";

	// タグキー
	const wstring tagField = L"Field";
	const wstring tagFieldObject = L"FieldObject";
	const wstring tagAbsorptionObject = L"AbsorptionObject";
	const wstring tagAbsorptionCoreObject = L"AbsorptionCoreObject";
	const wstring tagSatellite = L"Satellite";
	const wstring tagGoalArea = L"GoalArea";
	const wstring tagBreakableObject = L"BreakableObject";
	const wstring tagPlayer = L"Player";
	const wstring tagPlayerBullet = L"PlayerBullet";
	const wstring tagRadarGageBar = L"RadarGageBar";
	const wstring tagPlayerWeaponAttackArea = L"PlayerWeaponAttackArea";
	const wstring tagEnemy = L"Enemy";
	const wstring tagEnemyBullet = L"EnemyBullet";
	const wstring tagItem = L"Item";
	const wstring tagStartSprite = L"StartSprite";

	// オブジェクトキー
	const wstring obField = L"Field";
	const wstring obPlayer = L"Player";
	const wstring obRadar = L"Radar";
	const wstring obRadarGage = L"RadarGage";
	const wstring obSearchArea = L"SearchArea";
	const wstring obPlayerWeapon = L"PlayerWeapon";
	const wstring obPlayerWeaponAttackArea = L"PlayerWeaponAttackArea";
	const wstring obPlayerHpBar = L"PlayerHpBar";
	const wstring obBossHpBar = L"BossHpBar";
	const wstring obPlayerWeaponGage = L"PlayerWeaponGage";
	const wstring obPlayerWeaponBar = L"PlayerWeaponBar";
	const wstring obGoal = L"Goal";

	const wstring obBoss = L"Boss";

	// オブジェクトグループキー
	const wstring obgSearchArea = L"SearchArea";
	const wstring obgTestCubes = L"TestCubes";
	const wstring obgEnemy = L"Enemys";
	const wstring obgEnemyBullet = L"EnemyBullets";
	const wstring obgConnectCube = L"ConnectCubes";

	//エフェクトキー
	const wstring effectExplosion = L"ExplosionEffect";
	const wstring effectEnemyBullet = L"EnemyBulletEffect";
	const wstring effectDamage = L"DamageEffect";

	// サウンドキー
	const wstring seDrill1 = L"PlayerDrill1_SE";
	const wstring seDrill2 = L"PlayerDrill2_SE";
	const wstring seKettei = L"Kettei_SE";
	const wstring seCursor = L"Cursor_SE";
	const wstring seCharge1 = L"Charge1_SE";
	const wstring seCharge2 = L"Charge2_SE";
	const wstring seShot = L"Shot_SE";
	const wstring seBoost = L"Boost_SE";
	const wstring seComplete = L"Complete_SE";
	const wstring sePlayerJump = L"PlayerJump_SE";
	const wstring sePlayerSwing = L"PlayerSwing_SE";
	const wstring sePlayerItemGet = L"PlayerItemGet_SE";
	const wstring sePlayerDamage1 = L"PlayerDamage1_SE";
	const wstring sePlayerDamage2 = L"PlayerDamage2_SE";
	const wstring seEnemyDamage = L"EnemyDamage_SE";
	const wstring seEnemyDestroy = L"EnemyDestroy_SE";
	const wstring seClear = L"Clear_SE";
	const wstring seRank_S = L"S_SE";
	const wstring seRank_A = L"A_SE";
	const wstring seRank_B = L"B_SE";
	const wstring seRank_C = L"C_SE";
	const wstring seExplosion = L"Explosion_SE";
	const wstring seSearch = L"Search_SE";
	const wstring seBoostLoop = L"BoostLoop_SE";

	// BGMキー
	const wstring bgmTitle = L"Title_BGM";
	const wstring bgmSelect = L"Select_BGM";
	const wstring bgmGame = L"Game_BGM";
	const wstring bgmResult = L"Result_BGM";
	const wstring bgmGOver = L"GameOver_BGM";
	// ステージキー
}