#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class SearchArea : public GameObject;
	//	用途: 索敵範囲
	//--------------------------------------------------------------------------------------
	//構築と破棄
	SearchArea::SearchArea(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<GameObject> parentPtr) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_ParentPtr(parentPtr)
	{
	}
	//初期化
	void SearchArea::OnCreate() {
		auto ptrTransform = GetComponent<Transform>();
		//親子関係化
		ptrTransform->SetParent(m_ParentPtr);

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		//CollisionSphere衝突判定を付ける
		auto ptrColl = AddComponent<CollisionSphere>();
		ptrColl->SetAfterCollision(AfterCollision::None);

		//アニメチップ
		float tipWidth = 128.0f / 1024.0f;
		float tipHeight = 1.0f;
		for (int c = 0; c < 1; c++) {
			for (int r = 0; r < 8; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 0;
		m_AnimTipCol = 0;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.75f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};


		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);
		PtrSpriteDraw->SetTextureResource(wstringKey::txSearchWave);
		PtrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		//透明処理
		SetAlphaActive(true);

		SetDrawLayer(4);
		m_SeFlg = false;
		////文字列をつける
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
	}

	////衝突判定
	//衝突したら
	void SearchArea::OnCollisionEnter(shared_ptr<GameObject>& other) {
	}
	//衝突し続けてるとき
	void SearchArea::OnCollisionExcute(shared_ptr<GameObject>& other) {
		auto player = dynamic_pointer_cast<Player>(m_ParentPtr);
		if (player->GetStateMachine()->GetCurrentState() != PlayerDead::Instance()) {
			auto& v = other;
			if (v->FindTag(wstringKey::tagAbsorptionCoreObject)) {
				float data = 0.5f;
				auto core = dynamic_pointer_cast<TestStageObjectChildren>(v);
				auto deltaTime = App::GetApp()->GetElapsedTime();
				auto corePos = core->GetComponent<Transform>()->GetWorldPosition();
				Vec3 dir = GetComponent<Transform>()->GetPosition() - corePos;

				Vec3 halfScale = GetComponent<Transform>()->GetScale() / 2.0f;
				halfScale = halfScale * dir.normalize();
				Vec3 magnificationScale = (halfScale - dir);
				magnificationScale = magnificationScale / halfScale;
				auto magnification = (magnificationScale.x + magnificationScale.y) / 2.0f;

				core->m_Gage += data * deltaTime*(0.2f + 0.8f*magnification);
			}
		}
	}
	//衝突し終わったら
	void SearchArea::OnCollisionExit(shared_ptr<GameObject>& other) {
	}

	//更新
	void SearchArea::OnUpdate() {
		m_delta += App::GetApp()->GetElapsedTime();
		float HelfSize = 0.5f;
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		auto animTip = m_AnimTip[m_AnimTipCol][m_AnimTipRow];
		vector<VertexPositionColorTexture> vertices = {
			{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
			{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(animTip.right, animTip.top) },
			{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
			{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(animTip.right, animTip.bottom) }
		};
		auto drawComp = GetComponent<PCTStaticDraw>();
		drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);
		if (m_delta >= 0.1f) {
			if (m_AnimTipRow == 0&&m_SeFlg) {
				m_AudioManager = App::GetApp()->GetXAudio2Manager();
				m_SE = m_AudioManager->Start(wstringKey::seSearch, 0, 0.3f);
			}
			m_delta = 0.0f;

			if (m_AnimTipRow < 6) {
				m_AnimTipRow++;
			}
			else {
				m_AnimTipRow = 0;
			}
		}

	}
	void SearchArea::OnUpdate2() {
		////文字列の表示
		//DrawStrings();
	}

	//文字列の表示
	void SearchArea::DrawStrings() {

		//文字列表示
		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring fpsStr(L"FPS: ");
		fpsStr += Util::UintToWStr(fps);
		fpsStr += L"\nElapsedTime: ";
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		fpsStr += Util::FloatToWStr(ElapsedTime);
		fpsStr += L"\n";

		auto pos = GetComponent<Transform>()->GetPosition();
		wstring positionStr(L"Position:\t");
		positionStr += L"X=" + Util::FloatToWStr(pos.x, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Y=" + Util::FloatToWStr(pos.y, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Z=" + Util::FloatToWStr(pos.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring speadStr(L"Spead:\t");
		//auto gravVelocity = GetComponent<Gravity>()->GetGravityVelocity();
		//speadStr += Util::FloatToWStr(m_Spead*100.0f, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Y=" + Util::FloatToWStr(gravVelocity.y, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Z=" + Util::FloatToWStr(gravVelocity.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring str = fpsStr + positionStr + speadStr;
		//文字列コンポーネントの取得
		auto ptrString = GetComponent<StringSprite>();
		ptrString->SetText(str);
	}
}