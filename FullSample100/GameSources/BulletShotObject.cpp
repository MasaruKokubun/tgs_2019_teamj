/*!
@file BulletShotObject.cpp
@brief 弾を撃つオブジェクト実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{
	//--------------------------------------------------------------------------------------
	/// class BulletShotObject : public GameObject      弾を撃つ範囲
	//--------------------------------------------------------------------------------------
	// 構築と破棄
	BulletShotObject::BulletShotObject(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Rotation,
		const Vec3& Position,
		const float& Span
	) :
		TestStageObject(StagePtr, Scale, Rotation, Position, 1, 1),
		m_Angle(Vec3(0.0f, 0.0f, 0.0f)),
		m_Span(Span),
		m_Time(0.0f),
		m_GagePtr(nullptr)
	{
	}
	BulletShotObject::~BulletShotObject() {}

	// 初期化
	void BulletShotObject::OnCreate()
	{
		TestStageObject::OnCreate();
	}
	
	// 更新
	void BulletShotObject::OnUpdate()
	{
		TestStageObject::OnUpdate();
		auto elapsedTime = App::GetApp()->GetElapsedTime();
		// playerを探す
		shared_ptr<Player> player = nullptr;
		for (auto& obj : GetStage()->GetGameObjectVec())
		{
			player = dynamic_pointer_cast<Player>(obj);
			if (player)
			{
				break;
			}
		}
		// playerとの位置、向きを測る
		auto playerPos = player->GetComponent<Transform>()->GetPosition();
		auto dir = m_Position - playerPos;
		auto angle = playerPos - m_Position;
		m_Angle = angle.normalize();
		auto halfScale = m_Scale * 0.5f;

		// 絶対値にする
		if (dir.x < 0)
		{
			dir.x = -dir.x;
		}
		if (dir.y < 0)
		{
			dir.y = -dir.y;
		}

		if ((dir.x <= halfScale.x * 1.5f) && (dir.y <= halfScale.y * 1.5f))
		{
			// 弾のクールタイムを計る
			m_Time += elapsedTime;

			auto XAPtr = App::GetApp()->GetXAudio2Manager();
			// クールタイム終わってもまだいるなら弾を撃つ
			if (m_Time >= m_Span) {
				auto group = GetStage()->GetSharedObjectGroup(L"EnemyBulletGroup");
				auto& vec = group->GetGroupVector();
				for (auto& v : vec) {
					auto shObj = v.lock();
					if (shObj) {
						if (!shObj->IsUpdateActive()) {
							auto shBullet = dynamic_pointer_cast<EnemyBullet>(shObj);
							if (shBullet) {
								shBullet->Reset(m_Position, m_Angle);
								XAPtr->Start(L"FireBall_SE", 0, 0.5f);
								m_Time = 0.0f;
								return;
							}
						}
					}
				}
				GetStage()->AddGameObject<EnemyBullet>(Vec3(0.2f, 0.2f, 0.2f), Vec3(0.0f, 0.0f, 0.0f), m_Position, m_Angle);
				m_Time = 0.0f;
			}

		}
		else
		{
			m_Time = 0.0f;
		}
	}


	//--------------------------------------------------------------------------------------
	//	class EnemyBullet : public GameObject;
	//	用途: 敵用の弾オブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	EnemyBullet::EnemyBullet(const shared_ptr<Stage>& stagePtr, 
		const Vec3& scale, 
		const Vec3& rotation, 
		const Vec3& position, 
		const Vec3& angle
	) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_Angle(angle)
	{
	}
	//初期化
	void EnemyBullet::OnCreate() {

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);
		m_Spead = 10.0f;
		//CollisionSphere衝突判定を付ける
		auto ptrColl = AddComponent<CollisionSphere>();
		ptrColl->SetAfterCollision(AfterCollision::None);
		//タグをつける
		AddTag(wstringKey::tagEnemyBullet);


		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		vector<VertexPositionColorTexture> vertices = {
			{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(0.0f,0.0f) },
			{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(1.0f, 0.0f) },
			{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(0.0f ,1.0f) },
			{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(1.0f, 1.0f) }
		};
		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);

		PtrSpriteDraw->SetTextureResource(wstringKey::txPlayerBullet);

		//透明処理
		SetAlphaActive(true);

		// 敵用の弾のグループに入れる
		auto group = GetStage()->GetSharedObjectGroup(L"EnemyBulletGroup");
		group->IntoGroup(GetThis<GameObject>());

		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		XAPtr->Start(L"FireBall_SE", 0, 0.5f);

		//文字列をつける
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
	}

	////衝突判定
	//衝突したら
	void EnemyBullet::OnCollisionEnter(shared_ptr<GameObject>& other) {
		if (other->FindTag(wstringKey::tagPlayer))
		{
			shared_ptr<Player> player = nullptr;
			player = dynamic_pointer_cast<Player>(other);
			player->Damage(0.025f);
			SetUpdateActive(false);
			SetDrawActive(false);

		}
	}
	//衝突し続けてるとき
	void EnemyBullet::OnCollisionExcute(shared_ptr<GameObject>& other) {

	}
	//衝突し終わったら
	void EnemyBullet::OnCollisionExit(shared_ptr<GameObject>& other) {

	}

	//更新
	void EnemyBullet::OnUpdate() {
		auto pos = GetComponent<Transform>()->GetPosition();
		pos += m_Angle * m_Spead * App::GetApp()->GetElapsedTime();
		GetComponent<Transform>()->SetPosition(pos);

		Rect rect;
		for (auto& obj : GetStage()->GetGameObjectVec())
		{
			auto bgScroll = dynamic_pointer_cast<BGScroll>(obj);
			if (bgScroll) {
				rect = bgScroll->GetRect();
			}
		}

		// ステージ外に出たら
		if ((pos.x <= rect.x || pos.x >= rect.w) || (pos.y <= rect.y || pos.y >= rect.h)) {
			SetUpdateActive(false);
			SetDrawActive(false);
			return;
		}
	}
	void EnemyBullet::OnUpdate2() {
		////文字列の表示
		//DrawStrings();
	}

	// オブジェクトのリセット
	void EnemyBullet::Reset(const Vec3& pos, const Vec3& angle) {
		auto ptrTrans = GetComponent<Transform>();
		ptrTrans->ResetPosition(pos);
		m_Angle = angle;
		SetUpdateActive(true);
		SetDrawActive(true);
	}

	//文字列の表示
	void EnemyBullet::DrawStrings() {

		//文字列表示
		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring fpsStr(L"FPS: ");
		fpsStr += Util::UintToWStr(fps);
		fpsStr += L"\nElapsedTime: ";
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		fpsStr += Util::FloatToWStr(ElapsedTime);
		fpsStr += L"\n";

		auto pos = GetComponent<Transform>()->GetPosition();
		wstring positionStr(L"Position:\t");
		positionStr += L"X=" + Util::FloatToWStr(pos.x, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Y=" + Util::FloatToWStr(pos.y, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Z=" + Util::FloatToWStr(pos.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring speadStr(L"Spead:\t");
		//auto gravVelocity = GetComponent<Gravity>()->GetGravityVelocity();
		//speadStr += Util::FloatToWStr(m_Spead*100.0f, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Y=" + Util::FloatToWStr(gravVelocity.y, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Z=" + Util::FloatToWStr(gravVelocity.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring str = fpsStr + positionStr + speadStr;
		//文字列コンポーネントの取得
		auto ptrString = GetComponent<StringSprite>();
		ptrString->SetText(str);
	}

}
//end basecross


