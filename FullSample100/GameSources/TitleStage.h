/*!
@file GameStage.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {



	//--------------------------------------------------------------------------------------
	//	タイトルステージクラス
	//--------------------------------------------------------------------------------------
	class TitleStage : public Stage {
		shared_ptr<SoundItem> m_BGM;
		float m_Time;
		float m_Alpha;
		float m_XPos = -1500.0f;
		//フェード用
		bool m_FadeFlg = false;
		// ビューの作成
		void CreateViewLight();
		// スプライトの作成
		void CreateTitleSprite();
		//プレイヤーの生成
		void CreateAnimPlayer();
		// 入力ハンドラー
		//InputHandler<ResultStage> m_InputHandler;
	public:
		// 構築と破棄
		TitleStage() : Stage() {}
		virtual ~TitleStage() {}
		//初期化
		virtual void OnCreate() override;
		// 更新
		virtual void OnUpdate() override;
		// Aボタン
		//void OnPushA();
		//アニメーション
		void OnAnimation();
	};

	//--------------------------------------------------------------------------------------
	///	プレイヤーアニメーション
	//--------------------------------------------------------------------------------------
	class TitlePlayer : public GameObject
	{
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;

		bool m_Destroyflg;
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[4][4];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
		float m_delta;
	public:
		//コンストラクタ&デストラクタ
		TitlePlayer(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position);
		~TitlePlayer() {}
		//初期化
		virtual void OnCreate() override;
		// 更新
		virtual void OnUpdate() override;
	};

	//--------------------------------------------------------------------------------------
	///	レーダー
	//--------------------------------------------------------------------------------------
	class TitleRadar :public GameObject
	{
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		shared_ptr<GameObject> m_ParentPtr;
	public:
		float m_Delta;
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[4][4];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
		//コンストラクタ&デストラクタ
		TitleRadar(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<GameObject> parentPtr = nullptr);
		~TitleRadar() {}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
	};

}
//end basecross

