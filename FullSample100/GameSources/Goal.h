/*!
@file Goal.h
@brief ゴール
*/

#pragma once
#include "stdafx.h"

namespace basecross{

	//--------------------------------------------------------------------------------------
	///	ゴール
	//--------------------------------------------------------------------------------------
	class GoalObject : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		shared_ptr<GameObject> m_ParentPtr;

		bool m_GoalFlg;
		bool m_GoalFadeFlg;

		Vec2 m_TextScale;
		Vec2 m_TextPosition;
		float m_TextSpeed;
	public :
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[1][1];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;

		//構築と破棄
		//--------------------------------------------------------------------------------------
		/*!
		@brief	コンストラクタ
		@param[in]	StagePtr	ステージ
		*/
		//--------------------------------------------------------------------------------------
		GoalObject(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const shared_ptr<GameObject>& parentPtr = nullptr
		);
		GoalObject(const shared_ptr<Stage>& StagePtr,
			const Vec3& Scale,
			const Vec3& Rotation,
			const Vec3& Position,
			const shared_ptr<GameObject>& parentPtr,
			const Vec2& TextScalePtr,
			const Vec2& TextPositionPtr,
			const float& TextSpeedPtr
		);
		//--------------------------------------------------------------------------------------
		/*!
		@brief	デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~GoalObject();

		// 初期化
		virtual void OnCreate() override;
		virtual void OnUpdate() override;
		virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;

		Vec2 GetTextPosition() const {
			return m_TextPosition;
		}
		void SetTextPosition(const Vec2& TextPositionPtr) {
			m_TextPosition = TextPositionPtr;
		}
	};

	//--------------------------------------------------------------------------------------
	/// ゴールエリア
	//--------------------------------------------------------------------------------------
	class GoalArea :public GameObject
	{
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[1][2];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
	public:
		//コンストラクタ&デストラクタ
		GoalArea(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position);
		~GoalArea() {}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
		virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;
		virtual void OnCollisionExcute(shared_ptr<GameObject>& Other) override;
		virtual void OnCollisionExit(shared_ptr<GameObject>& Other) override;
	};

}
//end basecross

