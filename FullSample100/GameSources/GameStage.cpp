/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス実体
	//--------------------------------------------------------------------------------------
	void GameStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<MyCamera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(Vec3(0.0f, 5.0f, -5.0f));
		PtrCamera->SetAt(Vec3(0.0f, 0.0f, 0.0f));
		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}
	void GameStage::CreateBG() {
		auto backGroundPtr = AddGameObject<BGScroll>(wstringKey::txBackGround, true,
			Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
		backGroundPtr->SetDrawLayer(-1);
	}
	//テストボックスの作成
	void GameStage::CreateTestBox() {
		//テストフレーム
		AddGameObject<FrameBox>(Vec3(50.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(25.0f, 0.0f, 0.0f));
		AddGameObject<FrameBox>(Vec3(50.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(25.0f, 15.0f, 0.0f));
		AddGameObject<FrameBox>(Vec3(1.0f, 15.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 7.5f, 0.0f));
		AddGameObject<FrameBox>(Vec3(1.0f, 15.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(50.0f, 7.5f, 0.0f));

		// 障害物オブジェクト
		//AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(14.0f, 6.0f, 0.0f));
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(20.0f, 5.0f, 0.0f));
		AddGameObject<TestItem>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(16.0f, 10.0f, 0.0f));
		AddGameObject<TestItem>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(40.0f, 5.0f, 0.0f));
		//テストオブジェクト
		Vec3 scale(4.0f, 4.0f, 4.0f);
		scale *= 1.5f;
		AddGameObject<TestStageObject>(scale, Vec3(0.0f, 0.0f, 0.0f), Vec3(7.5f, 7.5f, 0.0f));
		AddGameObject<BulletShotObject>(scale, Vec3(0.0f, 0.0f, 0.0f), Vec3(27.0f, 5.0f, 0.0f));
		CreateSharedObjectGroup(L"EnemyBulletGroup");

		// ゴール用オブジェクト
		auto goalPtr = AddGameObject<GoalArea>(scale, Vec3(0.0f, 0.0f, 0.0f), Vec3(46.0f, 7.5f, 0.0f));
		SetSharedGameObject(wstringKey::obGoal, goalPtr);

	}
	//テストプレイヤーの作成
	void GameStage::CreateTestPlayer() {
		CreateSharedObjectGroup(wstringKey::obgSearchArea);
		auto playerPtr=AddGameObject<Player>(Vec3(0.5f, 0.5f, 0.5f), Vec3(0.0f, 0.0f, -90.0f * (XM_PI / 180.0f)), Vec3(1.0f, 7.5f, 0.0f), Vec3(1.0f, 0.0001f, 0.0f));
		SetSharedGameObject(wstringKey::obPlayer, playerPtr);
		//レーダーゲージ
		auto radarGagePtr = AddGameObject<RadarGageBar>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f));
		SetSharedGameObject(wstringKey::obRadarGage, radarGagePtr);
		//エネルギー
		auto scale = 1.5f;
		AddGameObject<EnergyGageBar>(Vec3(256.0f*scale, 128.0f*scale, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(-500.0f- 256.0f/2.0f, -360.0f, 0.0f), playerPtr);
	}
	void GameStage::CreateUISprite() {
	}
	void GameStage::CreatePlanetUISprite() {
		auto ptrPlanet = AddGameObject<PlanetUISprite>(wstringKey::txPlanets, true,
			Vec2(64.0f, 64.0f), Vec2(300.0f, -360.0f));
		ptrPlanet->SetDrawLayer(1000);
		ptrPlanet->AddTag(L"SCORE");
	}

	void GameStage::CreatePlanetCounter() {
		UINT PlanetCount = App::GetApp()->GetScene<Scene>()->GetPlanetCount();
		// スコアカウンター
		UINT CountMAX = App::GetApp()->GetScene<Scene>()->GetPlanetMax();
		AddGameObject<Score>(Vec2(50.0f,65.0f), Vec2(420.0f, -360.0f), PlanetCount, 2, CountMAX, true);
		AddGameObject<AbcSprite>(wstringKey::txABCW, true, Vec2(80.0f, 80.0f), Vec2(475.0f, -360.0f));
		// スコアの最大値
		AddGameObject<Score>(Vec2(50.0f, 65.0f), Vec2(570.0f, -360.0f), CountMAX, 2, CountMAX, false);
	}

	void GameStage::CreateParticle() {
		// Bomb
		// パーティクルの準備
		auto bombPtr = AddGameObject<BombParticle>();
		// 共有オブジェクトに登録
		SetSharedGameObject(L"BombParticle", bombPtr);

		// Recovery
		// パーティクルの準備
		auto recoveryPtr = AddGameObject<RecoveryParticle>();
		// 共有オブジェクトに登録
		SetSharedGameObject(L"RecoveryParticle", recoveryPtr);

	}

	void GameStage::CreateTutorialText() {
		float TextSizeW = 1000.0f;
		float TexsSizeH = 100.0f;
		auto tcount = 0;

		
		auto ptrT_Text1 = AddGameObject<TutorialText>(wstringKey::txT_Text1, true,
			Vec2(TextSizeW, TexsSizeH), Vec2(0.0f, 250.0f));
		auto ptrT_Text2 = AddGameObject<TutorialText>(wstringKey::txT_Text2, true,
			Vec2(TextSizeW, TexsSizeH), Vec2(0.0f, 250.0f));
		auto ptrT_Text3 = AddGameObject<TutorialText>(wstringKey::txT_Text3, true,
			Vec2(TextSizeW, TexsSizeH * 2.0f), Vec2(0.0f, 250.0f));
		auto ptrT_Text4 = AddGameObject<TutorialText>(wstringKey::txT_Text4, true,
			Vec2(TextSizeW, TexsSizeH * 2.0f), Vec2(0.0f, 250.0f));
		auto ptrT_Text5 = AddGameObject<TutorialText>(wstringKey::txT_Text5, true,
			Vec2(512.0f, 64.0f ), Vec2(430.0f, -280.0f));
		auto ptrT_Text6 = AddGameObject<TutorialText>(wstringKey::txT_Text6, true,
			Vec2(436.0f, 96.0f ), Vec2(-430.0f, -230.0f));
		auto ptrT_Text7 = AddGameObject<TutorialText>(wstringKey::txT_Text7, true,
			Vec2(TextSizeW, TexsSizeH * 2.0f), Vec2(0.0f, 250.0f));
		auto ptrT_Text8 = AddGameObject<TutorialText>(wstringKey::txT_Text8, true,
			Vec2(TextSizeW, TexsSizeH * 2.0f), Vec2(0.0f, 250.0f));

		auto ptrT_Window1 = AddGameObject<TutorialText>(wstringKey::txT_Window1, true,
			Vec2(TextSizeW, TexsSizeH * 2.0f), Vec2(0.0f, 250.0f));
		auto ptrT_Check = AddGameObject<TutorialText>(wstringKey::txT_Check, true,
			Vec2(400.0f, 200.0f), Vec2(430.0f, -230.0f));
		auto ptrT_Anim = AddGameObject<TutorialAnimSprite>(wstringKey::txT_Anim, true,
			Vec2(360.0f, 225.0f), Vec2(430.0f, -210.0f),4,2,0.1f);

		ptrT_Text1->SetDrawActive(false);
		ptrT_Text1->SetDrawLayer(5);
		SetSharedGameObject(L"Text1", ptrT_Text1);

		ptrT_Text2->SetDrawActive(false);
		ptrT_Text2->SetDrawLayer(5);
		SetSharedGameObject(L"Text2", ptrT_Text2);

		ptrT_Text3->SetDrawActive(false);
		ptrT_Text3->SetDrawLayer(5);
		SetSharedGameObject(L"Text3", ptrT_Text3);

		ptrT_Text4->SetDrawActive(false);
		ptrT_Text4->SetDrawLayer(5);
		SetSharedGameObject(L"Text4", ptrT_Text4);

		ptrT_Text5->SetDrawActive(true);
		ptrT_Text5->SetDrawLayer(5);
		SetSharedGameObject(L"Text5", ptrT_Text5);

		ptrT_Text6->SetDrawActive(true);
		ptrT_Text6->SetDrawLayer(6);
		SetSharedGameObject(L"Text6", ptrT_Text6);

		ptrT_Text7->SetDrawActive(false);
		ptrT_Text7->SetDrawLayer(6);
		SetSharedGameObject(L"Text7", ptrT_Text7);

		ptrT_Text8->SetDrawActive(false);
		ptrT_Text8->SetDrawLayer(6);
		SetSharedGameObject(L"Text8", ptrT_Text8);

		ptrT_Window1->SetDrawActive(true);
		ptrT_Window1->SetDrawLayer(6);
		SetSharedGameObject(L"Window1", ptrT_Window1);


		ptrT_Check->SetDrawActive(false);
		ptrT_Check->SetDrawLayer(5);
		SetSharedGameObject(L"Check", ptrT_Check);


		ptrT_Anim->SetDrawActive(false);
		ptrT_Anim->SetDrawLayer(5);
		SetSharedGameObject(L"TAnim", ptrT_Anim);


	}

	void GameStage::OnCreate() {
		try {

			// スコアのリセット
			App::GetApp()->GetScene<Scene>()->ResetScore();

			//ビューとライトの作成
			CreateViewLight();
			//背景の作成
			CreateBG();
			//テストボックスの作成
			CreateTestBox();
			//テストプレイヤーの作成
			CreateTestPlayer();
			//スピードUIの作成
			CreateUISprite();
			//惑星UIの作成
			CreatePlanetUISprite();
			//数字の作成
			CreatePlanetCounter();
			// パーティクルの作成
			CreateParticle();
			// FadeIn
			AddGameObject<FadeIn>(L"txFade", true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
			//チュートリアルテキスト
			CreateTutorialText();
		}
		catch (...) {
			throw;
		}
	}

	void GameStage::OnUpdate() {
		UINT PlanetCount = App::GetApp()->GetScene<Scene>()->GetPlanetCount();

		// 惑星カウンターの更新
		shared_ptr<Score> score = nullptr;
		auto gameObjects = GetGameObjectVec();
		for (auto obj : gameObjects) {
			score = dynamic_pointer_cast<Score>(obj);
			if (score)	break;
		}

		// スコアを加算する
		if (score) {
			score->SetValue(PlanetCount);
		}

		//チュートリアル
		TextUpdate();

		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//リセットコマンド
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_START&&CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_BACK) {
			App::GetApp()->GetScene<Scene>()->ResetScore();
			//ScorePtr1->SetScore(PlanetCount);
			PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToTitleStage");
		}
	}

	void GameStage::OnDestroy() {
		Stage::OnDestroy();
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		if (m_BGM) {
			XAPtr->Stop(m_BGM);
		}
	}
	//　テキスト更新
	void GameStage::TextUpdate() {
		//メモ
		//bool sw = false;
		//実装したいTutorial　燃料ゲージの説明、惑星グルグル、コース説明
		auto PC = App::GetApp()->GetScene<Scene>()->GetPlanetCount();
		auto searchArea = this->GetSharedGameObject<SearchArea>(wstringKey::obSearchArea);
		auto SA = searchArea->m_SeFlg;
		auto T1 = GetSharedGameObject<TutorialText>(L"Text1");
		auto T2 = GetSharedGameObject<TutorialText>(L"Text2");
		auto T3 = GetSharedGameObject<TutorialText>(L"Text3");
		auto T4 = GetSharedGameObject<TutorialText>(L"Text4");
		auto T5 = GetSharedGameObject<TutorialText>(L"Text5");
		auto T6 = GetSharedGameObject<TutorialText>(L"Text6");
		auto T7 = GetSharedGameObject<TutorialText>(L"Text7");
		auto T8 = GetSharedGameObject<TutorialText>(L"Text8");

		auto TW = GetSharedGameObject<TutorialText>(L"Window1");

		auto TC = GetSharedGameObject<TutorialText>(L"Check");
		auto TA = GetSharedGameObject<TutorialAnimSprite>(L"TAnim");
		
		//TC->GetAlphaActive

		if (SA == true && PC < 1)
		{

			T1->SetDrawActive(false);
			T2->SetDrawActive(true);
			TA->SetDrawActive(true);
		}else if (PC < 1) 
		{
			TW->SetDrawActive(false);
			T5->SetDrawActive(false);
			T6->SetDrawActive(false);
			T2->SetDrawActive(false);
			T1->SetDrawActive(true);
			TA->SetDrawActive(false);
		}

		else if (SA == true && PC == 1 && m_sw == false)
		{
			T3->SetDrawActive(false);
			TC->SetDrawActive(false);
			T2->SetDrawActive(false);
			TA->SetDrawActive(false);
			T7->SetDrawActive(true);
		}
		else if (SA == true && PC == 1 && m_sw == true)
		{
			T3->SetDrawActive(false);
			TC->SetDrawActive(false);
			T8->SetDrawActive(true);
		}

		else if (PC  == 1 && m_sw ==false) {
			m_sw = true;
			T7->SetDrawActive(false);
			T3->SetDrawActive(true);
			TC->SetDrawActive(true);
			//TA->SetDrawActive(false);
		}
		else if (PC > 1 ) {
			T8->SetDrawActive(false);
			T4->SetDrawActive(true);
		}
	}

	
}
//end basecross
