/*!
@file ProjectBehavior.h
@brief プロジェク定義の行動クラス
*/

#pragma once
#include "stdafx.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	///	コントローラのボタンのハンドラ
	//--------------------------------------------------------------------------------------
	template<typename T>
	struct InputHandler {
		void PushHandle(const shared_ptr<T>& Obj) {
			//コントローラの取得
			auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
			if (cntlVec[0].bConnected) {
				//Aボタン
				if (cntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
					Obj->OnPushA();
				}
				if (cntlVec[0].wButtons&XINPUT_GAMEPAD_A) {
					Obj->OnHoldA();
				}
				if (cntlVec[0].wReleasedButtons&XINPUT_GAMEPAD_A) {
					Obj->OnRelease();
				}
				////Bボタン
				//if (cntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B) {
				//	Obj->OnPushB();
				//}
				//Xボタン
				if (cntlVec[0].wPressedButtons & XINPUT_GAMEPAD_X) {
					Obj->OnPushX();
				}
				////Yボタン
				//if (cntlVec[0].wPressedButtons & XINPUT_GAMEPAD_Y) {
				//	Obj->OnPushY();
				//}
			}
		}
	};
}

//end basecross

