/*!
@file Player.cpp
@brief プレイヤーなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Player::Player(const shared_ptr<Stage>& StagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, Vec3 angle) :
		GameObject(StagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_Angle(angle),
		m_Velocity(0)
	{}

	Vec3 Player::GetMoveVector() {
		auto delta = App::GetApp()->GetElapsedTime();
		float increase = 0.05f;
		//コントローラの取得
		auto cntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (cntlVec[0].bConnected) {
			if (GetStateMachine()->GetCurrentState() == PlayerDefault::Instance()) {
				if (cntlVec[0].fThumbLX != 0 || cntlVec[0].fThumbLY != 0) {
					//方向
					Vec3 angle = Vec3(cntlVec[0].fThumbLX, cntlVec[0].fThumbLY, 0.0f).normalize()*increase;
					m_Angle += angle;
					if (abs(m_Angle.x) >= 1.0f) {
						m_Angle.x = m_Angle.normalize().x;
					}
					if (abs(m_Angle.y) >= 1.0f) {
						m_Angle.y = m_Angle.normalize().y;
					}
				}
				else {
					if (m_Angle.normalize() != Vec3(0.0f, 0.0f, 0.0f)) {
						m_Angle += m_Angle.normalize()*increase;
					}
				}
			}
			if ((GetStateMachine()->GetCurrentState() == PlayerAcceleration::Instance()) || (GetStateMachine()->GetCurrentState() == PlayerBoost::Instance())) {
				increase = 0.15f;
				if (cntlVec[0].fThumbLX != 0 || cntlVec[0].fThumbLY != 0) {
					//方向
					Vec3 angle = Vec3(cntlVec[0].fThumbLX, cntlVec[0].fThumbLY, 0.0f).normalize()*increase;
					m_Angle += angle;
					if (abs(m_Angle.x) >= 1.0f) {
						m_Angle.x = m_Angle.normalize().x;
					}
					if (abs(m_Angle.y) >= 1.0f) {
						m_Angle.y = m_Angle.normalize().y;
					}
				}
				else {
					if (m_Angle.normalize() != Vec3(0.0f, 0.0f, 0.0f)) {
						m_Angle += m_Angle.normalize()*increase;
					}
				}
				//if (cntlVec[0].fThumbLX != 0 || cntlVec[0].fThumbLY != 0) {
				//	//方向
				//	m_Angle = Vec3(cntlVec[0].fThumbLX, cntlVec[0].fThumbLY, 0.0f);
				//	//正規化する
				//	m_Angle.normalize();
				//}
			}
		}
		return m_Angle;
	}

	void Player::MovePlayer() {
		float elapsedTime = App::GetApp()->GetElapsedTime();

		auto pos = GetComponent<Transform>()->GetPosition();
		if (m_Spead >= 1.0f) {
			m_Spead = 1.0f;
		}
		pos += m_MaxSpead * m_Spead * GetMoveVector()*elapsedTime;
		GetComponent<Transform>()->SetPosition(pos);

		auto UtilPtr = GetBehavior<UtilBehavior>();
		Quat quat(Vec3(0.0f, 0.0f, -1.0f), atan2(m_Angle.x, m_Angle.y));
		//Quat quat(Vec3(cos(atan2(m_Angle.y, m_Angle.x)), sin(atan2(m_Angle.y, m_Angle.x)), 0.0f), 1.0f);
		GetComponent<Transform>()->SetQuaternion(quat);
	}

	//初期化
	void Player::OnCreate() {
		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		//CollisionSphere衝突判定を付ける
		auto ptrColl = AddComponent<CollisionSphere>();
		//ptrColl->AddExcludeCollisionTag(wstringKey::tagAbsorptionObject);

		//タグをつける
		AddTag(wstringKey::tagPlayer);

		//パラメーター
		m_Energy = 1.0f;
		m_MaxSpead = 7.5f;
		m_Spead = 0.25f;
		////重力をつける
		//auto ptrGra = AddComponent<Gravity>();


		////文字列をつける
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		//アニメチップ
		float tipWidth = 128.0f / 512.0f;
		float tipHeight = 128.0f / 512.0f;
		for (int c = 0; c < 4; c++) {
			for (int r = 0; r < 4; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 0;
		m_AnimTipCol = 0;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.75f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);
		PtrSpriteDraw->SetTextureResource(wstringKey::txTestPlayer);
		PtrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		//透明処理
		SetAlphaActive(true);
		SetDrawLayer(0);

		//SE
		m_AudioManager = App::GetApp()->GetXAudio2Manager();

		//カメラを得る
		auto ptrCamera = dynamic_pointer_cast<MyCamera>(OnGetDrawCamera());
		if (ptrCamera) {
			m_ptrMyCamera = ptrCamera;
			//MyCameraである
			//MyCameraに注目するオブジェクト（プレイヤー）の設定
			ptrCamera->SetTargetObject(GetThis<GameObject>());
			ptrCamera->SetTargetToAt(Vec3(0.0f, 0.0f, 0.0f));
		}
		//ステートマシンの構築
		m_StateMachine.reset(new StateMachine<Player>(GetThis<Player>()));
		//最初のステートを設定
		m_StateMachine->ChangeState(PlayerStart::Instance());

		//レーダー生成
		auto radarPtr = GetStage()->AddGameObject<Radar>(GetComponent<Transform>()->GetScale(), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f), GetThis<GameObject>());
		GetStage()->SetSharedGameObject(wstringKey::obRadar, radarPtr);
		m_RadarPtr = radarPtr->GetThis<GameObject>();
		//サーチエリア生成
		auto searchAreaPtr = GetStage()->AddGameObject<SearchArea>(GetComponent<Transform>()->GetScale() * 7.5f, Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f), GetThis<GameObject>());
		GetStage()->SetSharedGameObject(wstringKey::obSearchArea, searchAreaPtr);
		m_SearchAreaPtr = searchAreaPtr->GetThis<GameObject>();
		searchAreaPtr->SetDrawActive(false);
		//ゴールマーカー生成
		auto goalMarkerPtr = GetStage()->AddGameObject<GoalMarker>(GetComponent<Transform>()->GetScale() * 0.5f, Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f), GetThis<GameObject>());
		m_GoalMarkerPtr = goalMarkerPtr->GetThis<GameObject>();
	}

	//更新
	void Player::OnUpdate() {

		if (GetStateMachine()->GetCurrentState() != PlayerClear::Instance() /*|| GetStateMachine()->GetCurrentState() != PlayerDead::Instance()*/) {
			if (m_Energy >= 1.0f) {
				m_Energy = 1.0f;
			}
			if (m_Energy > 0) {
				if (GetStateMachine()->GetCurrentState() != PlayerAcceleration::Instance()) {
					m_Energy -= 0.01f * App::GetApp()->GetElapsedTime();
				}
				else {
					m_Energy -= 0.01f / 2.0f * App::GetApp()->GetElapsedTime();
				}
			}
			else
			{
				if (GetStateMachine()->GetCurrentState() != PlayerDead::Instance()) {
					m_Energy = 0.0f;
					m_StateMachine->ChangeState(PlayerDead::Instance());
				}
			}
			if (m_Energy > 0.0f) {
				//コントローラチェックして入力があればコマンド呼び出し
				m_InputHandler.PushHandle(GetThis<Player>());
				MovePlayer();
			}
		}
		//ステートマシン
		m_StateMachine->Update();

	}
	void Player::OnDestroy() {
		m_AudioManager->Stop(m_SE);
		m_SE = nullptr;
	}

	void Player::OnCollisionEnter(shared_ptr<GameObject>& Other) {
		if (GetStateMachine()->GetCurrentState() != PlayerDead::Instance()) {
			auto& object = Other;
			//重力圏との当たり判定
			if (object->FindTag(wstringKey::tagAbsorptionObject)) {
				//子オブジェクト参照テスト
				vector<shared_ptr<GameObject>> childVec;
				//この段階でchildVecに入る
				auto& objectVec = GetStage()->GetGameObjectVec();
				for (auto& v : objectVec) {
					auto ptrTrans = v->GetComponent<Transform>();
					auto ptrParent = ptrTrans->GetParent();
					if (object->GetThis<GameObject>() == ptrParent) {
						childVec.push_back(v);
					}
				}
				//TestStageObjectChildrenかどうか
				shared_ptr<TestStageObjectChildren> corePtr = nullptr;
				for (auto& childs : childVec) {
					if (childs->FindTag(wstringKey::tagAbsorptionCoreObject)) {
						corePtr = dynamic_pointer_cast<TestStageObjectChildren>(childs);
					}
				}

				auto radar = GetStage()->GetSharedGameObject<Radar>(wstringKey::obRadar);
				radar->GetStateMachine()->ChangeState(RadarExpansion::Instance());
				auto searchArea = GetStage()->GetSharedGameObject<SearchArea>(wstringKey::obSearchArea);
				searchArea->SetUpdateActive(true);
				searchArea->SetDrawActive(true);
				searchArea->m_SeFlg = true;
				auto radarGagePtr = GetStage()->GetSharedGameObject<RadarGageBar>(wstringKey::obRadarGage);
				auto pos = object->GetComponent<Transform>()->GetPosition() + Vec3(-radarGagePtr->GetComponent<Transform>()->GetScale().x / 2.0f, 0.0f, 0.0f);
				radarGagePtr->SetRadarGageActive(pos);
				radarGagePtr->SetCorePtr(corePtr);
			}
			//惑星との当たり判定
			if (object->FindTag(wstringKey::tagAbsorptionCoreObject)) {
				auto core = dynamic_pointer_cast<TestStageObjectChildren>(object);
				auto playerPos = GetComponent<Transform>()->GetPosition();
				if (!core->m_HitFlg)
				{
					core->m_HitFlg = true;
					Damage(0.1f);
				}
				auto pos = core->GetComponent<Transform>()->GetWorldPosition();
				auto movePos = pos - playerPos;
				auto scale = core->GetComponent<Transform>()->GetScale() / 2.0f + GetComponent<Transform>()->GetScale() / 2.0f;
				scale += Vec3(0.2f, 0.2f, 0.0f);
				scale = scale * movePos.normalize();
				scale.z = 0.0f;
				movePos = movePos - scale;
				GetComponent<Transform>()->SetPosition(playerPos + movePos);
			}
			//衛星との当たり判定
			if (object->FindTag(wstringKey::tagSatellite)) {
				auto satellite = dynamic_pointer_cast<Satellite>(object);
				auto playerPos = GetComponent<Transform>()->GetPosition();
				if (!satellite->m_HitFlg)
				{
					satellite->m_HitFlg = true;
					Damage(0.05f);
				}
				auto pos = satellite->GetComponent<Transform>()->GetWorldPosition();
				auto movePos = pos - playerPos;
				auto scale = satellite->GetComponent<Transform>()->GetScale() / 2.0f + GetComponent<Transform>()->GetScale() / 2.0f;
				scale += Vec3(1.0f, 1.0f, 0.0f);
				scale = scale * movePos.normalize();
				scale.z = 0.0f;
				movePos = movePos - scale;
				GetComponent<Transform>()->SetPosition(playerPos + movePos);
			}
			//障害物オブジェクトとの当たり判定
			if (object->FindTag(wstringKey::tagBreakableObject)) {
				Damage(0.05f);
				GetStage()->RemoveGameObject<GameObject>(object->GetThis<GameObject>());
			}
			//アイテムとの当たり判定
			if (object->FindTag(wstringKey::tagItem)) {
				Recovery(0.15f);
				GetStage()->RemoveGameObject<GameObject>(object->GetThis<GameObject>());
			}
		}
	}

	void Player::OnCollisionExcute(shared_ptr<GameObject>& Other) {
		if (GetStateMachine()->GetCurrentState() != PlayerDead::Instance()) {
			auto& object = Other;
			//重力圏に当たり続けてる時
			if (object->FindTag(wstringKey::tagAbsorptionObject)) {
				float spead = 0.5f;
				GetStateMachine()->ChangeState(PlayerAcceleration::Instance());
				auto deltaTime = App::GetApp()->GetElapsedTime();
				auto playerPos = GetComponent<Transform>()->GetPosition();
				Vec3 dir = object->GetComponent<Transform>()->GetPosition() - playerPos;

				Vec3 halfScale = object->GetComponent<Transform>()->GetScale() / 2.0f;
				halfScale = halfScale * dir.normalize();
				Vec3 magnificationScale = (halfScale - dir);
				magnificationScale = magnificationScale / halfScale;
				auto automagnification = (magnificationScale.x + magnificationScale.y) / 2.0f;

				m_Spead += spead * deltaTime*(0.2f + 0.8f*automagnification);
				playerPos += m_MaxSpead * (0.75f*automagnification)*dir.normalize() * m_Spead * deltaTime;
				GetComponent<Transform>()->SetPosition(playerPos);
			}

		}
	}

	void Player::OnCollisionExit(shared_ptr<GameObject>& Other) {
		if (GetStateMachine()->GetCurrentState() != PlayerDead::Instance()) {

			auto& object = Other;
			//惑星から抜けたら
			if (object->FindTag(wstringKey::tagAbsorptionCoreObject)) {
				auto core = dynamic_pointer_cast<TestStageObjectChildren>(object);
				core->m_IntervalFlg = true;
			}
			//衛星から抜けたら
			if (object->FindTag(wstringKey::tagSatellite)) {
				auto satellite = dynamic_pointer_cast<Satellite>(object);
				satellite->m_IntervalFlg = true;
			}
			//重力圏から抜けたら
			if (object->FindTag(wstringKey::tagAbsorptionObject)) {
				if (!m_OnHoldA) {
					GetStateMachine()->ChangeState(PlayerDefault::Instance());
				}
				else
				{
					GetStateMachine()->ChangeState(PlayerBoost::Instance());
				}
				auto radar = GetStage()->GetSharedGameObject<Radar>(wstringKey::obRadar);
				radar->GetStateMachine()->ChangeState(RadarDefault::Instance());
				auto searchArea = GetStage()->GetSharedGameObject<SearchArea>(wstringKey::obSearchArea);
				searchArea->m_AnimTipRow = 0;
				searchArea->m_SeFlg = false;
				searchArea->SetDrawActive(false);
				searchArea->SetUpdateActive(false);

				auto radarGagePtr = GetStage()->GetSharedGameObject<RadarGageBar>(wstringKey::obRadarGage);
				radarGagePtr->OnDestroy();

			}
		}
	}
	void Player::OnUpdate2() {
		Vec3 eye=m_ptrMyCamera->GetEye();
		if (m_Spead > 0.0f) {
			eye.z = -5.0f - 2.5f*m_Spead;
			m_ptrMyCamera->SetEye(eye);
		}
		//////文字列の表示
		////DrawStrings();
	}

	//Aボタン
	void Player::OnPushA() {
		if (GetStateMachine()->GetCurrentState() != PlayerAcceleration::Instance()) {
			m_StateMachine->ChangeState(PlayerBoost::Instance());
		}
	}
	void Player::OnHoldA() {
		m_OnHoldA = true;
		if (GetStateMachine()->GetCurrentState() != PlayerAcceleration::Instance()) {
			auto delta = App::GetApp()->GetElapsedTime();
			m_Energy -= 0.1f*delta;
			m_Spead += 0.25f*(1.0f - m_Spead)*delta;
		}
	}
	void Player::OnRelease() {
		m_OnHoldA = false;
		m_StateMachine->ChangeState(PlayerDefault::Instance());
	}
	//Xボタン
	void Player::OnPushX() {
		//if (m_Energy > 0) {
		//	auto XAPtr = App::GetApp()->GetXAudio2Manager();
		//	XAPtr->Start(wstringKey::seShot, 0, 0.5f);
		//	m_Energy -= 0.05f;
		//	GetStage()->AddGameObject<Bullet>(Vec3(0.2f, 0.2f, 0.2f), Vec3(0.0f, 0.0f, 0.0f), GetComponent<Transform>()->GetPosition(), m_Angle);
		//}
	}
	//ダメージ
	void Player::Damage(float damage) {
		m_Energy -= damage;
		// パーティクルエフェクト
		auto bombPtr = GetStage()->GetSharedGameObject<BombParticle>(L"BombParticle");
		if (bombPtr) {
			bombPtr->InsertBomb(GetComponent<Transform>()->GetPosition(), L"txEffBombPlayer");
		}
	}

	//回復
	void Player::Recovery(float recovery) {
		m_Energy += recovery;
		m_Spead += 0.5f;
		// パーティクルエフェクト
		auto recoveryPtr = GetStage()->GetSharedGameObject<RecoveryParticle>(L"RecoveryParticle");
		if (recoveryPtr) {
			recoveryPtr->InsertRecovery(GetComponent<Transform>()->GetPosition());
		}
	}

	////プレイヤーステート集
	//デフォルトステート
	IMPLEMENT_SINGLETON_INSTANCE(PlayerDefault)

	void PlayerDefault::Enter(const shared_ptr<Player>& obj) {
		obj->m_AudioManager->Stop(obj->m_SE);
		if (obj->m_SE == nullptr) {
			obj->m_SE = obj->m_AudioManager->Start(wstringKey::seBoostLoop, XAUDIO2_LOOP_INFINITE, 0.3f);

		}
	}
	void PlayerDefault::Execute(const shared_ptr<Player>& obj) {
		float elapsedTime = App::GetApp()->GetElapsedTime();
		if (obj->m_Spead > 0.1f) {
			obj->m_Spead -= 0.05f * elapsedTime;
		}
		else {
			obj->m_Spead = 0.1f;
		}
		obj->m_delta += elapsedTime;
		float HelfSize = 0.5f;
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		auto animTip = obj->m_AnimTip[obj->m_AnimTipCol][obj->m_AnimTipRow];
		vector<VertexPositionColorTexture> vertices = {
			{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
			{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(animTip.right, animTip.top) },
			{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
			{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(animTip.right, animTip.bottom) }
		};

		auto drawComp = obj->GetComponent<PCTStaticDraw>();
		drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);
		if (obj->m_delta >= 0.05f) {
			obj->m_delta = 0.0f;
			if (obj->m_AnimTipRow < 2) {
				obj->m_AnimTipRow++;
			}
			else {
				obj->m_AnimTipRow = 1;
			}
		}
	}
	void PlayerDefault::Exit(const shared_ptr<Player>& obj) {
		obj->m_AudioManager->Stop(obj->m_SE);
		obj->m_SE = nullptr;
	}

	//加速ステート
	IMPLEMENT_SINGLETON_INSTANCE(PlayerAcceleration)

	void PlayerAcceleration::Enter(const shared_ptr<Player>& obj) {
		obj->m_AudioManager->Stop(obj->m_SE);
		obj->m_AnimTipRow = 0;
		float elapsedTime = App::GetApp()->GetElapsedTime();
		obj->m_delta += elapsedTime;
		float HelfSize = 0.5f;
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		auto animTip = obj->m_AnimTip[obj->m_AnimTipCol][obj->m_AnimTipRow];
		vector<VertexPositionColorTexture> vertices = {
			{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
			{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(animTip.right, animTip.top) },
			{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
			{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(animTip.right, animTip.bottom) }
		};
		auto drawComp = obj->GetComponent<PCTStaticDraw>();
		drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);
	}
	void PlayerAcceleration::Execute(const shared_ptr<Player>& obj) {
	}
	void PlayerAcceleration::Exit(const shared_ptr<Player>& obj) {
	}

	//ブーストステート
	IMPLEMENT_SINGLETON_INSTANCE(PlayerBoost)

	void PlayerBoost::Enter(const shared_ptr<Player>& obj) {
		obj->m_AnimTipRow = 2;
		obj->m_AudioManager->Stop(obj->m_SE);
		if (obj->m_SE == nullptr) {
			obj->m_SE = obj->m_AudioManager->Start(wstringKey::seBoostLoop, XAUDIO2_LOOP_INFINITE, 0.5f);
		}
	}
	void PlayerBoost::Execute(const shared_ptr<Player>& obj) {
		//obj->m_Spead += 0.1f*(1.05f - obj->m_Spead)*App::GetApp()->GetElapsedTime();
		//obj->GetStateMachine()->ChangeState(PlayerDefault::Instance());
		float elapsedTime = App::GetApp()->GetElapsedTime();
		obj->m_delta += elapsedTime;
		float HelfSize = 0.5f;
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		auto animTip = obj->m_AnimTip[obj->m_AnimTipCol][obj->m_AnimTipRow];
		vector<VertexPositionColorTexture> vertices = {
			{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
			{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(animTip.right, animTip.top) },
			{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
			{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(animTip.right, animTip.bottom) }
		};
		auto drawComp = obj->GetComponent<PCTStaticDraw>();
		drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);
		if (obj->m_delta >= 0.05f) {
			obj->m_delta = 0.0f;
			if (obj->m_AnimTipRow < 3) {
				obj->m_AnimTipRow++;
			}
			else {
				obj->m_AnimTipRow = 2;
			}
		}
	}
	void PlayerBoost::Exit(const shared_ptr<Player>& obj) {
		obj->m_AudioManager->Stop(obj->m_SE);
		obj->m_SE = nullptr;
	}

	//スタートステート
	IMPLEMENT_SINGLETON_INSTANCE(PlayerStart)

	void PlayerStart::Enter(const shared_ptr<Player>& obj) {
		obj->m_AudioManager->Stop(obj->m_SE);
		obj->m_AnimTipRow = 0;
		float HelfSize = 0.5f;
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		auto animTip = obj->m_AnimTip[obj->m_AnimTipCol][obj->m_AnimTipRow];
		vector<VertexPositionColorTexture> vertices = {
			{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
			{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(animTip.right, animTip.top) },
			{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
			{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(animTip.right, animTip.bottom) }
		};
		auto drawComp = obj->GetComponent<PCTStaticDraw>();
		drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);
	}
	void PlayerStart::Execute(const shared_ptr<Player>& obj) {

	}
	void PlayerStart::Exit(const shared_ptr<Player>& obj) {
	}


	//クリアステート
	IMPLEMENT_SINGLETON_INSTANCE(PlayerClear)

	void PlayerClear::Enter(const shared_ptr<Player>& obj) {
		obj->m_AnimTipRow = 0;
		float HelfSize = 0.5f;
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		auto animTip = obj->m_AnimTip[obj->m_AnimTipCol][obj->m_AnimTipRow];
		vector<VertexPositionColorTexture> vertices = {
			{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
			{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(animTip.right, animTip.top) },
			{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
			{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(animTip.right, animTip.bottom) }
		};
		auto drawComp = obj->GetComponent<PCTStaticDraw>();
		drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);

		m_GoalPtr = obj->GetStage()->GetSharedGameObject<GameObject>(wstringKey::obGoal);
		m_GoalPos = m_GoalPtr->GetComponent<Transform>()->GetPosition();
		auto playerPos = obj->GetComponent<Transform>()->GetPosition();
		m_Deg = atan2(playerPos.y - m_GoalPos.y, playerPos.x - m_GoalPos.x);
		m_Rad = sqrtf((m_GoalPos.x - playerPos.x) * (m_GoalPos.x - playerPos.x) + (m_GoalPos.y - playerPos.y) * (m_GoalPos.y - playerPos.y));
		m_RadMax = m_Rad;
	}
	void PlayerClear::Execute(const shared_ptr<Player>& obj) {
		if (!obj->GetGoalHitFlg()) {
			auto playerTrans = obj->GetComponent<Transform>();
			auto playerPos = playerTrans->GetPosition();
			float elapsedTime = App::GetApp()->GetElapsedTime();
			//向きを計算
			Vec3 front = m_GoalPos - playerPos;
			//向きからの角度を算出
			float FrontAngle = atan2(front.y, front.x);
			playerTrans->SetQuaternion(Quat(0.0f, 0.0f, -1.0f, FrontAngle));
			//周回
			m_Rad -= 1.0f*elapsedTime;
			if (m_Rad <= 0.0f) {
				m_Rad = 0.0f;
			}
			auto movePos = Vec3(cos(m_Deg) * m_Rad + m_GoalPos.x, sin(m_Deg) * m_Rad + m_GoalPos.y, 0.0f);
			obj->GetComponent<Transform>()->SetPosition(movePos);
			m_Deg *= 180.0f / XM_PI;
			m_Deg += 180.0f*elapsedTime/**(m_Rad / m_RadMax)*/;
			if (m_Deg > 360.0f) {
				m_Deg = 0.0f;
			}
			m_Deg *= XM_PI / 180.0f;
		}
	}
	void PlayerClear::Exit(const shared_ptr<Player>& obj) {
		
	}

	//デッドステート
	IMPLEMENT_SINGLETON_INSTANCE(PlayerDead)

	void PlayerDead::Enter(const shared_ptr<Player>& obj) {
		obj->m_MaxSpead = 0.0f;
		auto ptrColl = obj->GetComponent<CollisionSphere>();
		ptrColl->SetSleepActive(true);

		obj->m_AudioManager->Stop(obj->m_SE);
		obj->m_AnimTipRow = 0;
		float elapsedTime = App::GetApp()->GetElapsedTime();
		obj->m_delta += elapsedTime;
		float HelfSize = 0.5f;
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		auto animTip = obj->m_AnimTip[obj->m_AnimTipCol][obj->m_AnimTipRow];
		vector<VertexPositionColorTexture> vertices = {
			{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
			{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(animTip.right, animTip.top) },
			{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
			{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(animTip.right, animTip.bottom) }
		};
		auto drawComp = obj->GetComponent<PCTStaticDraw>();
		drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);

		auto objectPtr=obj->GetStage()->AddGameObject<CutIn>(wstringKey::txGameOverText,true,Vec2(768.0f, 192.0f), Vec3(1000.0f, 0.0f,0.0f));
		objectPtr->SetDrawLayer(10);
		m_CutInPtr = objectPtr->GetThis<GameObject>();
		auto radarPtr = dynamic_pointer_cast<Radar>(obj->GetRadar());
		auto searchAreaPtr = dynamic_pointer_cast<SearchArea>(obj->GetSearchArea());
		searchAreaPtr->m_SeFlg = false;
		searchAreaPtr->SetDrawActive(false);
		//searchAreaPtr->SetUpdateActive(false);
		auto goalMarkerPtr = dynamic_pointer_cast<GoalMarker>(obj->GetGoalMarker());
		goalMarkerPtr->SetDrawActive(false);
	}
	void PlayerDead::Execute(const shared_ptr<Player>& obj) {
		float elapsedTime = App::GetApp()->GetElapsedTime();
		obj->m_delta += elapsedTime;
		auto cutInPtr = dynamic_pointer_cast<CutIn>(m_CutInPtr);

		if (cutInPtr->m_CutInFlg) {
			// エネルギーが1〜0の間に弾撃つと落ちますが、デバッグ文字全て切っていれば解決する問題です
			shared_ptr<FadeOut> fadeout = nullptr;
			bool fadeoutFlg = false;
			auto objs = obj->GetStage()->GetGameObjectVec();
			for (auto& object : objs) {
				fadeout = dynamic_pointer_cast<FadeOut>(object);
				if (fadeout)
				{
					fadeoutFlg = true;
					break;
				}
			}
			if (!fadeoutFlg) {
				obj->GetStage()->AddGameObject<FadeOut>(L"txFade", true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
			}
			if (fadeout) {
				if (fadeout->GetAlpha() >= 1.0f) {
					obj->PostEvent(0.0f, obj->GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToGameOverStage");
				}
			}
		}
		//PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToGameOverStage");

	}
	void PlayerDead::Exit(const shared_ptr<Player>& obj) {

	}

	//文字列の表示
	void Player::DrawStrings() {
		//文字列表示
		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring fpsStr(L"FPS: ");
		fpsStr += Util::UintToWStr(fps);
		fpsStr += L"\nElapsedTime: ";
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		fpsStr += Util::FloatToWStr(ElapsedTime);
		fpsStr += L"\n";

		auto pos = GetComponent<Transform>()->GetPosition();
		wstring positionStr(L"Position:\t");
		positionStr += L"X=" + Util::FloatToWStr(pos.x, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Y=" + Util::FloatToWStr(pos.y, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Z=" + Util::FloatToWStr(pos.z, 6, Util::FloatModify::Fixed) + L"\n";


		wstring speadStr(L"Spead:\t");
		//auto gravVelocity = GetComponent<Gravity>()->GetGravityVelocity();
		speadStr +=  Util::FloatToWStr(m_Spead*100.0f, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Y=" + Util::FloatToWStr(gravVelocity.y, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Z=" + Util::FloatToWStr(gravVelocity.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring energyStr(L"Energy:\t");
		energyStr += Util::FloatToWStr(m_Energy*100.0f, 6, Util::FloatModify::Fixed) + L",\t";

		auto v = GetStage()->GetGameObjectVec();
		auto size = v.size();
		wstring objectVecStr(L"Size:\t");
		objectVecStr += Util::FloatToWStr((float)size, 6, Util::FloatModify::Fixed) + L",\t";


		wstring str = fpsStr + positionStr + speadStr + energyStr + objectVecStr;
		//文字列コンポーネントの取得
		auto ptrString = GetComponent<StringSprite>();
		ptrString->SetText(str);
	}


}
//end basecross

