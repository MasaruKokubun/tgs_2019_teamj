/*!
@file Character.cpp
@brief キャラクターなど実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{

	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//--------------------------------------------------------------------------------------
	//構築と破棄
	FixedBox::FixedBox(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Rotation,
		const Vec3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	FixedBox::~FixedBox() {}

	//初期化
	void FixedBox::OnCreate() {
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale);
		ptrTransform->SetRotation(m_Rotation);
		ptrTransform->SetPosition(m_Position);
		//OBB衝突j判定を付ける
		auto ptrColl = AddComponent<CollisionObb>();
		ptrColl->SetFixed(true);
		//タグをつける
		AddTag(L"FixedBox");
		//影をつける（シャドウマップを描画する）
		auto shadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		shadowPtr->SetMeshResource(L"DEFAULT_CUBE");
		auto ptrDraw = AddComponent<BcPNTStaticDraw>();
		ptrDraw->SetMeshResource(L"DEFAULT_CUBE");
		ptrDraw->SetTextureResource(L"SKY_TX");
		ptrDraw->SetFogEnabled(true);
		ptrDraw->SetOwnShadowActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class TestBox : public GameObject;
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TestBox::TestBox(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Rotation,
		const Vec3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	TestBox::~TestBox() {}

	//初期化
	void TestBox::OnCreate() {
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale);
		ptrTransform->SetRotation(m_Rotation);
		ptrTransform->SetPosition(m_Position);
		//OBB衝突j判定を付ける
		auto ptrColl = AddComponent<CollisionObb>();
		ptrColl->SetFixed(true);
		//タグをつける
		AddTag(L"FixedBox");
		//影をつける（シャドウマップを描画する）
		auto shadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		shadowPtr->SetMeshResource(L"DEFAULT_CUBE");
		auto ptrDraw = AddComponent<BcPNTStaticDraw>();
		ptrDraw->SetMeshResource(L"DEFAULT_CUBE");
		ptrDraw->SetTextureResource(wstringKey::txWall);
		ptrDraw->SetFogEnabled(true);
		ptrDraw->SetOwnShadowActive(true);
		////文字列をつける
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
	}
	//更新
	void TestBox::OnUpdate() {
		m_Rotation += Vec3(0 * (XM_PI / 180), 5.0f * (XM_PI / 180), 0 * (XM_PI / 180))*App::GetApp()->GetElapsedTime();
		if (m_Rotation.y >= 360.0f*(XM_PI / 180)) {
			m_Rotation.y = 0.0f;
		}
		GetComponent<Transform>()->SetRotation(m_Rotation);

	}
	void TestBox::OnUpdate2() {
		//文字列の表示
		DrawStrings();
	}
	//文字列の表示
	void TestBox::DrawStrings() {

		//文字列表示
		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring fpsStr(L"FPS: ");
		fpsStr += Util::UintToWStr(fps);
		fpsStr += L"\nElapsedTime: ";
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		fpsStr += Util::FloatToWStr(ElapsedTime);
		fpsStr += L"\n";

		auto rot = GetComponent<Transform>()->GetQuaternion();
		wstring rotationStr(L"Rotation:\t");
		rotationStr += L"X=" + Util::FloatToWStr(rot.x, 6, Util::FloatModify::Fixed) + L",\t";
		rotationStr += L"Y=" + Util::FloatToWStr(asin(rot.y)*(180 / XM_PI) * 2, 6, Util::FloatModify::Fixed) + L",\t";
		rotationStr += L"Z=" + Util::FloatToWStr(rot.z, 6, Util::FloatModify::Fixed) + L"\n";

		//wstring gravStr(L"GravityVelocoty:\t");
		//auto gravVelocity = GetComponent<Gravity>()->GetGravityVelocity();
		//gravStr += L"X=" + Util::FloatToWStr(gravVelocity.x, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Y=" + Util::FloatToWStr(gravVelocity.y, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Z=" + Util::FloatToWStr(gravVelocity.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring str = fpsStr + rotationStr;
		////文字列コンポーネントの取得
		//auto ptrString = GetComponent<StringSprite>();
		//ptrString->SetText(str);
	}
	//--------------------------------------------------------------------------------------
	//	class SpinBox : public GameObject;
	//--------------------------------------------------------------------------------------
	//構築と破棄
	SpinBox::SpinBox(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Rotation,
		const Vec3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	SpinBox::~SpinBox() {}

	//初期化
	void SpinBox::OnCreate() {
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale);
		ptrTransform->SetRotation(m_Rotation);
		ptrTransform->SetPosition(m_Position);
		//OBB衝突j判定を付ける
		auto ptrColl = AddComponent<CollisionObb>();
		ptrColl->SetFixed(true);
		//タグをつける
		AddTag(L"FixedBox");
		//影をつける（シャドウマップを描画する）
		auto shadowPtr = AddComponent<Shadowmap>();
		//影の形（メッシュ）を設定
		shadowPtr->SetMeshResource(L"DEFAULT_CUBE");
		auto ptrDraw = AddComponent<BcPNTStaticDraw>();
		ptrDraw->SetMeshResource(L"DEFAULT_CUBE");
		ptrDraw->SetTextureResource(L"SKY_TX");
		ptrDraw->SetFogEnabled(true);
		ptrDraw->SetOwnShadowActive(true);
	}

	// 操作
	void SpinBox::OnUpdate() {
		float elapsedTime = App::GetApp()->GetElapsedTime();
		Quat span(Vec3(0, 0, -1.0f), 0.01f);
		auto qt = GetComponent<Transform>()->GetQuaternion();
		qt *= span;
		GetComponent<Transform>()->SetQuaternion(qt);
	}

	//--------------------------------------------------------------------------------------
	//	class FrameBox : public GameObject;
	//--------------------------------------------------------------------------------------
	//構築と破棄
	FrameBox::FrameBox(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Rotation,
		const Vec3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_TotalTime(0.5f),
		m_CollisionFlg(false),
		m_Alpha(0.0f)
	{
	}
	FrameBox::~FrameBox(){}

	// 初期化
	void FrameBox::OnCreate() {
		m_CollisionFlg = false;
		m_PlayerFlg = Pos::None;

		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale);
		ptrTransform->SetRotation(m_Rotation);
		ptrTransform->SetPosition(m_Position);

		//初期カラー
		m_Color = Col4(1.0f, 0.7f, 0.7f, m_Alpha);

		//OBB衝突j判定を付ける
		auto ptrColl = AddComponent<CollisionObb>();
		ptrColl->SetAfterCollision(AfterCollision::None);
		//タグをつける
		AddTag(L"FrameBox");

		//描画するテクスチャを設定
		//Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), m_Color, Vec2(0.0f, 0.0f)},
			{Vec3(+HelfSize, +HelfSize, 0), m_Color, Vec2(1.0f, 0.0f)},
			{Vec3(-HelfSize, -HelfSize, 0), m_Color, Vec2(0.0f, 1.0f)},
			{Vec3(+HelfSize, -HelfSize, 0), m_Color, Vec2(1.0f, 1.0f)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};

		//頂点とインデックスを指定してスプライト作成
		auto ptrDraw = this->AddComponent<PCTStaticDraw>();
		ptrDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		ptrDraw->SetOriginalMeshUse(true);
		ptrDraw->SetDiffuse(m_Color);

		SetAlphaActive(true);
	}

	// 更新
	void FrameBox::OnUpdate() {
		float elapsedTime = App::GetApp()->GetElapsedTime();
		if (m_CollisionFlg)
		{
			m_TotalTime += elapsedTime;
			m_Alpha = 0.5f;
			if (m_TotalTime >= 2.0f)
			{
				m_TotalTime = 0.0f;
				m_CollisionFlg = false;
			}
		}
		else
		{
			m_Alpha -= elapsedTime;
			if (m_Alpha <= 0.0f)
			{
				m_Alpha = 0.0f;
			}
		}

		if (m_PlayerFlg)
		{
			shared_ptr<Player> player = nullptr;
			for (auto obj : GetStage()->GetGameObjectVec()) {
				player = dynamic_pointer_cast<Player>(obj);
				if (player)
				{
					break;
				}
			}

			auto playerPos = player->GetComponent<Transform>()->GetPosition();
			auto playerhalfScale = player->GetComponent<Transform>()->GetScale() * 0.5f;
			auto halfScale = m_Scale * 0.5f;

			auto XDistanceRight = m_Position.x + halfScale.x + playerhalfScale.x;
			auto XDistanceLeft = m_Position.x - halfScale.x - playerhalfScale.x;
			auto YDistanceBottom = m_Position.y + halfScale.y + playerhalfScale.y;
			auto YDistanceTop = m_Position.y - halfScale.y - playerhalfScale.y;

			switch (m_PlayerFlg)
			{
			case Pos::Right:
				playerPos.x = XDistanceRight;
				break;
			case Pos::Left:
				playerPos.x = XDistanceLeft;
				break;
			case Pos::Bottom:
				playerPos.y = YDistanceBottom;
				break;
			case Pos::Top:
				playerPos.y = YDistanceTop;
				break;
			default:
				break;
			}

			player->GetComponent<Transform>()->SetPosition(playerPos);

		}

		m_Color.w = m_Alpha;

		auto ptrDraw = GetComponent<PCTStaticDraw>();
		ptrDraw->SetDiffuse(m_Color);
	}

	// 衝突判定
	// 当たった瞬間
	void FrameBox::OnCollisionEnter(shared_ptr<GameObject>& other) {
		if (other->FindTag(wstringKey::tagPlayer) || other->FindTag(wstringKey::tagEnemyBullet)) {
			for (auto obj : GetStage()->GetGameObjectVec())
			{
				auto framebox = dynamic_pointer_cast<FrameBox>(obj);
				if (framebox)
				{
					framebox->m_CollisionFlg = true;
				}
			}
		}
		// 衝突したのがプレイヤーなら
		if (other->FindTag(wstringKey::tagPlayer))
		{
			auto player = dynamic_pointer_cast<Player>(other);
			auto playerPos = player->GetComponent<Transform>()->GetPosition();
			auto playerhalfScale = player->GetComponent<Transform>()->GetScale() * 0.5f;
			auto halfScale = m_Scale * 0.5f;
			// スケール分の距離
			auto half = halfScale + playerhalfScale;

			auto distance = m_Position - playerPos;

			// それぞれの距離
			auto XDistanceRight = m_Position.x + halfScale.x + playerhalfScale.x;	// 右
			auto XDistanceLeft = m_Position.x - halfScale.x - playerhalfScale.x;	// 左
			auto YDistanceBottom = m_Position.y + halfScale.y + playerhalfScale.y;	// 下
			auto YDistanceTop = m_Position.y - halfScale.y - playerhalfScale.y;		// 上

			// 縦のフレームの衝突
			if (m_Scale.x < m_Scale.y) {
				if (XDistanceRight > playerPos.x && (distance.x < half.x && distance.x < 0)) {
					// 右側の衝突
					m_PlayerFlg = Pos::Right;
				}
				else if (XDistanceLeft < playerPos.x && (distance.x < half.x && distance.x > 0)) {
					// 左側の衝突
					m_PlayerFlg = Pos::Left;
				}
			}
			// 横のフレームの衝突
			if (m_Scale.y < m_Scale.x) {
				if (YDistanceBottom > playerPos.y && (distance.y < half.y && distance.y < 0)) {
					// 下側の衝突
					m_PlayerFlg = Pos::Bottom;
				}
				else if (YDistanceTop < playerPos.y && (distance.y < half.y && distance.y > 0)) {
					// 上側の衝突
					m_PlayerFlg = Pos::Top;
				}
			}
		}
	}

	// 衝突している間
	void FrameBox::OnCollisionExcute(shared_ptr<GameObject>& other)
	{
		// 衝突しているのがプレイヤーなら
		if (other->FindTag(wstringKey::tagPlayer))
		{
			for (auto obj : GetStage()->GetGameObjectVec())
			{
				auto framebox = dynamic_pointer_cast<FrameBox>(obj);
				if (framebox)
				{
					framebox->m_CollisionFlg = true;
				}
			}
		}
	}

	// 出る瞬間
	void FrameBox::OnCollisionExit(shared_ptr<GameObject>& other) {
		// プレイヤーとの衝突を終わる
		if (other->FindTag(wstringKey::tagPlayer)) {
			m_PlayerFlg = Pos::None;
		}
	}

	//--------------------------------------------------------------------------------------
	//	Tutorialアニメーション
	//--------------------------------------------------------------------------------------
	TutorialAnimSprite::TutorialAnimSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vec2& StartScale, const Vec2& StartPos, uint32_t PieceXCount, uint32_t PieceYCount, float AnimeTime) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_TotalTime(0.0f),
		m_PieceXCount(PieceXCount),
		m_PieceYCount(PieceYCount),
		m_PieceIndex(0),
		m_AnimeTime(AnimeTime)

	{}
	TutorialAnimSprite::~TutorialAnimSprite() {}

	void TutorialAnimSprite::OnCreate() {
		float HelfSize = 0.5f;
		//頂点配列(縦横5個ずつ表示)
		m_BackupVertices = {
			{ VertexPositionColorTexture(Vec3(-HelfSize, HelfSize, 0),Col4(1.0f,1.0f,1.0f,1.0f), Vec2(0.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vec3(HelfSize, HelfSize, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(1.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vec3(-HelfSize, -HelfSize, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(0.0f, 1.0f)) },
			{ VertexPositionColorTexture(Vec3(HelfSize, -HelfSize, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(1.0f, 1.0f)) },
		};
		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);
		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(m_TextureKey);
	}
		void TutorialAnimSprite::OnUpdate() {
			float ElapsedTime = App::GetApp()->GetElapsedTime();
			m_TotalTime += ElapsedTime;
			if (m_TotalTime >= m_AnimeTime) {
				m_PieceIndex++;
				if (m_PieceIndex >= m_PieceXCount * m_PieceYCount) {
					m_PieceIndex = 0;
				}
				m_TotalTime = 0;
			}
			vector<VertexPositionColorTexture> newVertices;
			uint32_t pieceX = m_PieceIndex % m_PieceXCount;
			uint32_t pieceY = m_PieceIndex / m_PieceXCount;
			float pieceWidth = 1.0f / (float)m_PieceXCount;
			float pieceHeight = 1.0f / (float)m_PieceYCount;

			float pieceStartX = (float)pieceX * pieceWidth;
			float pieceEndX = pieceStartX + pieceWidth;

			float pieceStartY = (float)pieceY * pieceHeight;
			float pieceEndY = pieceStartY + pieceHeight;

			for (size_t i = 0; i < m_BackupVertices.size(); i++) {
				Vec2 uv = m_BackupVertices[i].textureCoordinate;
				switch (i) {
				case 0:
					uv.x = pieceStartX;
					uv.y = pieceStartY;
					break;
				case 1:
					uv.x = pieceEndX;
					uv.y = pieceStartY;
					break;
				case 2:
					uv.x = pieceStartX;
					uv.y = pieceEndY;
					break;
				case 3:
					uv.x = pieceEndX;
					uv.y = pieceEndY;
					break;
				}
				auto v = VertexPositionColorTexture(
					m_BackupVertices[i].position,
					m_BackupVertices[i].color,
					uv
				);
				newVertices.push_back(v);
			}
			auto ptrDraw = GetComponent<PCTSpriteDraw>();
			ptrDraw->UpdateVertices(newVertices);
		}
	

}
//end basecross
