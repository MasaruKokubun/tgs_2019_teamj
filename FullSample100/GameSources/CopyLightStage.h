/*!
@file GameStage.h
@brief コピーライトステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	コピーライトステージクラス
	//--------------------------------------------------------------------------------------
	class CopyLightStage : public Stage {
		float m_Time;
		float m_Alpha = 1.0f;
		float m_Count = 0.0f;
		// ビューの作成
		void CreateViewLight();
		// スプライトの作成
		void CreateCopyLightSprite();

		// 入力ハンドラー
		//InputHandler<ResultStage> m_InputHandler;

	public:
		// 構築と破棄
		CopyLightStage() : Stage() {}
		virtual ~CopyLightStage() {}
		//初期化
		virtual void OnCreate() override;
		// 更新
		virtual void OnUpdate() override;
	};
}