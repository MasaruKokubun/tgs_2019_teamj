#pragma once
#include "stdafx.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	///	タイトルのスプライト
	//--------------------------------------------------------------------------------------
	class Sprite : public GameObject {
	protected:
		bool m_Trace;
		Vec2 m_StartScale;
		Vec2 m_StartPos;
		wstring m_TextureKey;

		// アニメーション用
		int m_Row;
		int m_Col;
		// 画像サイズ
		Vec2 m_TipSize;
		// 画像の分割数
		Vec2 m_DivisionCount;
	public:
		Sprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
			const Vec2& StartScale, const Vec2& StartPos);
		Sprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
			const Vec2& StartScale, const Vec2& StartPos,
			const int& Row, const int& Col,
			const Vec2& TipSize,
			const Vec2& DivisionCount);
		virtual ~Sprite();
		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};

	//--------------------------------------------------------------------------------------
	///	スコア用のスプライト
	//--------------------------------------------------------------------------------------
	class NumSprite : public GameObject {
		bool m_Trace;
		Vec2 m_StartScale;
		Vec2 m_StartPos;
		wstring m_TextureKey;

		vector<VertexPositionColorTexture> m_Vertices;
	public:
		NumSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
			const Vec2& StartScale, const Vec2& StartPos);
		virtual ~NumSprite();
		virtual void OnCreate()override;

		void SetRect(const Rect2D<float>& rect) {
			auto w = App::GetApp()->GetGameWidth() * 0.5f;
			auto h = 128.0f;
			m_Vertices[0].textureCoordinate = Vec2(rect.left / w, rect.top / h);
			m_Vertices[1].textureCoordinate = Vec2(rect.right / w, rect.top / h);
			m_Vertices[2].textureCoordinate = Vec2(rect.left / w, rect.bottom / h);
			m_Vertices[3].textureCoordinate = Vec2(rect.right / w, rect.bottom / h);

			auto drawComp = GetComponent<PCTSpriteDraw>();
			drawComp->UpdateVertices(m_Vertices);
		}
	};

	class AbcSprite : public GameObject {
		bool m_Trace;
		Vec2 m_StartScale;
		Vec2 m_StartPos;
		wstring m_TextureKey;

		vector<VertexPositionColorTexture> m_Vertices;
		enum EAlphabet{
			A = 0,B,C,D,E,F,G,H,I,
			J = 0,K,L,M,N,O,P,Q,R,
			S = 0,T,U,V,W,X,Y,Z,SLASH,
			ABCMAX = 27
		};

		int m_AbcU;
		int m_AbcV;

	public:
		AbcSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
			const Vec2& StartScale, const Vec2& StartPos);
		virtual ~AbcSprite();
		virtual void OnCreate()override;

		void SetRect(const Rect2D<float>& rect) {
			auto w = App::GetApp()->GetGameWidth() * 0.5f;
			auto h = 128.0f;
			m_Vertices[0].textureCoordinate = Vec2(rect.left / w, rect.top / h);
			m_Vertices[1].textureCoordinate = Vec2(rect.right / w, rect.top / h);
			m_Vertices[2].textureCoordinate = Vec2(rect.left / w, rect.bottom / h);
			m_Vertices[3].textureCoordinate = Vec2(rect.right / w, rect.bottom / h);

			auto drawComp = GetComponent<PCTSpriteDraw>();
			drawComp->UpdateVertices(m_Vertices);
		}
	};

	// 演出スプライト
	class EffectSprite : public GameObject {
		bool m_Trace;
		Vec2 m_StartScale;
		Vec2 m_StartPos;
		wstring m_TextureKey;

		float m_TotalTime;

		Rect2D<float> m_AnimTip[4][1];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
		vector<VertexPositionColorTexture> m_BackupVertices;
	public:
		EffectSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, const bool Trace,
			const Vec2& StartScale, const Vec2& StartPos);
		virtual ~EffectSprite();
		virtual void OnCreate() override;
		virtual void OnUpdate() override;

	};
	const enum class Level {
		none,	// どのレベルでもないスプライト
		a1,
		a2,
		a3,
		b1,
		b2,
		c1,
		c2,
		c3,
		c4,
		d1,
		d2,
		e1,
		e2,
		f1,
		f2,
		g1,
		g2
	};
//--------------------------------------------------------------------------------------
//	セレクトスプライト
//--------------------------------------------------------------------------------------
	class SelectSprite : public GameObject {
	private:
		wstring m_TextureKey;	// 貼り付けるテクスチャ
		Vec2 m_StartPos;	// 位置
		Vec2 m_StartScale;	// 大きさ
		Level m_Level;	// このスプライトがどのタイプか

		//バックアップ頂点データ
		vector<VertexPositionColorTexture> m_BackupVertices;
	public:
		// テクスチャ　位置　大きさ	レベル
		SelectSprite(const shared_ptr<Stage>& StagePtr,
			const wstring& TextureKey,
			bool Trace,
			const Vec2& StartPos,
			const Vec2& StartScale,
			const Level& level);
		SelectSprite(const shared_ptr<Stage>& StagePtr,
			const wstring& TextureKey,
			bool Trace,
			const Vec2& StartPos,
			const Vec2& StartScale);
		virtual ~SelectSprite();
		//初期化
		virtual void OnCreate()override;
		// 更新
		virtual void OnUpdate()override;
		// カラーの変更	(Brightness = 明度)
		void UpdateColor(Col4 Brightness);
		// 大きさの変更
		void UpdateScale(const Vec2& Scale);
		// レベルの取得
		Level GetLevel() const { return m_Level; }
	};

	//--------------------------------------------------------------------------------------
	//	スタートスプライト
	//--------------------------------------------------------------------------------------
	class StartSprite : public GameObject
	{
		wstring m_TextureKey;
		bool m_Trace;
		Vec2 m_StartScale;
		Vec2 m_StartPos;

		// トータル時間
		float m_TotalTime;

		// スタートフラグ
		bool m_StartFlg;
		// 描画されてるかのフラグ
		bool m_DrawFlg;
	public:
		// 構築と破棄
		StartSprite(const shared_ptr<Stage>& StagePtr, 
			const wstring& TextureKey, 
			const bool& Trace, 
			const Vec2& StartScale,	
			const Vec2& StartPos);
		virtual ~StartSprite();

		// 初期化
		virtual void OnCreate() override;
		// 更新
		virtual void OnUpdate() override;

		// セッター　ゲッター
		bool GetStartFlg() const;
		void SetDrawFlg(bool DrawFlg);

	};

	//--------------------------------------------------------------------------------------
	// 惑星UI用スプライト
	//--------------------------------------------------------------------------------------
	class PlanetUISprite : public GameObject
	{
		wstring m_TextureKey;
		bool m_Trace;
		Vec2 m_StartScale;
		Vec2 m_StartPos;
		// (アニメーション用の)コマ割りデータ
		Rect2D<float> m_AnimTip[6][6];
		int m_AnimTipCol, m_AnimTipRow;
	public:
		// 構築と破棄
		PlanetUISprite(const shared_ptr<Stage>& StagePtr,
			const wstring& TextureKey,
			const bool& Trace,
			const Vec2& StartScale,
			const Vec2& StartPos,
			const int& Row = 1,
			const int& Col = 0);
		~PlanetUISprite();

		// 初期化
		virtual void OnCreate() override;

	};

	//-------------------------------------------------------------------------------------
	//　チュートリアルテキスト
	//-------------------------------------------------------------------------------------
	class TutorialText : public GameObject {
		bool m_Trace;
		Vec2 m_StartScale;
		Vec2 m_StartPos;
		wstring m_TextureKey;
	public:
		TutorialText(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
			const Vec2& StartScale, const Vec2& StartPos);
		virtual ~TutorialText();
		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};
}