#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	///	エネルギーゲージバー
	//--------------------------------------------------------------------------------------
	class EnergyGageBar : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		shared_ptr<Player> m_PlayerPtr;
	public:
		bool m_DestroyFlg = false;
		float m_delta;
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[1][1];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
		//構築と破棄
		//--------------------------------------------------------------------------------------
		/*!
		@brief	コンストラクタ
		@param[in]	StagePtr	ステージ
		*/
		//--------------------------------------------------------------------------------------
		EnergyGageBar(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<Player> playerPtr = nullptr);
		//--------------------------------------------------------------------------------------
		/*!
		@brief	デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~EnergyGageBar() {}

		//
		//void SetPlayerPtr(shared_ptr<Player> playerPtr) {
		//	m_PlayerPtr = playerPtr;
		//}
		//アクセサ
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
		virtual void OnDestroy()override;
	};

	//--------------------------------------------------------------------------------------
	///	エネルギーゲージフレーム
	//--------------------------------------------------------------------------------------
	class EnergyGageFrame : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		shared_ptr<GameObject> m_ParentPtr;
		//文字列の表示
		//void DrawStrings();
	public:
		float m_delta;
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[1][1];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
		//構築と破棄
		//--------------------------------------------------------------------------------------
		/*!
		@brief	コンストラクタ
		@param[in]	StagePtr	ステージ
		*/
		//--------------------------------------------------------------------------------------
		EnergyGageFrame(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<GameObject> parentPtr = nullptr);
		//--------------------------------------------------------------------------------------
		/*!
		@brief	デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~EnergyGageFrame() {}
		//アクセサ
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
		virtual void OnDestroy()override;
	};
}
