
/*!
@file Scene.cpp
@brief シーン実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{
	void Scene::CreateResourses() {
		wstring dataDir;
		//サンプルのためアセットディレクトリを取得
		//App::GetApp()->GetAssetsDirectory(dataDir);
		//各ゲームは以下のようにデータディレクトリを取得すべき
		App::GetApp()->GetDataDirectory(dataDir);
		dataDir += L"Textures/";
		wstring strTexture = dataDir + L"trace.png";
		App::GetApp()->RegisterTexture(wstringKey::txTrace, strTexture);
		strTexture = dataDir + L"BackGround1S.png";
		App::GetApp()->RegisterTexture(wstringKey::txBackGround, strTexture);
		strTexture = dataDir + L"BackGround5.png";
		App::GetApp()->RegisterTexture(wstringKey::txBackGround2, strTexture);

		strTexture = dataDir + L"sky.jpg";
		App::GetApp()->RegisterTexture(wstringKey::txSky, strTexture);
		strTexture = dataDir + L"number_2.png";
		App::GetApp()->RegisterTexture(wstringKey::txNum, strTexture);
		strTexture = dataDir + L"abc.png";
		App::GetApp()->RegisterTexture(wstringKey::txAbc, strTexture);
		strTexture = dataDir + L"abc2.png";
		App::GetApp()->RegisterTexture(wstringKey::txABC, strTexture);
		strTexture = dataDir + L"abcw.png";
		App::GetApp()->RegisterTexture(wstringKey::txAbcW, strTexture);
		strTexture = dataDir + L"abc2w.png";
		App::GetApp()->RegisterTexture(wstringKey::txABCW, strTexture);
		strTexture = dataDir + L"wall.jpg";
		App::GetApp()->RegisterTexture(wstringKey::txWall, strTexture);
		strTexture = dataDir + L"Player.png";
		App::GetApp()->RegisterTexture(wstringKey::txTestPlayer, strTexture);
		strTexture = dataDir + L"SearchWave.png";
		App::GetApp()->RegisterTexture(wstringKey::txSearchWave, strTexture);
		strTexture = dataDir + L"testStar.png";
		App::GetApp()->RegisterTexture(wstringKey::txTestStar, strTexture);
		strTexture = dataDir + L"Spiral.png";
		App::GetApp()->RegisterTexture(wstringKey::txTestSpiral, strTexture);
		strTexture = dataDir + L"Spiral2.png";
		App::GetApp()->RegisterTexture(wstringKey::txTestSpiral2, strTexture);
		strTexture = dataDir + L"HpFrame.png";
		App::GetApp()->RegisterTexture(wstringKey::txHpFrame, strTexture);
		strTexture = dataDir + L"HpGage.png";
		App::GetApp()->RegisterTexture(wstringKey::txHpBar, strTexture);
		strTexture = dataDir + L"EneFrame2.png";
		App::GetApp()->RegisterTexture(wstringKey::txEnergyFrame, strTexture);
		strTexture = dataDir + L"EneGage2.png";
		App::GetApp()->RegisterTexture(wstringKey::txEnergyBar, strTexture);
		strTexture = dataDir + L"RadarGage.png";
		App::GetApp()->RegisterTexture(wstringKey::txRadarGage, strTexture);
		strTexture = dataDir + L"SpeedFrame.png";
		App::GetApp()->RegisterTexture(wstringKey::txSpeedFrame, strTexture);
		strTexture = dataDir + L"SpeedGage.png";
		App::GetApp()->RegisterTexture(wstringKey::txSpeedBar, strTexture);
		strTexture = dataDir + L"Plasma.png";
		App::GetApp()->RegisterTexture(wstringKey::txPlayerBullet, strTexture);
		strTexture = dataDir + L"Wave2.png";
		App::GetApp()->RegisterTexture(wstringKey::txWave1, strTexture);
		strTexture = dataDir + L"wakusei_R.png";
		App::GetApp()->RegisterTexture(wstringKey::txPlanet_R, strTexture);
		strTexture = dataDir + L"wakusei_Bl.png";
		App::GetApp()->RegisterTexture(wstringKey::txPlanet_Bl, strTexture);
		strTexture = dataDir + L"wakusei_G.png";
		App::GetApp()->RegisterTexture(wstringKey::txPlanet_G, strTexture);
		strTexture = dataDir + L"wakusei_Y.png";
		App::GetApp()->RegisterTexture(wstringKey::txPlanet_Y, strTexture);
		strTexture = dataDir + L"wakusei_P.png";
		App::GetApp()->RegisterTexture(wstringKey::txPlanet_P, strTexture);
		strTexture = dataDir + L"wakusei_Br.png";
		App::GetApp()->RegisterTexture(wstringKey::txPlanet_Br, strTexture);
		strTexture = dataDir + L"wakusei_1.png";
		App::GetApp()->RegisterTexture(wstringKey::txPlanets, strTexture);
		strTexture = dataDir + L"Debri.png";
		App::GetApp()->RegisterTexture(wstringKey::txDebris, strTexture);
		strTexture = dataDir + L"item.png";
		App::GetApp()->RegisterTexture(wstringKey::txItem, strTexture);
		strTexture = dataDir + L"Goal.png";
		App::GetApp()->RegisterTexture(wstringKey::txGoal, strTexture);
		strTexture = dataDir + L"gameclear.png";
		App::GetApp()->RegisterTexture(wstringKey::txGoalText, strTexture);
		strTexture = dataDir + L"gameover.png";
		App::GetApp()->RegisterTexture(wstringKey::txGameOverText, strTexture);
		strTexture = dataDir + L"Arrow.png";
		App::GetApp()->RegisterTexture(wstringKey::txArrow, strTexture);
		strTexture = dataDir + L"StartA.png";
		App::GetApp()->RegisterTexture(L"TX_StartA", strTexture);

		//チュートリアルテキスト
		strTexture = dataDir + L"T_Text1.png";
		App::GetApp()->RegisterTexture(wstringKey::txT_Text1, strTexture);
		strTexture = dataDir + L"T_Text2.png";
		App::GetApp()->RegisterTexture(wstringKey::txT_Text2, strTexture);
		strTexture = dataDir + L"T_Text3.png";
		App::GetApp()->RegisterTexture(wstringKey::txT_Text3, strTexture);
		strTexture = dataDir + L"T_Text4.png";
		App::GetApp()->RegisterTexture(wstringKey::txT_Text4, strTexture);
		strTexture = dataDir + L"T_Text5.png";
		App::GetApp()->RegisterTexture(wstringKey::txT_Text5, strTexture);
		strTexture = dataDir + L"T_Text6.png";
		App::GetApp()->RegisterTexture(wstringKey::txT_Text6, strTexture);
		strTexture = dataDir + L"T_Text7.png";
		App::GetApp()->RegisterTexture(wstringKey::txT_Text7, strTexture);
		strTexture = dataDir + L"T_Text8.png";
		App::GetApp()->RegisterTexture(wstringKey::txT_Text8, strTexture);

		strTexture = dataDir + L"T_Window1.png";
		App::GetApp()->RegisterTexture(wstringKey::txT_Window1, strTexture);
		strTexture = dataDir + L"T_Check.png";
		App::GetApp()->RegisterTexture(wstringKey::txT_Check, strTexture);
		strTexture = dataDir + L"T_Anim.png";
		App::GetApp()->RegisterTexture(wstringKey::txT_Anim, strTexture);

		// フェード用
		strTexture = dataDir + L"blackfade.png";
		App::GetApp()->RegisterTexture(L"txFade", strTexture);

		// パーティクル用
		strTexture = dataDir + L"tx_EffectBomb2_01.png";
		App::GetApp()->RegisterTexture(L"txEffBomb", strTexture);
		strTexture = dataDir + L"EffectBomb_04.png";
		App::GetApp()->RegisterTexture(L"txEffBombPlayer", strTexture);
		strTexture = dataDir + L"tx_EffectSurveyComp_01.png"; 
		App::GetApp()->RegisterTexture(L"txEffSurvey", strTexture);
		strTexture = dataDir + L"tx_EffectSurveyComp.png";
		App::GetApp()->RegisterTexture(L"txEffSurveyComp", strTexture);
		strTexture = dataDir + L"tx_EffectRecovery.png";
		App::GetApp()->RegisterTexture(L"txEffRecovery", strTexture);

		//リザルト
		strTexture = dataDir + L"ResultBG.png";
		App::GetApp()->RegisterTexture(wstringKey::txResultBG, strTexture);
		strTexture = dataDir + L"ResultWindow.png";
		App::GetApp()->RegisterTexture(wstringKey::txResultWindow, strTexture);
		strTexture = dataDir + L"Result_Rank.png";
		App::GetApp()->RegisterTexture(wstringKey::txResultRank, strTexture);
		strTexture = dataDir + L"MissonComplete3.png";
		App::GetApp()->RegisterTexture(wstringKey::txMissonComplete, strTexture);
		strTexture = dataDir + L"ResultWindow.png";
		App::GetApp()->RegisterTexture(wstringKey::txResultWindow, strTexture);
		strTexture = dataDir + L"rank.png";
		App::GetApp()->RegisterTexture(wstringKey::txRank, strTexture);
		strTexture = dataDir + L"rank_A.png";
		App::GetApp()->RegisterTexture(wstringKey::txRank_A, strTexture);
		strTexture = dataDir + L"rank_B.png";
		App::GetApp()->RegisterTexture(wstringKey::txRank_B, strTexture);
		strTexture = dataDir + L"rank_C.png";
		App::GetApp()->RegisterTexture(wstringKey::txRank_C, strTexture);
		strTexture = dataDir + L"rank_S.png";
		App::GetApp()->RegisterTexture(wstringKey::txRank_S, strTexture);

		//コピーライト
		strTexture = dataDir + L"CopyLight.png";
		App::GetApp()->RegisterTexture(wstringKey::txCopyLight, strTexture);

		//タイトル
		strTexture = dataDir + L"Title_Logo.png";
		App::GetApp()->RegisterTexture(wstringKey::txTitle_Logo, strTexture);
		strTexture = dataDir + L"Titlebutton.png";
		App::GetApp()->RegisterTexture(wstringKey::txMessage, strTexture);
		strTexture = dataDir + L"testShip.png";
		App::GetApp()->RegisterTexture(wstringKey::txShip, strTexture);

		//ステセレ
		strTexture = dataDir + L"StageSelect_Button.png";
		App::GetApp()->RegisterTexture(wstringKey::txStageSelect_Button, strTexture);
		strTexture = dataDir + L"Cursor2.png";
		App::GetApp()->RegisterTexture(wstringKey::txStageSelect_Cursor, strTexture);
		strTexture = dataDir + L"StageSelect_StNo.png";
		App::GetApp()->RegisterTexture(wstringKey::txStageSelectNo, strTexture);
		//ゲームオーバー
		strTexture = dataDir + L"GameOver_BG.png";
		App::GetApp()->RegisterTexture(wstringKey::txGameOverBG , strTexture);
		strTexture = dataDir + L"GameOver_Logo.png";
		App::GetApp()->RegisterTexture(wstringKey::txGameOver_Logo , strTexture);
		strTexture = dataDir + L"Gover_Retry.png";
		App::GetApp()->RegisterTexture(wstringKey::txGameOver_Retry, strTexture);
		strTexture = dataDir + L"Gover_Select.png";
		App::GetApp()->RegisterTexture(wstringKey::txGameOver_Select, strTexture);
		strTexture = dataDir + L"Gover_Title.png";
		App::GetApp()->RegisterTexture(wstringKey::txGameOver_Title, strTexture);
		//BGM
		wstring strMusic = dataDir + L"BGM_3.wav";
		App::GetApp()->RegisterWav(wstringKey::bgmGame, strMusic);
		strMusic = dataDir + L"BGM_4.wav";
		App::GetApp()->RegisterWav(wstringKey::bgmTitle, strMusic);
		strMusic = dataDir + L"BGM_7.wav";
		App::GetApp()->RegisterWav(wstringKey::bgmResult, strMusic);
		strMusic = dataDir + L"Title_2.wav";
		App::GetApp()->RegisterWav(wstringKey::bgmSelect, strMusic);
		strMusic = dataDir + L"GOver_Stage.wav";
		App::GetApp()->RegisterWav(wstringKey::bgmGOver, strMusic);
		strMusic = dataDir + L"GameOver_BGM.wav";
		App::GetApp()->RegisterWav(L"GameOver_BGM", strMusic);
		//SE
		wstring strSe = dataDir + L"kettei.wav";
		App::GetApp()->RegisterWav(wstringKey::seKettei, strSe);
		strSe = dataDir + L"SE_cursor.wav";
		App::GetApp()->RegisterWav(wstringKey::seCursor, strSe);
		strSe = dataDir + L"Complete.wav";
		App::GetApp()->RegisterWav(wstringKey::seComplete, strSe);
		strSe = dataDir + L"shot1.wav";
		App::GetApp()->RegisterWav(wstringKey::seShot, strSe);
		strSe = dataDir + L"Boost.wav";
		App::GetApp()->RegisterWav(wstringKey::seBoost, strSe);
		strSe = dataDir + L"rank_S.wav";
		App::GetApp()->RegisterWav(wstringKey::seRank_S, strSe);
		strSe = dataDir + L"rank_A.wav";
		App::GetApp()->RegisterWav(wstringKey::seRank_A, strSe);
		strSe = dataDir + L"rank_B.wav";
		App::GetApp()->RegisterWav(wstringKey::seRank_B, strSe);
		strSe = dataDir + L"rank_C.wav";
		App::GetApp()->RegisterWav(wstringKey::seRank_C, strSe);
		strSe = dataDir + L"Explosion.wav";
		App::GetApp()->RegisterWav(wstringKey::seExplosion, strSe);
		strSe = dataDir + L"Search.wav";
		App::GetApp()->RegisterWav(wstringKey::seSearch, strSe);
		strSe = dataDir + L"BoostLoop.wav";
		App::GetApp()->RegisterWav(wstringKey::seBoostLoop, strSe);
		strSe = dataDir + L"Survey.wav";
		App::GetApp()->RegisterWav(L"Survey_SE", strSe);
		strSe = dataDir + L"breakDebris.wav";
		App::GetApp()->RegisterWav(L"BreakDebris_SE", strSe);
		strSe = dataDir + L"fireBall.wav";
		App::GetApp()->RegisterWav(L"FireBall_SE", strSe);
		strSe = dataDir + L"Recovery.wav";
		App::GetApp()->RegisterWav(L"Recovery_SE", strSe);

	}
	//--------------------------------------------------------------------------------------
	///	ゲームシーン
	//--------------------------------------------------------------------------------------
	void Scene::OnCreate(){
		try {
			//マウスカーソル消去
			::ShowCursor(false);
			//リソース生成
			CreateResourses();
			//クリアする色を設定
			Col4 Col;
			Col.set(31.0f / 255.0f, 30.0f / 255.0f, 71.0f / 255.0f, 255.0f / 255.0f);
			SetClearColor(Col);
			//自分自身にイベントを送る
			//これにより各ステージやオブジェクトがCreate時にシーンにアクセスできる
			PostEvent(0.0f, GetThis<ObjectInterface>(), GetThis<Scene>(), L"ToCopyLightStage");
			//PostEvent(0.0f, GetThis<ObjectInterface>(), GetThis<Scene>(), L"ToResultStage");
			//PostEvent(0.0f, GetThis<ObjectInterface>(), GetThis<Scene>(), L"ToGameStage4");

		}
		catch (...) {
			throw;
		}
	}

	Scene::~Scene() {
	}

	void Scene::OnEvent(const shared_ptr<Event>& event) {
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		if (event->m_MsgStr == L"ToTutorialStage") {
			XAPtr->Stop(m_BGM);
			m_BGM = XAPtr->Start(wstringKey::bgmGame, XAUDIO2_LOOP_INFINITE, 0.15f);
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage>();
		}
		if (event->m_MsgStr == L"ToGameStage1") {
			XAPtr->Stop(m_BGM);
			m_BGM = XAPtr->Start(wstringKey::bgmGame, XAUDIO2_LOOP_INFINITE, 0.15f);
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage2>();
		}
		if (event->m_MsgStr == L"ToGameStage2") {
			XAPtr->Stop(m_BGM);
			m_BGM = XAPtr->Start(wstringKey::bgmGame, XAUDIO2_LOOP_INFINITE, 0.15f);
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage3>();
		}
		if (event->m_MsgStr == L"ToGameStage3") {
			XAPtr->Stop(m_BGM);
			m_BGM = XAPtr->Start(wstringKey::bgmGame, XAUDIO2_LOOP_INFINITE, 0.15f);
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage4>();
		}
		if (event->m_MsgStr == L"ToGameStage4") {
			XAPtr->Stop(m_BGM);
			m_BGM = XAPtr->Start(wstringKey::bgmGame, XAUDIO2_LOOP_INFINITE, 0.15f);
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage5>();
		}
		if (event->m_MsgStr == L"ToGameStage5") {
			XAPtr->Stop(m_BGM);
			m_BGM = XAPtr->Start(wstringKey::bgmGame, XAUDIO2_LOOP_INFINITE, 0.15f);
			//最初のアクティブステージの設定
			ResetActiveStage<GameStage6>();
		}
		else if (event->m_MsgStr == L"ToTitleStage") {
			XAPtr->Stop(m_BGM);
			m_BGM = XAPtr->Start(wstringKey::bgmTitle, XAUDIO2_LOOP_INFINITE, 0.15f);
			ResetActiveStage<TitleStage>();
		}
		else if (event->m_MsgStr == L"ToCopyLightStage") {
			ResetActiveStage<CopyLightStage>();
		}
		else if (event->m_MsgStr == L"ToSelectStage") {
			XAPtr->Stop(m_BGM);
			m_BGM = XAPtr->Start(wstringKey::bgmSelect, XAUDIO2_LOOP_INFINITE, 0.15f);
			ResetActiveStage<SelectStage>();
		}
		else if (event->m_MsgStr == L"ToResultStage") {
			XAPtr->Stop(m_BGM);
			m_BGM = XAPtr->Start(wstringKey::bgmResult, XAUDIO2_LOOP_INFINITE, 0.15f);
			ResetActiveStage<ResultStage>();
		}
		else if (event->m_MsgStr == L"ToGameOverStage") {
			XAPtr->Stop(m_BGM);
			m_BGM = XAPtr->Start(wstringKey::bgmGOver, XAUDIO2_LOOP_INFINITE, 0.15f);
			//m_BGM = XAPtr->Start(L"GameOver_BGM", XAUDIO2_LOOP_INFINITE, 0.25f);
			ResetActiveStage<GameOverStage>();
		}
	}
	// ランク計算
	void Scene::RankCalc() {
		UINT planetPtr = App::GetApp()->GetScene<Scene>()->GetPlanetCount();
		UINT countMax = App::GetApp()->GetScene<Scene>()->GetPlanetMax();

		float recoveryRate = (float)planetPtr / countMax;
		if (recoveryRate >= 0.5f) {
			m_Rank = int(4 * recoveryRate) - 1;
		}
	}

	// スコア付け
	void Scene::SetAllHighScore(const wstring& sceneKey, const int& stageNum) {
		if (nowScene == sceneKey) {
			UINT highScore = this->GetHighScore(stageNum);
			if (highScore < m_Planet) {
				this->SetHighScore(stageNum, m_Planet);
				this->RankCalc();
				this->SetBestRank(stageNum, m_Rank);
			}
		}
	}

}
//end basecross
