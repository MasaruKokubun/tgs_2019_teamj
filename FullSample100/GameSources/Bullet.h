#pragma once
#include "stdafx.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
///	テストステージブジェクト
//--------------------------------------------------------------------------------------
	class Bullet :public GameObject
	{
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		Vec3 m_Angle;
		float m_Speade;
		//文字列の表示
		void DrawStrings();
	public:
		//コンストラクタ&デストラクタ
		Bullet(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, const Vec3& angle);
		~Bullet() {}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
		virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;
		virtual void OnCollisionExcute(shared_ptr<GameObject>& Other) override;
		virtual void OnCollisionExit(shared_ptr<GameObject>& Other) override;
	};
}