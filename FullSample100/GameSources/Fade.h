/*!
@file Fade.h
@brief フェード
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	フェードアウトクラス
	//--------------------------------------------------------------------------------------
	class FadeOut : public GameObject
	{
		Vec2 m_Scale;
		Vec2 m_Pos;
		wstring m_TextureKey;
		bool m_Trace;
		float m_Alpha;
		bool m_FadeFlg;
	public :
		// 構築と破棄
		FadeOut(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, const bool Trace, const Vec2& Scale, const Vec2& Pos);
		~FadeOut() {}
		// 初期化
		void OnCreate() override;
		// 更新
		void OnUpdate() override;
		// ほかのオブジェクトの動き
		void OtherUpdateActive(bool active);

		// ゲッターとセッター
		void SetFadeFlg(bool fadeFlg);

		float GetAlpha() const;
		bool GetFadeFlg() const;
	};


	//--------------------------------------------------------------------------------------
	//	フェードインクラス
	//--------------------------------------------------------------------------------------
	class FadeIn : public GameObject
	{
		Vec2 m_Scale;
		Vec2 m_Pos;
		wstring m_TextureKey;
		bool m_Trace;

		float m_Alpha;
		bool m_FadeFlg;

		shared_ptr<StartSprite> m_StartSprite = nullptr;
	public:
		// 構築と破棄
		FadeIn(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, const bool Trace, const Vec2& Scale, const Vec2& Pos);
		~FadeIn() {}
		// 初期化
		void OnCreate() override;
		// 更新
		void OnUpdate() override;
		// ほかのオブジェクトの動き
		void OtherUpdateActive(bool active);

		// ゲッターとセッター
		void SetFadeFlg(bool fadeFlg);

		float GetAlpha() const;
		bool GetFadeFlg() const;
	};

}
//end basecross

