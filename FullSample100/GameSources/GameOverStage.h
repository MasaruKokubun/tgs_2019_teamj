/*!
@file GameOverStage.h
@brief ゲームオーバーステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
//	セレクトマネージャー
//--------------------------------------------------------------------------------------
	class SelectManager_GG : public GameObject {
		shared_ptr<SoundItem> m_BGM;
		float m_vol = 0.1f;
	private:
		Level m_level;	// 今選択しているレベル
		float m_TotalTime;	// 選択する時に使う
		float m_ScaleTime;	// スケールを変更する用
		int m_x;
		int m_y;
		bool m_lr = false;
		vector<vector<Level>> m_StageManager;	// 選択する時に使う
		bool m_fadeFlag = false;
		bool m_fadeoutFlag = true;

	public:
		// 構築と破壊
		SelectManager_GG(const shared_ptr<Stage>& StagePtr);
		virtual ~SelectManager_GG();
		//初期化
		virtual void OnCreate()override;
		// 更新
		virtual void OnUpdate()override;
		//// 後更新
		//virtual void OnUpdate2()override;
		//// 文字列の表示
		//void DrawStrings();
		//セッターとゲッター
		bool GetFadeFlag() { return m_fadeFlag; };
		void SetFadeFlag(bool flag) { m_fadeFlag = flag; };
		bool GetFadeoutFlag() { return m_fadeoutFlag; };
		void SetFadeoutFlag(bool flag) { m_fadeoutFlag = flag; };
		// レベルの取得
		Level GetLevel() const { return m_level; }
	};
//--------------------------------------------------------------------------------------
//	セレクトマネージャー
//--------------------------------------------------------------------------------------
	class SelectManager_G : public GameObject {
	private:
		float m_TotalTime;	// 選択する時に使う
		float m_ScaleTime;	// スケールを変更する用
		int m_x;
		int m_y;
		//現在のカーソルの位置
		int m_position = 1;
		bool m_Active = true;
	public:
		// 構築と破壊
		SelectManager_G(const shared_ptr<Stage>& StagePtr);
		virtual ~SelectManager_G();
		//初期化
		virtual void OnCreate()override {}
		// 更新
		virtual void OnUpdate()override;
		// 後更新
		virtual void OnUpdate2()override {}
		// 文字列の表示
		void DrawStrings() {}
		//ゲッター
		int GetCursorPosition() { return m_position; };
		//セッター
		void SetCursorPosition(int num) { m_position = num; };
	};

	//--------------------------------------------------------------------------------------
	//	ゲームオーバーステージクラス
	//--------------------------------------------------------------------------------------
	class GameOverStage : public Stage {
		float m_FinishPosY = 300.0f;
		float m_StartPosY = 600.0f;
		float m_Alpha = 0.0f;
		float m_Alpha_C = 0.0f;
		float m_Time = 0.0f;

		bool m_CursorFlg = false;

		// ビューの作成
		void CreateViewLight();
		// スプライトの作成
		void CreateGameOverSprite();
		//カーソルの作成
		void CreateCursor();
		// 入力ハンドラー
		//InputHandler<GameOverStage> m_InputHandler;
		// シーン遷移
		void OnSceneTransition(const wstring& wstringKey);

	public:
		// 構築と破棄
		GameOverStage() : Stage() {}
		virtual ~GameOverStage() {}
		//初期化
		virtual void OnCreate() override;
		// 更新
		virtual void OnUpdate() override;
		//アニメーション
		void OnAnimation();
		//カーソル移動
		void OnCursorMove();

		// ゲッター
		bool GetCursorFlg() const;

		// Aボタン
		//void OnPushA();
	};

}
//end basecross

