#pragma once
#include "stdafx.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	///	テストステージブジェクト
	//--------------------------------------------------------------------------------------
	class TestStageObject :public GameObject
	{
	protected:
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		int m_ChildrenAnimCol;
		int m_ChildrenAnimRow;
		shared_ptr<GameObject> m_CorePtr;
		//文字列の表示
		void DrawStrings();
	public:
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[1][2];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
		//コンストラクタ&デストラクタ
		TestStageObject(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position,int col=0,int row=1);
		~TestStageObject() {}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
	};

	//--------------------------------------------------------------------------------------
	///	テストステージブジェクトチルドレン
	//--------------------------------------------------------------------------------------
	class TestStageObjectChildren :public GameObject
	{
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		const shared_ptr<GameObject>& m_ParentPtr;
		//文字列の表示
		void DrawStrings();
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[6][6];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
	public:
		bool m_HitFlg;
		bool m_IntervalFlg;
		float m_IntervalTime;
		//ゲージ
		float m_Gage;
		//カウントフラグ
		bool m_CountFlg=true;
		//コンストラクタ&デストラクタ
		TestStageObjectChildren(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, const shared_ptr<GameObject>& parentPtr=nullptr, int col = 0, int row = 1);
		~TestStageObjectChildren() {}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
		//virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;
		//virtual void OnCollisionExcute(shared_ptr<GameObject>& Other) override;
		//virtual void OnCollisionExit(shared_ptr<GameObject>& Other) override;
	};

	//--------------------------------------------------------------------------------------
	///	破壊可能なステージブジェクト
	//--------------------------------------------------------------------------------------
	class BreakableObject : public GameObject
	{
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;

		bool m_Destroyflg;
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[3][3];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
	public:
		//コンストラクタ&デストラクタ
		BreakableObject(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position);
		~BreakableObject() {}
		//初期化
		virtual void OnCreate() override;
		////更新
		//virtual void OnUpdate() override;
		//virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;
	};

	//--------------------------------------------------------------------------------------
	///	テストアイテム
	//--------------------------------------------------------------------------------------
	class TestItem :public GameObject
	{
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		//文字列の表示
		void DrawStrings();
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[1][1];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
	public:
		float m_delta;
		//コンストラクタ&デストラクタ
		TestItem(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position);
		~TestItem() {}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
	};
	//--------------------------------------------------------------------------------------
	///	テストアイテムチルドレン
	//--------------------------------------------------------------------------------------
	class TestItemChildren :public GameObject
	{
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		//文字列の表示
		void DrawStrings();
	public:
		//コンストラクタ&デストラクタ
		TestItemChildren(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position);
		~TestItemChildren() {}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
		virtual void OnCollisionEnter(shared_ptr<GameObject>& Other) override;
		virtual void OnCollisionExcute(shared_ptr<GameObject>& Other) override;
		virtual void OnCollisionExit(shared_ptr<GameObject>& Other) override;
	};
}
