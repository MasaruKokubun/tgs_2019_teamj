/*!
@file Particle.cpp
@brief 
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {


	//--------------------------------------------------------------------------------------
	//	爆発パーティクルクラス
	//--------------------------------------------------------------------------------------
	// 構築と破棄
	BombParticle::BombParticle(const shared_ptr<Stage>& StagePtr)
		:
		MultiParticle(StagePtr)
	{}


	// 初期化
	void BombParticle::OnCreate()
	{
		// 加算処理
		SetAddType(true);
		SetDrawLayer(2);
	}

	void BombParticle::InsertBomb(const Vec3& Pos, const wstring& wstringKey)
	{
		auto PtrParticle = InsertParticle(4);
		PtrParticle->SetEmitterPos(Pos);
		PtrParticle->SetTextureResource(wstringKey);
		PtrParticle->SetMaxTime(0.5f);
		for (auto& par : PtrParticle->GetParticleSpriteVec()) {
			par.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;
			par.m_LocalPos.y = Util::RandZeroToOne() * 0.1f;
			par.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f;
			// 各パーティクルの移動速度の指定
			par.m_Velocity = par.m_LocalPos * 5.0f;
			// 色の指定
			par.m_Color = Col4(1.0f, 1.0f, 1.0f, 1.0f);
		}
		//SE
		auto AudioManager = App::GetApp()->GetXAudio2Manager();
		AudioManager->Start(wstringKey::seExplosion, 0, 0.3f);

	}

	//--------------------------------------------------------------------------------------
	//	回復用パーティクルクラス
	//--------------------------------------------------------------------------------------
	// 構築と破棄
	RecoveryParticle::RecoveryParticle(const shared_ptr<Stage>& StagePtr)
		:
		MultiParticle(StagePtr)
	{
	}

	// 初期化
	void RecoveryParticle::OnCreate()
	{
		// 加算処理
		SetAddType(true);
	}

	void RecoveryParticle::InsertRecovery(const Vec3& Pos)
	{
		SetDrawLayer(2);
		auto PtrParticle = InsertParticle(3);
		PtrParticle->SetEmitterPos(Pos);
		PtrParticle->SetTextureResource(L"txEffRecovery");
		PtrParticle->SetMaxTime(0.5f);

		for (auto& par : PtrParticle->GetParticleSpriteVec())
		{
			Col4 white = Col4(1.0f, 1.0f, 1.0f, 1.0f);
			par.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;
			par.m_LocalPos.y = Util::RandZeroToOne() * 0.1f;
			par.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f;
			// 各パーティクルの移動速度の指定
			par.m_Velocity = par.m_LocalPos * 5.0f;
			// 色の指定
			par.m_Color = white;

		}
		// SE
		auto AudioManager = App::GetApp()->GetXAudio2Manager();
		AudioManager->Start(L"Recovery_SE", 0, 0.3f);

	}
}
//end basecross