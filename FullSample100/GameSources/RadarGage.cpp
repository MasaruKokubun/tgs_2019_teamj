#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class RadarGageBar : public GameObject;
	//	用途: レーダーゲージバー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	RadarGageBar::RadarGageBar(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<TestStageObjectChildren> corePtr) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_CorePtr(corePtr)
	{}

	//初期化
	void RadarGageBar::OnCreate() {

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);
		//タグ付け
		AddTag(wstringKey::tagRadarGageBar);

		////文字列をつける
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
		//アニメチップ作成
		float tipWidth = 128.0f / 256.0f;
		float tipHeight = 64.0f / 128.0f;
		for (int c = 0; c < 2; c++) {
			for (int r = 0; r < 2; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 0;
		m_AnimTipCol = 1;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		float maxSize = 1.0f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-0.0f, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+maxSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-0.0f, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+maxSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);
		PtrSpriteDraw->SetTextureResource(wstringKey::txRadarGage);
		PtrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		//PtrDraw->SetTextureResource(L"WaraFront_TX");
		//透明処理
		SetAlphaActive(true);

		GetStage()->AddGameObject<RadarGageFrame>(m_Scale, m_Rotation, Vec3(m_Scale.x / 2.0f, 0.0f, 0.0f),GetThis<GameObject>());

		SetDrawActive(false);
		//SetUpdateActive(false);

		SetDrawLayer(5);
	}

	void RadarGageBar::SetRadarGageActive(Vec3 pos) {
		SetDrawActive(true);
		//SetUpdateActive(true);
		GetComponent<Transform>()->SetPosition(pos);
		//子オブジェクト参照テスト
		vector<shared_ptr<GameObject>> childVec;
		//この段階でchildVecに入る
		auto& objeVec = GetStage()->GetGameObjectVec();
		for (auto& v : objeVec) {
			auto ptrTrans = v->GetComponent<Transform>();
			auto ptrParent = ptrTrans->GetParent();
			if (GetThis<GameObject>() == ptrParent) {
				v->SetDrawActive(true);
				//v->SetUpdateActive(true);
			}
		}
	}

	//更新
	void RadarGageBar::OnUpdate() {
		if (m_CorePtr!=nullptr) {
//			auto ptrTrans = GetComponent<Transform>();
//			Quat qt;
//			auto camera = OnGetDrawCamera();
//			auto eye = camera->GetEye();
//			auto at = camera->GetAt();
//			auto norm = at - eye;
//			norm.normalize();
//			qt.facing(norm);
//			ptrTrans->SetQuaternion(qt);


			auto gage = m_CorePtr->m_Gage;
			float elapsedTime = App::GetApp()->GetElapsedTime();
			m_delta += elapsedTime;
			float HelfSize = 0.5f;
			float maxSize = 1.0f*gage;
			Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
			auto animTip = m_AnimTip[m_AnimTipCol][m_AnimTipRow];
			vector<VertexPositionColorTexture> vertices = {
				{ Vec3(-0.0f, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
				{ Vec3(+maxSize, +HelfSize, 0), white, Vec2(animTip.right*gage, animTip.top) },
				{ Vec3(-0.0f, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
				{ Vec3(+maxSize, -HelfSize, 0), white, Vec2(animTip.right*gage, animTip.bottom) }
			};


			auto drawComp = GetComponent<PCTStaticDraw>();
			drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);
		}
	}

	void RadarGageBar::OnUpdate2() {
		////文字列の表示
		//DrawStrings();
	}
	void RadarGageBar::OnDestroy() {
		//子オブジェクト参照テスト
		vector<shared_ptr<GameObject>> childVec;
		//この段階でchildVecに入る
		auto& objeVec = GetStage()->GetGameObjectVec();
		for (auto& v : objeVec) {
			auto ptrTrans = v->GetComponent<Transform>();
			auto ptrParent = ptrTrans->GetParent();
			if (GetThis<GameObject>() == ptrParent) {
				//v->GetComponent<RadarGageFrame>()->OnDestroy();
				v->SetDrawActive(false);
			}
		}
		SetDrawActive(false);
		//SetUpdateActive(false);
	}

	//--------------------------------------------------------------------------------------
	//	class RadarGageFrame : public GameObject;
	//	用途: レーダーゲージフレーム
	//--------------------------------------------------------------------------------------
	//構築と破棄
	RadarGageFrame::RadarGageFrame(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<GameObject> parentPtr) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_ParentPtr(parentPtr)
	{}

	//初期化
	void RadarGageFrame::OnCreate() {
		auto ptrTransform = GetComponent<Transform>();
		//親子関係化
		ptrTransform->SetParent(m_ParentPtr);

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		////文字列をつける
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
		//アニメチップ作成
		float tipWidth = 128.0f / 256.0f;
		float tipHeight = 64.0f / 128.0f;
		for (int c = 0; c < 2; c++) {
			for (int r = 0; r < 2; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 1;
		m_AnimTipCol = 0;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		float maxSize = 1.0f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);
		PtrSpriteDraw->SetTextureResource(wstringKey::txRadarGage);
		PtrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		//透明処理
		SetAlphaActive(true);
		SetDrawLayer(4);
		SetDrawActive(false);
		//SetUpdateActive(false);
	}

	//更新
	void RadarGageFrame::OnUpdate() {
		//auto gage = m_CorePtr->m_Gage;
		////auto gage = 0.5f;
		//float elapsedTime = App::GetApp()->GetElapsedTime();
		//m_delta += elapsedTime;
		//float HelfSize = 0.5f;
		//float maxSize = 1.0f*gage;
		//Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		//auto animTip = m_AnimTip[m_AnimTipCol][m_AnimTipRow];
		//vector<VertexPositionColorTexture> vertices = {
		//	{ Vec3(-0.0f, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
		//	{ Vec3(+maxSize, +HelfSize, 0), white, Vec2(animTip.right*gage, animTip.top) },
		//	{ Vec3(-0.0f, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
		//	{ Vec3(+maxSize, -HelfSize, 0), white, Vec2(animTip.right*gage, animTip.bottom) }
		//};
		//auto drawComp = GetComponent<PCTStaticDraw>();
		//drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);
	}

	void RadarGageFrame::OnUpdate2() {
		////文字列の表示
		//DrawStrings();
	}

	void RadarGageFrame::OnDestroy() {
		SetDrawActive(false);
		//SetUpdateActive(false);
	}
}
