#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	///	レーダー
	//--------------------------------------------------------------------------------------
	class Radar :public GameObject
	{
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		shared_ptr<GameObject> m_ParentPtr;
		//ステートマシン
		unique_ptr<StateMachine<Radar>>  m_StateMachine;

		shared_ptr<SoundItem> m_SE;

		//文字列の表示
		void DrawStrings();
	public:
		float m_Delta;
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[4][4];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
		//コンストラクタ&デストラクタ
		Radar(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<GameObject> parentPtr = nullptr);
		~Radar() {}
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;

		//ステートマシンの取得
		unique_ptr< StateMachine<Radar>>& GetStateMachine() {
			return m_StateMachine;
		}

		//SEセッター,ゲッター
		void SetSE(const wstring& wstringKey);
		shared_ptr<SoundItem> GetSE() const;

	};

	//レーダーステート
	//--------------------------------------------------------------------------------------
	/// デフォルト
	//--------------------------------------------------------------------------------------
	class RadarDefault : public ObjState<Radar> {
		RadarDefault() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(RadarDefault)
		void Enter(const shared_ptr<Radar>& obj) override;
		void Execute(const shared_ptr<Radar>& obj) override;
		void Exit(const shared_ptr<Radar>& obj) override;
	};
	//--------------------------------------------------------------------------------------
	/// 展開
	//--------------------------------------------------------------------------------------
	class RadarExpansion : public ObjState<Radar> {
		RadarExpansion() {}
	public:
		//ステートのインスタンス取得
		DECLARE_SINGLETON_INSTANCE(RadarExpansion)
		void Enter(const shared_ptr<Radar>& obj) override;
		void Execute(const shared_ptr<Radar>& obj) override;
		void Exit(const shared_ptr<Radar>& obj) override;
	};
}