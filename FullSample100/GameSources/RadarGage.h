#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	///	レーダーゲージバー
	//--------------------------------------------------------------------------------------
	class RadarGageBar : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		shared_ptr<TestStageObjectChildren> m_CorePtr;
		//文字列の表示
		//void DrawStrings();
	public:
		bool m_DestroyFlg = false;
		float m_delta;
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[2][2];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
		//構築と破棄
		//--------------------------------------------------------------------------------------
		/*!
		@brief	コンストラクタ
		@param[in]	StagePtr	ステージ
		*/
		//--------------------------------------------------------------------------------------
		RadarGageBar(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<TestStageObjectChildren> gagePtr=nullptr);
		//--------------------------------------------------------------------------------------
		/*!
		@brief	デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~RadarGageBar() {}

		//
		void SetCorePtr(shared_ptr<TestStageObjectChildren> corePtr) {
			m_CorePtr = corePtr;
		}
		void SetRadarGageActive(Vec3 pos);
		//アクセサ
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
		virtual void OnDestroy()override;
	};

	//--------------------------------------------------------------------------------------
	///	レーダーゲージフレーム
	//--------------------------------------------------------------------------------------
	class RadarGageFrame : public GameObject {
		Vec3 m_Scale;
		Vec3 m_Rotation;
		Vec3 m_Position;
		shared_ptr<GameObject> m_ParentPtr;
		//文字列の表示
		//void DrawStrings();
	public:
		float m_delta;
		// アニメーション用のデータ
		Rect2D<float> m_AnimTip[2][2];
		int m_AnimTipCol, m_AnimTipRow;
		float m_AnimTime;
		//構築と破棄
		//--------------------------------------------------------------------------------------
		/*!
		@brief	コンストラクタ
		@param[in]	StagePtr	ステージ
		*/
		//--------------------------------------------------------------------------------------
		RadarGageFrame(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, shared_ptr<GameObject> parentPtr = nullptr);
		//--------------------------------------------------------------------------------------
		/*!
		@brief	デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~RadarGageFrame() {}
		//アクセサ
		//初期化
		virtual void OnCreate() override;
		//更新
		virtual void OnUpdate() override;
		virtual void OnUpdate2() override;
		virtual void OnDestroy()override;
	};

}
