#pragma once
#include "stdafx.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	///	背景スクロール
	//--------------------------------------------------------------------------------------
	class BGScroll : public GameObject {
		bool m_Trace;
		Vec2 m_StartScale;
		Vec2 m_StartPos;
		Vec2 m_Angle;
		wstring m_TextureKey;
		vector<VertexPositionColorTexture> m_BackupVertices;

		Rect m_Rect;

	public:
		BGScroll(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
			const Vec2& StartScale, const Vec2& StartPos);
		BGScroll(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
			const Vec2& StartScale, const Vec2& StartPos, const Rect& Rect);
		virtual ~BGScroll();
		virtual void OnCreate()override;
		virtual void OnUpdate()override;

		// セッター, ゲッター
		void SetAngle(Vec2 Angle);
		void SetAngle(float x, float y);

		Rect GetRect() const;

	};
}