/*!
@file GameStage6.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス実体
	//--------------------------------------------------------------------------------------
	void GameStage6::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<MyCamera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(Vec3(0.0f, 5.0f, -5.0f));
		PtrCamera->SetAt(Vec3(0.0f, 0.0f, 0.0f));
		//マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		//デフォルトのライティングを指定
		PtrMultiLight->SetDefaultLighting();
	}
	void GameStage6::CreateBG() {
		auto backGroundPtr = AddGameObject<BGScroll>(wstringKey::txBackGround, true,
			Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f), Rect((size_t)0.0f, (size_t)0.0f, (size_t)m_Width, (size_t)m_Height));
		backGroundPtr->SetDrawLayer(-1);
	}
	//テストボックスの作成
	void GameStage6::CreateTestBox() {
		//テストフレーム
		AddGameObject<FrameBox>(Vec3(m_Width + frameOverlap, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(m_HalfWidth, 0.0f, 0.0f));
		AddGameObject<FrameBox>(Vec3(m_Width + frameOverlap, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(m_HalfWidth, m_Height, 0.0f));
		AddGameObject<FrameBox>(Vec3(1.0f, m_Height - frameOverlap, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, m_HalfHeight, 0.0f));
		AddGameObject<FrameBox>(Vec3(1.0f, m_Height - frameOverlap, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(m_Width, m_HalfHeight, 0.0f));


		//テストオブジェクト
		Vec3 scale(4.0f, 4.0f, 4.0f);
		scale *= 1.5f;
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3( 3.0f,  3.0f, 0.0f));
		AddGameObject<SatelliteWithPlanet>(scale,			   Vec3(0.0f, 0.0f, 0.0f), Vec3( 4.0f, 53.0f, 0.0f));
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3( 5.0f,  4.0f, 0.0f));
		AddGameObject<SatelliteWithPlanet>(scale,			   Vec3(0.0f, 0.0f, 0.0f), Vec3( 8.0f, 19.0f, 0.0f));
	
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(10.0f, 24.0f, 0.0f));
		AddGameObject<SatelliteWithPlanet>(scale,			   Vec3(0.0f, 0.0f, 0.0f), Vec3(11.0f,  5.3f, 0.0f));
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(15.0f, 55.0f, 0.0f));
		AddGameObject<SatelliteWithPlanet>(scale,			   Vec3(0.0f, 0.0f, 0.0f), Vec3(16.0f, 45.0f, 0.0f));
		AddGameObject<BulletShotObject>(scale,				   Vec3(0.0f, 0.0f, 0.0f), Vec3(19.0f, 35.0f, 0.0f));
	
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(20.0f, 13.0f, 0.0f));
		AddGameObject<BulletShotObject>(scale,				   Vec3(0.0f, 0.0f, 0.0f), Vec3(21.0f, 24.0f, 0.0f));
		AddGameObject<BulletShotObject>(scale,				   Vec3(0.0f, 0.0f, 0.0f), Vec3(22.0f, 53.0f, 0.0f));
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(23.0f, 15.0f, 0.0f));
		AddGameObject<TestStageObject>(scale,				   Vec3(0.0f, 0.0f, 0.0f), Vec3(29.0f, 39.0f, 0.0f));
	
		AddGameObject<BulletShotObject>(scale,				   Vec3(0.0f, 0.0f, 0.0f), Vec3(31.0f,  7.0f, 0.0f));
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(32.0f, 45.0f, 0.0f));
		AddGameObject<TestItem>		  (Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(34.0f,  5.0f, 0.0f));
		AddGameObject<SatelliteWithPlanet>(scale,			   Vec3(0.0f, 0.0f, 0.0f), Vec3(35.0f, 18.0f, 0.0f));
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(37.0f, 30.0f, 0.0f));
	
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(40.0f,  3.0f, 0.0f));
		AddGameObject<TestStageObject>(scale,				   Vec3(0.0f, 0.0f, 0.0f), Vec3(40.5f, 23.5f, 0.0f));
		AddGameObject<TestItem>		  (Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(41.0f, 37.0f, 0.0f));
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(47.0f, 54.0f, 0.0f));
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(49.0f, 28.0f, 0.0f));
	
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(50.0f,  3.0f, 0.0f));
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(50.0f, 37.0f, 0.0f));
		AddGameObject<SatelliteWithPlanet>(scale,			   Vec3(0.0f, 0.0f, 0.0f), Vec3(53.0f, 55.0f, 0.0f));
		AddGameObject<BreakableObject>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(55.0f, 14.0f, 0.0f));
		AddGameObject<TestItem>(Vec3(1.0f, 1.0f, 1.0f),		   Vec3(0.0f, 0.0f, 0.0f), Vec3(54.0f, 25.0f, 0.0f));

		//↓弾を撃つオブジェクトを置くときに一緒に書いてください
		CreateSharedObjectGroup(L"EnemyBulletGroup");



		// アイテムオブジェクト

		// ゴール用オブジェクト
		auto goalPtr = AddGameObject<GoalArea>(Vec3(3.0f, 3.0f, 3.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(m_HalfWidth, m_HalfHeight, 0.0f));
		SetSharedGameObject(wstringKey::obGoal, goalPtr);

	}
	//テストプレイヤーの作成
	void GameStage6::CreateTestPlayer() {
		CreateSharedObjectGroup(wstringKey::obgSearchArea);
		auto playerPtr = AddGameObject<Player>(Vec3(0.5f, 0.5f, 0.5f), Vec3(0.0f, 0.0f, 0.0f), Vec3(m_HalfWidth, m_HalfHeight + 3.0f, 0.0f), Vec3(0.001f, 1.0f, 0.0f));
		SetSharedGameObject(wstringKey::obPlayer, playerPtr);
		//レーダーゲージ
		auto radarGagePtr = AddGameObject<RadarGageBar>(Vec3(1.0f, 1.0f, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, 0.0f));
		SetSharedGameObject(wstringKey::obRadarGage, radarGagePtr);
		//エネルギー
		auto scale = 1.5f;
		AddGameObject<EnergyGageBar>(Vec3(256.0f*scale, 128.0f*scale, 1.0f), Vec3(0.0f, 0.0f, 0.0f), Vec3(-500.0f - 256.0f / 2.0f, -360.0f, 0.0f), playerPtr);
	}
	void GameStage6::CreateUISprite() {
	}
	void GameStage6::CreatePlanetUISprite() {
		auto ptrPlanet = AddGameObject<Sprite>(wstringKey::txPlanet_R, true,
			Vec2(64.0f, 64.0f), Vec2(300.0f, -360.0f));
		ptrPlanet->SetDrawLayer(1000);
		ptrPlanet->AddTag(L"SCORE");
	}


	void GameStage6::CreatePlanetCounter() {
		UINT PlanetCount = App::GetApp()->GetScene<Scene>()->GetPlanetCount();
		// スコアカウンター
		UINT CountMAX = App::GetApp()->GetScene<Scene>()->GetPlanetMax();
		AddGameObject<Score>(Vec2(50.0f, 65.0f), Vec2(420.0f, -360.0f), PlanetCount, 2, CountMAX, true);
		AddGameObject<AbcSprite>(wstringKey::txABCW, true, Vec2(80.0f, 80.0f), Vec2(475.0f, -360.0f));
		// スコアの最大値
		AddGameObject<Score>(Vec2(50.0f, 65.0f), Vec2(570.0f, -360.0f), CountMAX, 2, CountMAX, false);
	}

	void GameStage6::CreateParticle() {
		// Bomb
		// パーティクルの準備
		auto bombPtr = AddGameObject<BombParticle>();
		// 共有オブジェクトに登録
		SetSharedGameObject(L"BombParticle", bombPtr);

		// Recovery
		// パーティクルの準備
		auto recoveryPtr = AddGameObject<RecoveryParticle>();
		// 共有オブジェクトに登録
		SetSharedGameObject(L"RecoveryParticle", recoveryPtr);

	}

	void GameStage6::OnCreate() {
		try {
			// スコアのリセット
			App::GetApp()->GetScene<Scene>()->ResetScore();

			//ビューとライトの作成
			CreateViewLight();
			//背景の作成
			CreateBG();
			//テストボックスの作成
			CreateTestBox();
			//テストプレイヤーの作成
			CreateTestPlayer();
			//スピードUIの作成
			CreateUISprite();
			//惑星UIの作成
			CreatePlanetUISprite();
			//数字の作成
			CreatePlanetCounter();
			// パーティクルの作成
			CreateParticle();
			// FadeIn
			AddGameObject<FadeIn>(L"txFade", true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));

		}
		catch (...) {
			throw;
		}
	}

	void GameStage6::OnUpdate() {
		UINT PlanetCount = App::GetApp()->GetScene<Scene>()->GetPlanetCount();
		// 惑星カウンターの更新
		shared_ptr<Score> score = nullptr;
		auto gameObjects = GetGameObjectVec();
		for (auto obj : gameObjects) {
			score = dynamic_pointer_cast<Score>(obj);
			if (score)	break;
		}

		// スコアを加算する
		if (score) {
			score->SetValue(PlanetCount);
		}

		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//リセットコマンド
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_START&&CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_BACK) {
			App::GetApp()->GetScene<Scene>()->ResetScore();
			//ScorePtr1->SetScore(PlanetCount);
			PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToTitleStage");
		}

	}

	void GameStage6::OnDestroy() {
		Stage::OnDestroy();
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		if (m_BGM) {
			XAPtr->Stop(m_BGM);
		}
	}

}
//end basecross
