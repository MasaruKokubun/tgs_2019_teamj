#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	///	タイトルのスプライト
	//--------------------------------------------------------------------------------------
	Sprite::Sprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vec2& StartScale, const Vec2& StartPos) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_Row(0),
		m_Col(0),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_TipSize(Vec2(1.0f, 1.0f)),
		m_DivisionCount(Vec2(1.0f,1.0f))
	{}

	Sprite::Sprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vec2& StartScale, const Vec2& StartPos, 
		const int& Row, const int& Col, const Vec2& TipSize, const Vec2& DivisionCount) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Row(Row),
		m_Col(Col),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_TipSize(TipSize),
		m_DivisionCount(DivisionCount)
	{}

	Sprite::~Sprite() {}
	void Sprite::OnCreate() {
		float tipWidth = m_TipSize.x / (m_TipSize.x * m_DivisionCount.x);
		float tipHeight = m_TipSize.y / (m_TipSize.y * m_DivisionCount.y);
		Vec2 tipSize(tipWidth, tipHeight);
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HalfSize = 0.5f;
		//頂点配列(縦横5個ずつ表示)
		vector<VertexPositionColorTexture> vertices = {
			{VertexPositionColorTexture(Vec3(-HalfSize,  HalfSize, 0), white, Vec2(tipSize.x * (m_Row + 0), tipSize.y * (m_Col + 0)))},
			{VertexPositionColorTexture(Vec3(HalfSize,   HalfSize, 0), white, Vec2(tipSize.x * (m_Row + 1), tipSize.y * (m_Col + 0)))},
			{VertexPositionColorTexture(Vec3(-HalfSize, -HalfSize, 0), white, Vec2(tipSize.x * (m_Row + 0), tipSize.y * (m_Col + 1)))},
			{VertexPositionColorTexture(Vec3(HalfSize,  -HalfSize, 0), white, Vec2(tipSize.x * (m_Row + 1), tipSize.y * (m_Col + 1)))}
		};

		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.3f);

		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(vertices, indices);
		PtrDraw->SetTextureResource(m_TextureKey);
	}

	void Sprite::OnUpdate() {

	}

	//--------------------------------------------------------------------------------------
	//	class ScoreSprite : public GameObject;
	//	用途:　スコア用の数値スプライト
	//--------------------------------------------------------------------------------------
	NumSprite::NumSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vec2& StartScale, const Vec2& StartPos) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos)
	{}

	NumSprite::~NumSprite(){}
	void NumSprite::OnCreate() {
		auto color = Col4(1, 1, 1, 1);
		float HelfSize = 0.5f;
		this->m_Vertices = {
			{ Vec3(-HelfSize,  HelfSize, 0), color, Vec2(-0.0f, -0.0f) },
			{ Vec3(HelfSize,  HelfSize, 0), color, Vec2(1.0f, -0.0f) },
			{ Vec3(-HelfSize, -HelfSize, 0), color, Vec2(-0.0f,  1.0f) },
			{ Vec3(HelfSize, -HelfSize, 0), color, Vec2(1.0f,  1.0f) }
		};
		//インデックス配列
		vector<uint16_t> indices = {
			0, 1, 2,
			1, 3, 2 
		};
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.3f);

		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_Vertices, indices);
		PtrDraw->SetTextureResource(m_TextureKey);
	}
	//--------------------------------------------------------------------------------------
	//	class ScoreSprite : public GameObject;
	//	用途:　スコア用の数値スプライト
	//--------------------------------------------------------------------------------------
	AbcSprite::AbcSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vec2& StartScale, const Vec2& StartPos) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos)
	{
	}

	AbcSprite::~AbcSprite() {}
	void AbcSprite::OnCreate() {
		auto color = Col4(1, 1, 1, 1);
		auto tipSize = Vec2(64.0f / 576.0f, 64.0f / 192.0f);
		m_AbcU = EAlphabet::SLASH;
		m_AbcV = 2;
		float HelfSize = 0.5f;
		// 頂点データ（座標・頂点カラー・UV座標など）
		m_Vertices = {
			{ Vec3(-HelfSize,  HelfSize, 0), color, Vec2(tipSize.x * (m_AbcU + 0), tipSize.y * (m_AbcV + 0))},
			{ Vec3(HelfSize,   HelfSize, 0), color, Vec2(tipSize.x * (m_AbcU + 1), tipSize.y * (m_AbcV + 0))},
			{ Vec3(-HelfSize, -HelfSize, 0), color, Vec2(tipSize.x * (m_AbcU + 0), tipSize.y * (m_AbcV + 1))},
			{ Vec3(HelfSize,  -HelfSize, 0), color, Vec2(tipSize.x * (m_AbcU + 1), tipSize.y * (m_AbcV + 1))}
		};

		//インデックス配列
		vector<uint16_t> indices = {
			0, 1, 2,
			1, 3, 2
		};

		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.3f);

		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_Vertices, indices);
		PtrDraw->SetTextureResource(m_TextureKey);
		SetDrawLayer(3);
	}

	//演出スプライト
	// 構築と破棄
	EffectSprite::EffectSprite(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vec2& StartScale, const Vec2& StartPos) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale * 1.7f),
		m_StartPos(StartPos),
		m_AnimTime(0.03f),
		m_TotalTime(0.0f)
	{
	}
	EffectSprite::~EffectSprite(){}

	// 描画
	void EffectSprite::OnCreate() {
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);

		float tipWidth = 256.0f / (256.0f * 4);
		float tipHeight = 256.0f / (256.0f * 1);
		Vec2 tipSize(tipWidth, tipHeight);
		//for (int c = 0; c < 4; c++) {
		//	for (int r = 0; r < 1; r++) {
		//		m_AnimTip[c][r].left = tipWidth * r;
		//		m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
		//		m_AnimTip[c][r].top = tipHeight * c;
		//		m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
		//	}
		//}

		m_AnimTipRow = 0;
		m_AnimTipCol = 0;
		float HalfSize = 0.5f;
		// 頂点配列
		//m_BackupVertices = {
		//	{Vec3(-HalfSize, +HalfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
		//	{Vec3(+HalfSize, +HalfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
		//	{Vec3(-HalfSize, -HalfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
		//	{Vec3(+HalfSize, -HalfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		//};
		m_BackupVertices = {
			{Vec3(-HalfSize,  HalfSize, 0), white, Vec2(tipSize.x * (m_AnimTipRow + 0), tipSize.y * (m_AnimTipCol + 0))},
			{Vec3(HalfSize,   HalfSize, 0), white, Vec2(tipSize.x * (m_AnimTipRow + 1), tipSize.y * (m_AnimTipCol + 0))},
			{Vec3(-HalfSize, -HalfSize, 0), white, Vec2(tipSize.x * (m_AnimTipRow + 0), tipSize.y * (m_AnimTipCol + 1))},
			{Vec3(HalfSize,  -HalfSize, 0), white, Vec2(tipSize.x * (m_AnimTipRow + 1), tipSize.y * (m_AnimTipCol + 1))}
		};


		vector<uint16_t> indices = {
		0, 1, 2,
		1, 3, 2
		};
		SetAlphaActive(m_Trace);
		//auto PtrTransform = AddComponent<Transform>();
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);

		//頂点とインデックスを指定してスプライト作成
		//auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		auto PtrDraw = this->AddComponent<PCTStaticDraw>();
		PtrDraw->CreateOriginalMesh<VertexPositionColorTexture>(m_BackupVertices, indices);
		PtrDraw->SetOriginalMeshUse(true);
		PtrDraw->SetDepthStencilState(DepthStencilState::None);
		PtrDraw->SetTextureResource(m_TextureKey);
		SetDrawLayer(3);
	}

	void EffectSprite::OnUpdate() {
		float elapsedTime = App::GetApp()->GetElapsedTime();
		m_TotalTime += elapsedTime;
		if (m_TotalTime >= m_AnimTime) {
			m_AnimTipRow++;
			if (m_AnimTipRow >= 4) {
				//m_AnimTipRow = 0;
				GetStage()->RemoveGameObject<GameObject>(GetThis<GameObject>());
			}
			m_TotalTime = 0.0f;
		}
		float tipWidth = 256.0f / (256.0f * 4);
		float tipHeight = 256.0f / (256.0f * 1);
		Vec2 tipSize(tipWidth, tipHeight);

		float HalfSize = 0.5f;
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		//vector<VertexPositionColorTexture> newVertices = {
		//	{Vec3(-HalfSize, +HalfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
		//	{Vec3(+HalfSize, +HalfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
		//	{Vec3(-HalfSize, -HalfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
		//	{Vec3(+HalfSize, -HalfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		//};

		vector<VertexPositionColorTexture> newVertices = {
			{Vec3(-HalfSize,  HalfSize, 0), white, Vec2(tipSize.x * (m_AnimTipRow + 0), tipSize.y * (m_AnimTipCol + 0))},
			{Vec3(HalfSize,   HalfSize, 0), white, Vec2(tipSize.x * (m_AnimTipRow + 1), tipSize.y * (m_AnimTipCol + 0))},
			{Vec3(-HalfSize, -HalfSize, 0), white, Vec2(tipSize.x * (m_AnimTipRow + 0), tipSize.y * (m_AnimTipCol + 1))},
			{Vec3(HalfSize,  -HalfSize, 0), white, Vec2(tipSize.x * (m_AnimTipRow + 1), tipSize.y * (m_AnimTipCol + 1))}
		};

		//vector<VertexPositionColorTexture> newVertices;
		//for (size_t i = 0; i < m_BackupVertices.size(); i++) {
		//	Vec2 uv = m_BackupVertices[i].textureCoordinate;
		//	switch (i) {
		//	case 0:
		//		uv.x = tipSize.x * (m_AnimTipRow * 0);
		//		uv.y = tipSize.y * (m_AnimTipCol * 0);
		//		break;
		//	case 1:
		//		uv.x = tipSize.x * (m_AnimTipRow * 1);
		//		uv.y = tipSize.y * (m_AnimTipCol * 0);
		//		break;
		//	case 2:
		//		uv.x = tipSize.x * (m_AnimTipRow * 0);
		//		uv.y = tipSize.y * (m_AnimTipCol * 1);
		//		break;
		//	case 3:
		//		uv.x = tipSize.x * (m_AnimTipRow * 1);
		//		uv.y = tipSize.y * (m_AnimTipCol * 1);
		//		break;
		//	}
		//	auto v = VertexPositionColorTexture(m_BackupVertices[i].position, m_BackupVertices[i].color, uv);
		//	newVertices.push_back(v);
		//}


		//auto PtrDraw = GetComponent<PCTSpriteDraw>();
		auto PtrDraw = GetComponent<PCTStaticDraw>();
		PtrDraw->UpdateVertices(newVertices);

	}
	//--------------------------------------------------------------------------------------
//	セレクトスプライト
//--------------------------------------------------------------------------------------
	SelectSprite::SelectSprite(const shared_ptr<Stage>& StagePtr,
		const wstring& TextureKey,
		bool Trace,
		const Vec2& StartPos,
		const Vec2& StartScale,
		const Level& level) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_StartPos(StartPos),
		m_StartScale(StartScale),
		m_Level(level)
	{}
	SelectSprite::SelectSprite(const shared_ptr<Stage>& StagePtr,
		const wstring& TextureKey,
		bool Trace,
		const Vec2& StartPos,
		const Vec2& StartScale) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_StartPos(StartPos),
		m_StartScale(StartScale),
		m_Level(Level::none)
	{}
	SelectSprite::~SelectSprite() {	}

	void SelectSprite::OnCreate() {
		float HelfSize = 0.5f;
		//頂点配列(縦横5個ずつ表示)
		m_BackupVertices = {
			{ VertexPositionColorTexture(Vec3(-HelfSize, HelfSize, 0),Col4(1.0f, 1.0f,1.0f, 1.0f), Vec2(0.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vec3(HelfSize, HelfSize, 0), Col4(1.0f, 1.0f,1.0f, 1.0f), Vec2(1.0f, 0.0f)) },
			{ VertexPositionColorTexture(Vec3(-HelfSize, -HelfSize, 0), Col4(1.0f, 1.0f,1.0f, 1.0f), Vec2(0.0f, 1.0f)) },
			{ VertexPositionColorTexture(Vec3(HelfSize, -HelfSize, 0), Col4(1.0f, 1.0f,1.0f, 1.0f), Vec2(1.0f, 1.0f)) },
		};
		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(true);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(m_TextureKey);
	}

	void SelectSprite::OnUpdate() {}

	void SelectSprite::UpdateColor(Col4 Brightness) {
		vector<VertexPositionColorTexture> NewVertices;
		for (size_t i = 0; i < m_BackupVertices.size(); i++) {
			Col4 col(Brightness);
			auto v = VertexPositionColorTexture(
				m_BackupVertices[i].position,
				col,
				m_BackupVertices[i].textureCoordinate
			);
			NewVertices.push_back(v);
		}
		auto PtrDraw = GetComponent<PCTSpriteDraw>();
		PtrDraw->UpdateVertices(NewVertices);
	}

	void SelectSprite::UpdateScale(const Vec2& Scale) {

		vector<VertexPositionColorTexture> NewVertices;
		for (size_t i = 0; i < m_BackupVertices.size(); i++) {
			Vec3 scale(Scale);
			auto v = VertexPositionColorTexture(
				Vec3(m_BackupVertices[i].position.x * scale.x, m_BackupVertices[i].position.y * scale.y, 0.0f),
				m_BackupVertices[i].color,
				m_BackupVertices[i].textureCoordinate
			);
			NewVertices.push_back(v);
		}
		auto PtrDraw = GetComponent<PCTSpriteDraw>();
		PtrDraw->UpdateVertices(NewVertices);

	}

	//--------------------------------------------------------------------------------------
	//	スタートスプライト
	//--------------------------------------------------------------------------------------
	// 構築と破棄
	StartSprite::StartSprite(const shared_ptr<Stage>& StagePtr,
		const wstring& TextureKey,
		const bool& Trace,
		const Vec2& StartScale,
		const Vec2& StartPos)
		:
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_TotalTime(0.0f),
		m_StartFlg(false),
		m_DrawFlg(false)
	{
	}
	StartSprite::~StartSprite(){}

	// 初期化
	void StartSprite::OnCreate()
	{
		AddTag(wstringKey::tagStartSprite);
		float HalfSize = 0.5f;
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		// 頂点配列
		vector<VertexPositionColorTexture> vertices = {
			{VertexPositionColorTexture(Vec3(-HalfSize, HalfSize, 0), white, Vec2(0.0f, 0.0f))},
			{VertexPositionColorTexture(Vec3(HalfSize, HalfSize, 0), white, Vec2(1.0f, 0.0f))},
			{VertexPositionColorTexture(Vec3(-HalfSize, -HalfSize, 0), white, Vec2(0.0f, 1.0f))},
			{VertexPositionColorTexture(Vec3(HalfSize, -HalfSize, 0), white, Vec2(1.0f, 1.0f))}
		};
		// インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		// 透過処理	
		SetAlphaActive(m_Trace);

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);
		// 頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(vertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(m_TextureKey);

		SetDrawLayer(10);
	}

	// 更新
	void StartSprite::OnUpdate()
	{
		// ゲームが始まったらこのスプライトは消す
		if (m_StartFlg)
		{
			GetStage()->RemoveGameObject<GameObject>(GetThis<GameObject>());
		}

		if (m_DrawFlg) {
			SetDrawActive(true);
			auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();

			float elapsedTime = App::GetApp()->GetElapsedTime();
			m_TotalTime += elapsedTime * 3.0f;
			if (m_TotalTime >= XM_2PI)
			{
				m_TotalTime = 0.0f;
			}
			auto PtrDraw = GetComponent<PCTSpriteDraw>();
			Col4 col(1.0f, 1.0f, 1.0f, 1.0f);
			col.w = sin(m_TotalTime) * 0.5f + 0.5f;
			PtrDraw->SetDiffuse(col);

			// Aボタンを入力されたらfadeinを通してゲームスタート
			if (CntlVec[0].bConnected)
			{
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A)
				{
					m_StartFlg = true;
					m_DrawFlg = false;
					// 現在のステージにplayerはいるか
					shared_ptr<Player> player = nullptr;
					for (auto& obj : this->GetStage()->GetGameObjectVec())
					{
						player = dynamic_pointer_cast<Player>(obj);
						if (player)
						{
							break;
						}
					}

					if (player)
					{
						player->GetStateMachine()->ChangeState(PlayerDefault::Instance());
					}
				}
			}
		}
		else {
			SetDrawActive(false);
		}
	}

	// ゲッター
	bool StartSprite::GetStartFlg() const
	{
		return m_StartFlg;
	}

	void StartSprite::SetDrawFlg(bool DrawFlg) 
	{
		m_DrawFlg = DrawFlg;
	}

	//--------------------------------------------------------------------------------------
	// 惑星UI用スプライト
	//--------------------------------------------------------------------------------------
	// 構築と破棄
	PlanetUISprite::PlanetUISprite(const shared_ptr<Stage>& StagePtr,
		const wstring& TextureKey,
		const bool& Trace,
		const Vec2& StartScale,
		const Vec2& StartPos,
		const int& Row,
		const int& Col)
		:
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_AnimTipRow(Row),
		m_AnimTipCol(Col)
	{
	}
	PlanetUISprite::~PlanetUISprite(){}

	// 初期化
	void PlanetUISprite::OnCreate()
	{
		// アニメチップ作成
		float tipWidth = 128.0f / 768.0f;
		float tipHeight = 128.0f / 768.0f;
		for (int col = 0; col < 6; col++) {
			for (int row = 0; row < 6; row++) {
				m_AnimTip[col][row].left = tipWidth * row;
				m_AnimTip[col][row].right = m_AnimTip[col][row].left + tipWidth;
				m_AnimTip[col][row].top = tipHeight * col;
				m_AnimTip[col][row].bottom = m_AnimTip[col][row].top + tipHeight;
			}
		}

		// 描画するテクスチャ設定
		float HalfSize = 0.5f;
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HalfSize, +HalfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HalfSize, +HalfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HalfSize, -HalfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HalfSize, -HalfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };

		// 初期位置等の設定
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.0f);

		// 頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(vertices, indices);
		PtrDraw->SetTextureResource(m_TextureKey);
		// 透明処理
		SetAlphaActive(m_Trace);
	}

	//-------------------------------------------------------------------------------------
	//　チュートリアルテキスト
	//-------------------------------------------------------------------------------------
	TutorialText::TutorialText(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vec2& StartScale, const Vec2& StartPos) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos)
	{}
	TutorialText::~TutorialText() {}

	void TutorialText::OnCreate() {

		float HelfSize = 0.5f;
		//頂点配列(縦横5個ずつ表示)
		vector<VertexPositionColorTexture> vertices = {
			{ VertexPositionColorTexture(Vec3(-HelfSize,  HelfSize, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(-0.0f, -0.0f)) },
			{ VertexPositionColorTexture(Vec3(HelfSize,  HelfSize, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(1.0f, -0.0f)) },
			{ VertexPositionColorTexture(Vec3(-HelfSize, -HelfSize, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(-0.0f,  1.0f)) },
			{ VertexPositionColorTexture(Vec3(HelfSize, -HelfSize, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(1.0f,  1.0f)) },
		};
		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.3f);

		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(vertices, indices);
		PtrDraw->SetTextureResource(m_TextureKey);
	}

	void  TutorialText::OnUpdate()
	{

	}
}