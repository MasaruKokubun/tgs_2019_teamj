#pragma once
#include "stdafx.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	///	スコア表示のスプライト
	//--------------------------------------------------------------------------------------
	class ScoreSprite : public GameObject {
		bool m_Trace;
		Vec2 m_StartScale;
		Vec3 m_StartPos;
		wstring m_TextureKey;
		float m_Score;
		int m_WallCount;
		int m_NowWallCount;
		//桁数
		UINT m_NumberOfDigits;
		//バックアップ頂点データ
		vector<VertexPositionTexture> m_BackupVertices;
	public:
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		@param[in]	StagePtr	ステージ
		@param[in]	NumberOfDigits	桁数
		@param[in]	TextureKey	テクスチャキー
		@param[in]	Trace	透明処理するかどうか
		@param[in]	StartScale	初期スケール
		@param[in]	StartPos	初期位置
		*/
		//--------------------------------------------------------------------------------------
		ScoreSprite(const shared_ptr<Stage>& StagePtr, UINT NumberOfDigits,
			const wstring& TextureKey, bool Trace,
			const Vec2& StartScale, const Vec3& StartPos);
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~ScoreSprite() {}
		//--------------------------------------------------------------------------------------
		/*!
		@brief スコアのセット
		@param[in]	f	値
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		void SetScore(float f) {
			m_Score = f;
		}
		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//--------------------------------------------------------------------------------------
		/*!
		@brief 更新
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnUpdate()override;
		int GetWall() {
			return m_WallCount;
		}
		void SetWall(int value) {
			m_WallCount = value;
		}
	};
//--------------------------------------------------------------------------------------
///	スコア表示のスプライト
//--------------------------------------------------------------------------------------
	class ScoreSprite10 : public GameObject {
		bool m_Trace;
		Vec2 m_StartScale;
		Vec3 m_StartPos;
		wstring m_TextureKey;
		float m_Score;
		int m_WallCount;
		int m_NowWallCount;
		//桁数
		UINT m_NumberOfDigits;
		//バックアップ頂点データ
		vector<VertexPositionTexture> m_BackupVertices;
	public:
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		@param[in]	StagePtr	ステージ
		@param[in]	NumberOfDigits	桁数
		@param[in]	TextureKey	テクスチャキー
		@param[in]	Trace	透明処理するかどうか
		@param[in]	StartScale	初期スケール
		@param[in]	StartPos	初期位置
		*/
		//--------------------------------------------------------------------------------------
		ScoreSprite10(const shared_ptr<Stage>& StagePtr, UINT NumberOfDigits,
			const wstring& TextureKey, bool Trace,
			const Vec2& StartScale, const Vec3& StartPos);
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~ScoreSprite10() {}
		//--------------------------------------------------------------------------------------
		/*!
		@brief スコアのセット
		@param[in]	f	値
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		void SetScore(float f) {
			m_Score = f;
		}
		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//--------------------------------------------------------------------------------------
		/*!
		@brief 更新
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnUpdate()override;
		int GetWall() {
			return m_WallCount;
		}
		void SetWall(int value) {
			m_WallCount = value;
		}
	};

	//--------------------------------------------------------------------------------------
	///	スコア表示
	//--------------------------------------------------------------------------------------
	class Score : public GameObject
	{
	private:
		Vec2 m_StartScale;
		Vec2 m_StartPosition;
		UINT m_Value;
		UINT m_Digit;
		UINT m_MaxValue;
		bool m_Updateflg;
		vector<shared_ptr<NumSprite>> m_sprits;

	public:
		// スコアカウンター
		Score(const shared_ptr<Stage>& stage, const Vec2& startScalePtr, const Vec2& startPosPtr, const UINT& valuePtr,
			const UINT& digitPtr, const UINT& maxvaluePtr, bool& UpdateflgPtr) :
			GameObject(stage),
			m_StartScale(startScalePtr),
			m_StartPosition(startPosPtr),
			m_Value(valuePtr),
			m_Digit(digitPtr),
			m_MaxValue(maxvaluePtr),
			m_Updateflg(UpdateflgPtr)
		{
		}

		virtual ~Score()
		{
		}

		virtual void OnCreate() override;
		virtual void OnUpdate2() override;
		virtual void OnDraw() override;

		// スコアのリセット
		void ResetScore() {
			m_Value = 0;
			m_MaxValue = 0;
		}

		void AddValue(const int& value)
		{
			m_Value += value;
		}

		void SetValue(const UINT& value)
		{
			if (value > m_MaxValue) {
				m_Value = m_MaxValue;
			}
			else
			{
				m_Value = value;
			}
		}

		void UpdateValue()
		{
			// スコアの表示
			UINT value = m_Value;
			UINT ans = 1, i = m_Digit, displayDigit = 0;
			while (i != 0) {
				ans = ans * 10;
				if (m_Value >= ans) {
					displayDigit++;
				}
				--i;
			}
			if (m_Value > m_MaxValue) {
				m_Value = m_MaxValue;
				value = m_Value;
			}
			unsigned int j = 0;
			for (auto& sprite : m_sprits) {
				int a = value % 10;
				value /= 10;

				sprite->SetRect(Rect2D<float>(64.0f * a, 0.0f, 64.0f * (a + 1.0f), 128.0f));
				auto drawComp = sprite->GetComponent<PCTSpriteDraw>();
				if (j > displayDigit) {
					drawComp->SetDrawActive(false);
				}
				else {
					drawComp->SetDrawActive(true);
				}
				j++;
			}
		}
	};
}
//end basecross