/*!
@file CopyLightStage.cpp
@brief コピーライトステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	コピーライトステージクラス実体
	//--------------------------------------------------------------------------------------
		// ビューの作成
	void CopyLightStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		// ビューのカメラの設定
		auto PtrCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrCamera);
		PtrCamera->SetEye(Vec3(0.0f, 5.0f, -5.0f));
		PtrCamera->SetAt(Vec3(0.0f, 0.0f, 0.0f));
		// マルチライトの作成
		auto PtrMultiLight = CreateLight<MultiLight>();
		// デフォルトのライティングを設定
		PtrMultiLight->SetDefaultLighting();
	}

		// スプライトの作成
	void CopyLightStage::CreateCopyLightSprite() {
		auto BGPtr = AddGameObject<Sprite>(L"txFade", true,
			Vec2(1280.0f, 800.0f), Vec2(0.0f, 0.0f));
		BGPtr->SetDrawLayer(-1);
		auto CopyLightPtr = AddGameObject<Sprite>(wstringKey::txCopyLight, true,
			Vec2(1280.0f, 800.0f), Vec2(0.0f, 0.0f));
			CopyLightPtr->SetDrawLayer(1);
			CopyLightPtr->AddTag(L"CL");
	}
		// 初期化
	void CopyLightStage::OnCreate()
	{
		// ビューの作成
		CreateViewLight();
		
		// スプライトの作成
		CreateCopyLightSprite();
	

	}

		// 更新
	void CopyLightStage::OnUpdate()
	{
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		m_Count += ElapsedTime * 5.0f;
		if (m_Count > 15.0f) {
			m_Time += ElapsedTime * 1.0f;
			for (auto& v : GetGameObjectVec()) {
				if (v->FindTag(L"CL")) {
					if (m_Alpha > 0.0f) {
						m_Alpha -= sin(m_Time) * 0.01f;
						v->GetComponent<PCTSpriteDraw>()->SetDiffuse(Col4(1.0f, 1.0f, 1.0f, m_Alpha));
						if (m_Alpha < 0.0f) {
							PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToTitleStage");
						}

					}
				}

			}
		}
	}
}
//end basecross
