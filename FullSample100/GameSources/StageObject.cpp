#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	class TestStageObject : public GameObject;
	//	用途: テストステージブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TestStageObject::TestStageObject(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, int col , int row) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_ChildrenAnimCol(col),
		m_ChildrenAnimRow(row)
	{
	}
	//初期化
	void TestStageObject::OnCreate() {

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		//CollisionSphere衝突判定を付ける
		auto ptrColl = AddComponent<CollisionSphere>();
		ptrColl->SetAfterCollision(AfterCollision::None);
		ptrColl->SetSleepActive(true);
		ptrColl->SetSleepTime(0.5f);
		//タグをつける
		AddTag(wstringKey::tagAbsorptionObject);

		//アニメチップ
		float tipWidth = 1.0f / 2.0f;
		float tipHeight = 1.0f;
		for (int c = 0; c < 1; c++) {
			for (int r = 0; r < 2; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 0;
		m_AnimTipCol = 0;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);
		PtrSpriteDraw->SetTextureResource(wstringKey::txWave1);
		//透明処理
		SetAlphaActive(true);

		//shared_ptr<TestStageObject> a = GetComponent<TestStageObject>();
		m_CorePtr = GetStage()->AddGameObject<TestStageObjectChildren>(m_Scale*0.4f, m_Rotation, m_Position, GetThis<GameObject>(), m_ChildrenAnimCol, m_ChildrenAnimRow)->GetThis<GameObject>();

		////文字列をつける
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		auto PlanetMax = App::GetApp()->GetScene<Scene>()->GetPlanetMax();
		PlanetMax++;
		App::GetApp()->GetScene<Scene>()->SetPlanetMax(PlanetMax);
	}

	//更新
	void TestStageObject::OnUpdate() {
		auto core = dynamic_pointer_cast<TestStageObjectChildren>(m_CorePtr);
		if (core->m_Gage >= 1.0f&&m_AnimTipRow==0) {
			m_AnimTipRow = 1;
			float HelfSize = 0.5f;
			Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
			auto animTip = m_AnimTip[m_AnimTipCol][m_AnimTipRow];
			vector<VertexPositionColorTexture> vertices = {
				{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(animTip.left, animTip.top) },
				{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(animTip.right, animTip.top) },
				{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(animTip.left, animTip.bottom) },
				{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(animTip.right, animTip.bottom) }
			};
			auto drawComp = GetComponent<PCTStaticDraw>();
			drawComp->UpdateVertices<VertexPositionColorTexture>(vertices);
		}
	}
	void TestStageObject::OnUpdate2() {
		////文字列の表示
		//DrawStrings();
	}

	//文字列の表示
	void TestStageObject::DrawStrings() {

		//文字列表示
		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring fpsStr(L"FPS: ");
		fpsStr += Util::UintToWStr(fps);
		fpsStr += L"\nElapsedTime: ";
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		fpsStr += Util::FloatToWStr(ElapsedTime);
		fpsStr += L"\n";

		auto pos = GetComponent<Transform>()->GetPosition();
		wstring positionStr(L"Position:\t");
		positionStr += L"X=" + Util::FloatToWStr(pos.x, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Y=" + Util::FloatToWStr(pos.y, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Z=" + Util::FloatToWStr(pos.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring speadStr(L"Spead:\t");
		//auto gravVelocity = GetComponent<Gravity>()->GetGravityVelocity();
		//speadStr += Util::FloatToWStr(m_Spead*100.0f, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Y=" + Util::FloatToWStr(gravVelocity.y, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Z=" + Util::FloatToWStr(gravVelocity.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring str = fpsStr + positionStr + speadStr;
		////文字列コンポーネントの取得
		//auto ptrString = GetComponent<StringSprite>();
		//ptrString->SetText(str);
	}

	//--------------------------------------------------------------------------------------
	//	class TestStageObjectChildren : public GameObject;
	//	用途: テストステージブジェクトチルドレン
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TestStageObjectChildren::TestStageObjectChildren(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, const shared_ptr<GameObject>& parentPtr, int col, int row ) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_ParentPtr(parentPtr),
		m_AnimTipCol(col),
		m_AnimTipRow(row)
	{
	}

	//初期化
	void TestStageObjectChildren::OnCreate() {
		auto ptrTransform = GetComponent<Transform>();
		//親子関係化
		ptrTransform->SetParent(m_ParentPtr);
		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(Vec3(0.0f,0.0f,0.0f));
		ptr->SetPosition(Vec3(0.0f, 0.0f, 0.0f));

		m_Gage = 0.0f;
		//CollisionSphere衝突判定を付ける
		auto ptrColl = AddComponent<CollisionSphere>();	
		ptrColl->SetAfterCollision(AfterCollision::None);
		ptrColl->SetSleepActive(true);
		ptrColl->SetSleepTime(0.5f);
		//タグをつける
		AddTag(wstringKey::tagAbsorptionCoreObject);

		//アニメチップ作成
		float tipWidth = 128.0f / 768.0f;
		float tipHeight = 128.0f / 768.0f;
		for (int c = 0; c < 6; c++) {
			for (int r = 0; r < 6; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		//m_AnimTipRow = 1;
		//m_AnimTipCol = 0;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		float maxSize = 1.0f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);
		PtrSpriteDraw->SetTextureResource(wstringKey::txPlanets);
		//PtrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		//透明処理
		SetAlphaActive(true);
		//レイヤー
		SetDrawLayer(2);
		////文字列をつける
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
	}

	//更新
	void TestStageObjectChildren::OnUpdate() {
		auto XAPtr = App::GetApp()->GetXAudio2Manager();
		if (m_IntervalFlg) {
			m_IntervalTime += App::GetApp()->GetElapsedTime();
			if (m_IntervalTime >= 0.5f) {
				m_IntervalFlg = false;
				m_IntervalTime = 0.0f;
				m_HitFlg = false;
			}
		}
		UINT planetCount = App::GetApp()->GetScene<Scene>()->GetPlanetCount();
		if (m_Gage >= 1.0f) {
			m_Gage = 1.0f;
			if (m_CountFlg) {
				XAPtr->Start(wstringKey::seComplete, 0, 0.5f);
				m_CountFlg = false;
				planetCount++;
				// パーティクル
				auto PtrPos = GetComponent<Transform>()->GetWorldPosition();
				auto PtrScale = GetComponent<Transform>()->GetScale();
				
				//auto surveyPtr = GetStage()->GetSharedGameObject<SurveyCompParticle>(L"SurveyCompParticle");
				//if (surveyPtr) {
				//	surveyPtr->InsertSurveyComp(this->GetComponent<Transform>()->GetWorldPosition());
				//}

				GetStage()->AddGameObject<EffectSprite>(L"txEffSurveyComp", true, Vec2(PtrScale.x, PtrScale.y), Vec2(PtrPos.x, PtrPos.y));

				App::GetApp()->GetScene<Scene>()->SetPlanetCount(planetCount);
			}
		}
	}
	void TestStageObjectChildren::OnUpdate2() {
		////文字列の表示
		//DrawStrings();
	}

	//文字列の表示
	void TestStageObjectChildren::DrawStrings() {

		//文字列表示
		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring fpsStr(L"FPS: ");
		fpsStr += Util::UintToWStr(fps);
		fpsStr += L"\nElapsedTime: ";
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		fpsStr += Util::FloatToWStr(ElapsedTime);
		fpsStr += L"\n";

		auto pos = GetComponent<Transform>()->GetPosition();
		wstring positionStr(L"Position:\t");
		positionStr += L"X=" + Util::FloatToWStr(pos.x, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Y=" + Util::FloatToWStr(pos.y, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Z=" + Util::FloatToWStr(pos.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring speadStr(L"Spead:\t");
		//auto gravVelocity = GetComponent<Gravity>()->GetGravityVelocity();
		//speadStr += Util::FloatToWStr(m_Spead*100.0f, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Y=" + Util::FloatToWStr(gravVelocity.y, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Z=" + Util::FloatToWStr(gravVelocity.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring str = fpsStr + positionStr + speadStr;
		////文字列コンポーネントの取得
		//auto ptrString = GetComponent<StringSprite>();
		//ptrString->SetText(str);
	}

	//--------------------------------------------------------------------------------------
	//	class BreakableObject : public GameObject;
	//	用途: 破壊可能なステージブジェクト
	//--------------------------------------------------------------------------------------
	BreakableObject::BreakableObject(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_Destroyflg(false)
	{
	}
	//初期化
	void BreakableObject::OnCreate() {

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		//CollisionSphere衝突判定を付ける
		auto ptrColl = AddComponent<CollisionSphere>();
		ptrColl->SetAfterCollision(AfterCollision::None);
		ptrColl->SetSleepActive(true);
		//タグをつける
		AddTag(wstringKey::tagBreakableObject);

		//アニメチップ作成
		float tipWidth = 192.0f / 576.0f;
		float tipHeight = 192.0f / 576.0f;
		for (int c = 0; c < 3; c++) {
			for (int r = 0; r < 3; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 0;
		m_AnimTipCol = 0;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		float maxSize = 1.0f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);
		PtrSpriteDraw->SetTextureResource(wstringKey::txDebris);
		//透明処理
		SetAlphaActive(true);
		//レイヤー
		SetDrawLayer(2);
	}

	//////衝突判定
	//void BreakableObject::OnCollisionEnter(shared_ptr<GameObject>& other) {
	//	//auto& v = other;
	//	//if (v->FindTag(wstringKey::tagPlayerBullet)) {
	//	//	// パーティクル
	//	//	auto PtrPos = GetComponent<Transform>()->GetPosition();
	//	//	//this->GetStage()->AddGameObject<EffectSprite>(L"txEffBomb", true, Vec2(256.0f, 256.0f), Vec2(PtrPos.x, PtrPos.y));
	//	//	auto bombPtr = GetStage()->GetSharedGameObject<BombParticle>(L"BombParticle");
	//	//	if (bombPtr) {
	//	//		bombPtr->InsertBomb(PtrPos, L"txEffBomb");
	//	//	}

	//	//	auto XAPtr = App::GetApp()->GetXAudio2Manager();
	//	//	XAPtr->Start(L"BreakDebris_SE", 0, 0.7f);

	//	//	// プレイヤーの弾だったら破壊する
	//	//	this->m_Destroyflg = true;
	//	//	this->SetDrawActive(false);
	//	//}
	//	//if (v->FindTag(wstringKey::tagPlayer)) {
	//	//	auto player = dynamic_pointer_cast<Player>(v);
	//	//	player->Damage(0.05f);
	//	//	GetStage()->RemoveGameObject<GameObject>(GetThis<GameObject>());
	//	//}
	//}

	//--------------------------------------------------------------------------------------
	//	class TestItem : public GameObject;
	//	用途: テストアイテム
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TestItem::TestItem(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position)
	{
	}
	//初期化
	void TestItem::OnCreate() {

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		//CollisionSphere衝突判定を付ける
		auto ptrColl = AddComponent<CollisionSphere>();
		ptrColl->SetAfterCollision(AfterCollision::None);
		ptrColl->SetSleepActive(true);
		//タグをつける
		AddTag(wstringKey::tagItem);

		//アニメチップ作成
		float tipWidth = 1.0f;
		float tipHeight = 1.0f;
		for (int c = 0; c < 1; c++) {
			for (int r = 0; r < 1; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 0;
		m_AnimTipCol = 0;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		float maxSize = 1.0f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);
		PtrSpriteDraw->SetTextureResource(wstringKey::txItem);
		//透明処理
		SetAlphaActive(true);
		//レイヤー
		SetDrawLayer(2);

		////文字列をつける
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
	}


	//更新
	void TestItem::OnUpdate() {

	}
	void TestItem::OnUpdate2() {
		////文字列の表示
		//DrawStrings();
	}

	//文字列の表示
	void TestItem::DrawStrings() {

		//文字列表示
		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring fpsStr(L"FPS: ");
		fpsStr += Util::UintToWStr(fps);
		fpsStr += L"\nElapsedTime: ";
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		fpsStr += Util::FloatToWStr(ElapsedTime);
		fpsStr += L"\n";

		auto pos = GetComponent<Transform>()->GetPosition();
		wstring positionStr(L"Position:\t");
		positionStr += L"X=" + Util::FloatToWStr(pos.x, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Y=" + Util::FloatToWStr(pos.y, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Z=" + Util::FloatToWStr(pos.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring speadStr(L"Spead:\t");
		//auto gravVelocity = GetComponent<Gravity>()->GetGravityVelocity();
		//speadStr += Util::FloatToWStr(m_Spead*100.0f, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Y=" + Util::FloatToWStr(gravVelocity.y, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Z=" + Util::FloatToWStr(gravVelocity.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring str = fpsStr + positionStr + speadStr;
		////文字列コンポーネントの取得
		//auto ptrString = GetComponent<StringSprite>();
		//ptrString->SetText(str);
	}

	//--------------------------------------------------------------------------------------
	//	class TestItemChildren : public GameObject;
	//	用途: テストアイテムチルドレン
	//--------------------------------------------------------------------------------------
	//構築と破棄
	TestItemChildren::TestItemChildren(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position)

	{
	}
	//初期化
	void TestItemChildren::OnCreate() {

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		//CollisionSphere衝突判定を付ける
		auto ptrColl = AddComponent<CollisionSphere>();
		ptrColl->SetAfterCollision(AfterCollision::None);
		//タグをつける
		AddTag(wstringKey::tagAbsorptionObject);

		////影をつける（シャドウマップを描画する）
		//auto shadowPtr = AddComponent<Shadowmap>();
		////影の形（メッシュ）を設定
		//shadowPtr->SetMeshResource(wstringKey::meshSphere);

		//描画コンポーネントの設定
		auto ptrDraw = AddComponent<BcPNTStaticDraw>();
		//描画するメッシュを設定
		ptrDraw->SetMeshResource(wstringKey::meshSphere);
		//ptrDraw->SetFogEnabled(true);
		////描画するテクスチャを設定
		//ptrDraw->SetTextureResource(wstringKey::txTrace);
		//SetAlphaActive(true);

		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		vector<VertexPositionColorTexture> vertices = {
			{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(0.0f,0.0f) },
			{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(1.0f, 0.0f) },
			{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(0.0f ,1.0f) },
			{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(1.0f, 1.0f) }
		};
		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);

		//auto PtrSpriteDraw = AddComponent<PCTStaticDraw>();
		//PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		//PtrSpriteDraw->SetOriginalMeshUse(true);
		PtrSpriteDraw->SetTextureResource(wstringKey::txTestSpiral2);

		//透明処理
		SetAlphaActive(true);

		////文字列をつける
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
	}

	////衝突判定
	//衝突したら
	void TestItemChildren::OnCollisionEnter(shared_ptr<GameObject>& other) {
		auto& v = other;
		if (v->FindTag(wstringKey::tagPlayer)) {
			auto player = dynamic_pointer_cast<Player>(v);
			player->GetStateMachine()->ChangeState(PlayerAcceleration::Instance());
			auto playerPos = player->GetComponent<Transform>()->GetPosition();

			player->m_Spead = 0.0f;
			player->m_MaxSpead = 0.0f;
			//playerPos = GetComponent<Transform>()->GetPosition();
			//player->GetComponent<Transform>()->SetPosition(playerPos);
		}
	}
	//衝突し続けてるとき
	void TestItemChildren::OnCollisionExcute(shared_ptr<GameObject>& other) {
	}
	//衝突し終わったら
	void TestItemChildren::OnCollisionExit(shared_ptr<GameObject>& other) {
	}

	//更新
	void TestItemChildren::OnUpdate() {

	}
	void TestItemChildren::OnUpdate2() {
		////文字列の表示
		//DrawStrings();
	}

	//文字列の表示
	void TestItemChildren::DrawStrings() {

		//文字列表示
		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring fpsStr(L"FPS: ");
		fpsStr += Util::UintToWStr(fps);
		fpsStr += L"\nElapsedTime: ";
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		fpsStr += Util::FloatToWStr(ElapsedTime);
		fpsStr += L"\n";

		auto pos = GetComponent<Transform>()->GetPosition();
		wstring positionStr(L"Position:\t");
		positionStr += L"X=" + Util::FloatToWStr(pos.x, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Y=" + Util::FloatToWStr(pos.y, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Z=" + Util::FloatToWStr(pos.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring speadStr(L"Spead:\t");
		//auto gravVelocity = GetComponent<Gravity>()->GetGravityVelocity();
		//speadStr += Util::FloatToWStr(m_Spead*100.0f, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Y=" + Util::FloatToWStr(gravVelocity.y, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Z=" + Util::FloatToWStr(gravVelocity.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring str = fpsStr + positionStr + speadStr;
		//文字列コンポーネントの取得
		auto ptrString = GetComponent<StringSprite>();
		ptrString->SetText(str);
	}

}