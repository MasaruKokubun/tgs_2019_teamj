/*!
@file SelectStage.h
@brief ゲームセレクトステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	//	セレクトマネージャー
	//--------------------------------------------------------------------------------------
	class SelectManager : public GameObject {
		shared_ptr<SoundItem> m_BGM;
		float m_vol = 0.1f;
	private:
		Level m_level;	// 今選択しているレベル
		float m_TotalTime;	// 選択する時に使う
		float m_ScaleTime;	// スケールを変更する用
		int m_x;
		int m_y;
		bool m_lr = false;
		vector<vector<Level>> m_StageManager;	// 選択する時に使う
		bool m_fadeFlag = false;
		bool m_fadeoutFlag = true;

	public:
		// 構築と破壊
		SelectManager(const shared_ptr<Stage>& StagePtr);
		virtual ~SelectManager();
		//初期化
		virtual void OnCreate()override;
		// 更新
		virtual void OnUpdate()override;
		//// 後更新
		//virtual void OnUpdate2()override;
		//// 文字列の表示
		//void DrawStrings();
		//セッターとゲッター
		bool GetFadeFlag() { return m_fadeFlag; };
		void SetFadeFlag(bool flag) { m_fadeFlag = flag; };
		bool GetFadeoutFlag() { return m_fadeoutFlag; };
		void SetFadeoutFlag(bool flag) { m_fadeoutFlag = flag; };
		// レベルの取得
		Level GetLevel() const { return m_level; }
	};
//--------------------------------------------------------------------------------------
/// カーソルセレクトマネージャー
//--------------------------------------------------------------------------------------
	class SelectManager_C : public GameObject {
	private:
		float m_TotalTime;	// 選択する時に使う
		float m_ScaleTime;	// スケールを変更する用
		int m_x;
		int m_y;
		//現在のカーソルの位置
		int m_position = 1;
		bool m_Active = true;
	public:
		// 構築と破壊
		SelectManager_C(const shared_ptr<Stage>& StagePtr);
		virtual ~SelectManager_C();
		//初期化
		virtual void OnCreate()override {}
		// 更新
		virtual void OnUpdate()override;
		// 後更新
		virtual void OnUpdate2()override {}
		// 文字列の表示
		void DrawStrings() {}
		//ゲッター
		int GetCursorPosition() { return m_position; };
		//セッター
		void SetCursorPosition(int num) { m_position = num; };
	};

	//--------------------------------------------------------------------------------------
	//	セレクトステージクラス
	//--------------------------------------------------------------------------------------
	class SelectStage : public Stage {
		float m_Time = 0.0f;
		float m_Alpha = 0.0f;
		// ビューの作成
		void CreateViewLight();
		// スプライトの作成
		void CreateSelectSprite();
		//カーソルの生成
		void CreateCursor();
		// UIの生成
		void CreatePlanetUI();
		// 入力ハンドラー
		//InputHandler<SelectStage> m_InputHandler;
		// シーン遷移
		void OnSceneTransition(const wstring& wstringKey);

	public:
		// 構築と破棄
		SelectStage() : Stage() {}
		virtual ~SelectStage() {}
		//初期化
		virtual void OnCreate() override;
		// 更新
		virtual void OnUpdate() override;
		// Aボタン
		//void OnPushA();
	};

}
//end basecross

