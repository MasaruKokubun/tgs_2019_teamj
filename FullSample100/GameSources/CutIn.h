#pragma once
#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	///	タイトルのスプライト
	//--------------------------------------------------------------------------------------
	class CutIn : public Sprite {
		Vec3 m_Pos;
		float m_CutInSpeed;
	public:
		float m_Delta;
		bool m_CutInFlg;
		CutIn(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,const Vec2& StartScale, const Vec3& StartPos);
		virtual ~CutIn();
		void OnCreate()override;
		void OnUpdate()override;
	};
}