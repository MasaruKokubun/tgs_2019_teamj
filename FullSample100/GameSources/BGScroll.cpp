#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	///	背景スクロール
	//--------------------------------------------------------------------------------------
	BGScroll::BGScroll(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vec2& StartScale, const Vec2& StartPos) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_Angle(Vec2(0.0f, 0.0f)),
		m_Rect(Rect((size_t)0.0f, (size_t)0.0f, (size_t)50.0f, (size_t)15.0f))
	{}

	BGScroll::BGScroll(const shared_ptr<Stage>& StagePtr, const wstring& TextureKey, bool Trace,
		const Vec2& StartScale, const Vec2& StartPos, const Rect& Rect) :
		GameObject(StagePtr),
		m_TextureKey(TextureKey),
		m_Trace(Trace),
		m_StartScale(StartScale),
		m_StartPos(StartPos),
		m_Angle(Vec2(0.0f, 0.0f)),
		m_Rect(Rect)
	{}


	BGScroll::~BGScroll() {}
	void BGScroll::OnCreate() {
		float HelfSize = 0.5f;
		//頂点配列(縦横5個ずつ表示)
		m_BackupVertices = {
			{ VertexPositionColorTexture(Vec3(-HelfSize,  HelfSize, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(-0.0f / m_StartScale.x, -0.0f / m_StartScale.y)) },
			{ VertexPositionColorTexture(Vec3(HelfSize,  HelfSize, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(m_StartScale.x / m_StartScale.x, -0.0f / m_StartScale.y)) },
			{ VertexPositionColorTexture(Vec3(-HelfSize, -HelfSize, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(-0.0f / m_StartScale.x,  m_StartScale.y / m_StartScale.y)) },
			{ VertexPositionColorTexture(Vec3(HelfSize, -HelfSize, 0), Col4(1.0f, 1.0f, 1.0f, 1.0f), Vec2(m_StartScale.x / m_StartScale.x,  m_StartScale.y / m_StartScale.y)) },
		};
		//インデックス配列
		vector<uint16_t> indices = { 0, 1, 2, 1, 3, 2 };
		SetAlphaActive(m_Trace);
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_StartScale.x, m_StartScale.y, 1.0f);
		PtrTransform->SetRotation(0, 0, 0);
		PtrTransform->SetPosition(m_StartPos.x, m_StartPos.y, 0.3f);

		//頂点とインデックスを指定してスプライト作成
		auto PtrDraw = AddComponent<PCTSpriteDraw>(m_BackupVertices, indices);
		PtrDraw->SetSamplerState(SamplerState::LinearWrap);
		PtrDraw->SetTextureResource(m_TextureKey);
	}

	void BGScroll::OnUpdate() {
		auto elapsedTime = App::GetApp()->GetElapsedTime();

		shared_ptr<Player> playerPtr = nullptr;
		auto objs = this->GetStage()->GetGameObjectVec();
		for (auto& obj : objs) {
			playerPtr = dynamic_pointer_cast<Player>(obj);
			if (playerPtr) {
				break;
			}
		}
		SetAngle(playerPtr->m_Angle.x, -playerPtr->m_Angle.y);
		auto playerPos = playerPtr->GetComponent<Transform>()->GetPosition();
		auto playerHalfScale = playerPtr->GetComponent<Transform>()->GetScale() * 0.5f;
		// 0.05は誤差の分
		auto halfScale = 1.0f * 0.55f;

		vector<VertexPositionColorTexture> newVertices;
		for (size_t i = 0; i < m_BackupVertices.size(); i++) {
			Vec2 uv = m_BackupVertices[i].textureCoordinate;

			if ((playerPos.x >= (m_Rect.x + halfScale + playerHalfScale.x) && playerPos.x <= (m_Rect.w - halfScale - playerHalfScale.x))) {
				uv.x += playerPtr->m_MaxSpead * playerPtr->m_Spead * m_Angle.x * elapsedTime / 19.2f;
			}
			if ((playerPos.y >= (m_Rect.y + halfScale + playerHalfScale.y) && playerPos.y <= (m_Rect.h - halfScale - playerHalfScale.y))) {
				uv.y += playerPtr->m_MaxSpead * playerPtr->m_Spead * m_Angle.y * elapsedTime / 10.8f;
			}


			auto v = VertexPositionColorTexture(m_BackupVertices[i].position, m_BackupVertices[i].color, uv);
			newVertices.push_back(v);
		}

		auto ptrDraw = GetComponent<PCTSpriteDraw>();
		ptrDraw->UpdateVertices(newVertices);
		m_BackupVertices = newVertices;
	}

	// セッターとゲッター
	void BGScroll::SetAngle(Vec2 Angle) {
		m_Angle = Angle;
	}

	void BGScroll::SetAngle(float x, float y) {
		m_Angle.x = x;
		m_Angle.y = y;
	}

	Rect BGScroll::GetRect() const
	{
		return m_Rect;
	}
}