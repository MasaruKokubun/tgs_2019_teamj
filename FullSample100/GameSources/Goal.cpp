/*!
@file Goal.cpp
@brief ゴール実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross{
	//--------------------------------------------------------------------------------------
	/// class GoalObject : public GameObject
	//--------------------------------------------------------------------------------------
	// 構築と破棄
	GoalObject::GoalObject(const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Rotation,
		const Vec3& Position,
		const shared_ptr<GameObject>& parentPtr
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_ParentPtr(parentPtr),
		m_GoalFlg(false),
		m_GoalFadeFlg(false)
	{
	}

	GoalObject::GoalObject(
		const shared_ptr<Stage>& StagePtr,
		const Vec3& Scale,
		const Vec3& Rotation,
		const Vec3& Position,
		const shared_ptr<GameObject>& parentPtr,
		const Vec2& TextScalePtr,
		const Vec2& TextPositionPtr,
		const float& TextSpeedPtr
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position),
		m_ParentPtr(parentPtr),
		m_GoalFlg(false),
		m_TextScale(TextScalePtr),
		m_TextPosition(TextPositionPtr),
		m_TextSpeed(TextSpeedPtr)
	{}

	GoalObject::~GoalObject() {}

	// 初期化
	void GoalObject::OnCreate()
	{
		auto ptrTransform = GetComponent<Transform>();
		ptrTransform->SetScale(m_Scale);
		ptrTransform->SetRotation(m_Rotation);
		ptrTransform->SetPosition(m_Position);
		//親子関係化
		ptrTransform->SetParent(m_ParentPtr);

		// CollisionObb衝突判定
		auto ptrColl = AddComponent<CollisionSphere>();
		ptrColl->SetAfterCollision(AfterCollision::None);
		// タグをつける(まだちゃんとした名前つけてません)
		AddTag(L"Goal");

		//アニメチップ
		float tipWidth = 1;
		float tipHeight = 1;
		for (int c = 0; c < 1; c++) {
			for (int r = 0; r < 1; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 0;
		m_AnimTipCol = 0;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.75f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);
		PtrSpriteDraw->SetTextureResource(wstringKey::txGoal);
		PtrSpriteDraw->SetDepthStencilState(DepthStencilState::None);
		//透明処理
		SetAlphaActive(true);
		SetDrawLayer(2);

	}

	// 更新
	void GoalObject::OnUpdate() {
		auto elapsedTime = App::GetApp()->GetElapsedTime();
		if (m_GoalFlg) {
			// 背景スクロールを止める
			for (auto obj : GetStage()->GetGameObjectVec())
			{
				auto bgScroll = dynamic_pointer_cast<BGScroll>(obj);
				if (bgScroll)
				{
					obj->SetUpdateActive(false);
					break;
				}
			}
			// ゲームクリアテキスト
			auto goalTextPtr = this->GetStage()->AddGameObject<Sprite>(wstringKey::txGoalText, true,Vec2(m_TextScale), Vec2(m_TextPosition));
			goalTextPtr->AddTag(L"clear");
			goalTextPtr->SetDrawLayer(1001);
			m_GoalFlg =false;
		}
		if (m_TextPosition.x <= -1000.0f) {
			// フェードアウト
			this->GetStage()->AddGameObject<FadeOut>(L"txFade", true, Vec2(1920.0f, 1080.0f), Vec2(0.0f, 0.0f));
		}
		shared_ptr<FadeOut> fadeout = nullptr;
		shared_ptr<Sprite> sprite = nullptr;
		for (auto& obj : this->GetStage()->GetGameObjectVec()) {
			fadeout = dynamic_pointer_cast<FadeOut>(obj);
			if (m_GoalFadeFlg) {
				if (fadeout) {
					if (fadeout->GetAlpha() >= 1.0f) {
						// リザルト画面へシーン遷移
						m_GoalFadeFlg = false;
						PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToResultStage");
					}
				}
			}
			sprite = dynamic_pointer_cast<Sprite>(obj);
			if (sprite) {
				if (sprite->FindTag(L"clear")) {
					auto transComp = sprite->GetComponent<Transform>();
					m_TextPosition = Vec2(transComp->GetPosition().x - m_TextSpeed * elapsedTime, transComp->GetPosition().y);
					transComp->SetPosition(Vec3(m_TextPosition.x, m_TextPosition.y, 0.0f));
				}
			}
		}

	}

	// 衝突した瞬間
	void GoalObject::OnCollisionEnter(shared_ptr<GameObject>& Other) {
		auto v = Other;
		if (v->FindTag(wstringKey::tagPlayer)) {
			// リザルト画面へシーン遷移
			auto player = dynamic_pointer_cast<Player>(v);
			player->SetGoalHitFlg(true);
			m_GoalFlg = true;
			m_GoalFadeFlg = true;
			//PostEvent(0.0f, GetThis<ObjectInterface>(), App::GetApp()->GetScene<Scene>(), L"ToResultStage");
		}
	}

	//--------------------------------------------------------------------------------------
	//	class GoalArea : public GameObject;
	//	用途: ゴールエリア
	//--------------------------------------------------------------------------------------
	//構築と破棄
	GoalArea::GoalArea(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position)
	{
	}
	//初期化
	void GoalArea::OnCreate() {

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);

		//CollisionSphere衝突判定を付ける
		auto ptrColl = AddComponent<CollisionSphere>();
		ptrColl->SetAfterCollision(AfterCollision::None);
		//タグをつける
		AddTag(wstringKey::tagGoalArea);

		//アニメチップ
		float tipWidth = 1.0f / 2.0f;
		float tipHeight = 1.0f;
		for (int c = 0; c < 1; c++) {
			for (int r = 0; r < 2; r++) {
				m_AnimTip[c][r].left = tipWidth * r;
				m_AnimTip[c][r].right = m_AnimTip[c][r].left + tipWidth;
				m_AnimTip[c][r].top = tipHeight * c;
				m_AnimTip[c][r].bottom = m_AnimTip[c][r].top + tipHeight;
			}
		}

		m_AnimTipRow = 0;
		m_AnimTipCol = 0;
		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		vector<VertexPositionColorTexture> vertices = {
			{Vec3(-HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(+HelfSize, +HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].top)},
			{Vec3(-HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].left, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)},
			{Vec3(+HelfSize, -HelfSize, 0), white, Vec2(m_AnimTip[m_AnimTipCol][m_AnimTipRow].right, m_AnimTip[m_AnimTipCol][m_AnimTipRow].bottom)}
		};

		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);
		PtrSpriteDraw->SetTextureResource(wstringKey::txWave1);
		//透明処理
		SetAlphaActive(true);

		//shared_ptr<TestStageObject> a = GetComponent<TestStageObject>();
		GetStage()->AddGameObject<GoalObject>(m_Scale*0.2f, m_Rotation, m_Position, GetThis<GameObject>(), Vec2(768.0f, 192.0f), Vec2(1000.0f, 0.0f), 500.0f);
	}

	////衝突判定
	//衝突したら
	void GoalArea::OnCollisionEnter(shared_ptr<GameObject>& other) {
		auto& v = other;
		if (v->FindTag(wstringKey::tagPlayer)) {
			auto player = dynamic_pointer_cast<Player>(v);
			player->GetStateMachine()->ChangeState(PlayerClear::Instance());
		}
	}
	//衝突し続けてるとき
	void GoalArea::OnCollisionExcute(shared_ptr<GameObject>& other) {

	}
	//衝突し終わったら
	void GoalArea::OnCollisionExit(shared_ptr<GameObject>& other) {

	}

	//更新
	void GoalArea::OnUpdate() {
	}
	void GoalArea::OnUpdate2() {
		////文字列の表示
		//DrawStrings();
	}

}
//end basecross


