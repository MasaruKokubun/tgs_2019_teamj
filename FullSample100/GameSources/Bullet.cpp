#include "stdafx.h"
#include "Project.h"

namespace basecross {
	//--------------------------------------------------------------------------------------
	//	class Bullet : public GameObject;
	//	用途: テストステージブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Bullet::Bullet(const shared_ptr<Stage>& stagePtr, const Vec3& scale, const Vec3& rotation, const Vec3& position, const Vec3& angle) :
		GameObject(stagePtr),
		m_Scale(scale),
		m_Rotation(rotation),
		m_Position(position),
		m_Angle(angle)
	{
	}
	//初期化
	void Bullet::OnCreate() {

		//初期位置などの設定
		auto ptr = AddComponent<Transform>();
		ptr->SetScale(m_Scale);	//直径25センチの球体
		ptr->SetRotation(m_Rotation);
		ptr->SetPosition(m_Position);
		m_Speade = 10.0f;
		//CollisionSphere衝突判定を付ける
		auto ptrColl = AddComponent<CollisionSphere>();
		ptrColl->SetAfterCollision(AfterCollision::None);
		//タグをつける
		AddTag(wstringKey::tagPlayerBullet);

		//描画するテクスチャを設定
		Col4 white(1.0f, 1.0f, 1.0f, 1.0f);
		float HelfSize = 0.5f;
		vector<VertexPositionColorTexture> vertices = {
			{ Vec3(-HelfSize, +HelfSize, 0), white, Vec2(0.0f,0.0f) },
			{ Vec3(+HelfSize, +HelfSize, 0), white, Vec2(1.0f, 0.0f) },
			{ Vec3(-HelfSize, -HelfSize, 0), white, Vec2(0.0f ,1.0f) },
			{ Vec3(+HelfSize, -HelfSize, 0), white, Vec2(1.0f, 1.0f) }
		};
		vector<uint16_t> indices = {
			0, 1, 2,
			2, 1, 3
		};
		//頂点とインデックスを指定してスプライト作成
		auto PtrSpriteDraw = this->AddComponent<PCTStaticDraw>();
		PtrSpriteDraw->CreateOriginalMesh<VertexPositionColorTexture>(vertices, indices);
		PtrSpriteDraw->SetOriginalMeshUse(true);

		PtrSpriteDraw->SetTextureResource(wstringKey::txPlayerBullet);

		//透明処理
		SetAlphaActive(true);

		////文字列をつける
		//auto ptrString = AddComponent<StringSprite>();
		//ptrString->SetText(L"");
		//ptrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));
	}

	////衝突判定
	//衝突したら
	void Bullet::OnCollisionEnter(shared_ptr<GameObject>& other) {

	}
	//衝突し続けてるとき
	void Bullet::OnCollisionExcute(shared_ptr<GameObject>& other) {

	}
	//衝突し終わったら
	void Bullet::OnCollisionExit(shared_ptr<GameObject>& other) {

	}

	//更新
	void Bullet::OnUpdate() {
		auto pos = GetComponent<Transform>()->GetPosition();
		pos += m_Angle * m_Speade*App::GetApp()->GetElapsedTime();
		GetComponent<Transform>()->SetPosition(pos);
	}
	void Bullet::OnUpdate2() {
		////文字列の表示
		//DrawStrings();
	}

	//文字列の表示
	void Bullet::DrawStrings() {

		//文字列表示
		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring fpsStr(L"FPS: ");
		fpsStr += Util::UintToWStr(fps);
		fpsStr += L"\nElapsedTime: ";
		float ElapsedTime = App::GetApp()->GetElapsedTime();
		fpsStr += Util::FloatToWStr(ElapsedTime);
		fpsStr += L"\n";

		auto pos = GetComponent<Transform>()->GetPosition();
		wstring positionStr(L"Position:\t");
		positionStr += L"X=" + Util::FloatToWStr(pos.x, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Y=" + Util::FloatToWStr(pos.y, 6, Util::FloatModify::Fixed) + L",\t";
		positionStr += L"Z=" + Util::FloatToWStr(pos.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring speadStr(L"Spead:\t");
		//auto gravVelocity = GetComponent<Gravity>()->GetGravityVelocity();
		//speadStr += Util::FloatToWStr(m_Spead*100.0f, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Y=" + Util::FloatToWStr(gravVelocity.y, 6, Util::FloatModify::Fixed) + L",\t";
		//gravStr += L"Z=" + Util::FloatToWStr(gravVelocity.z, 6, Util::FloatModify::Fixed) + L"\n";

		wstring str = fpsStr + positionStr + speadStr;
		//文字列コンポーネントの取得
		auto ptrString = GetComponent<StringSprite>();
		ptrString->SetText(str);
	}

}